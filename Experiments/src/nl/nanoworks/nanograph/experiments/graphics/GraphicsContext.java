/* 
 * NanoGraph, a small footprint java graph drawing component
 * 
 * Copyright (C) 2004 Jeroen van Grondelle
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * You can contact the author at jeroen@nanoworks.nl.
 * 
 */ 
package nl.nanoworks.nanograph.experiments.graphics;

/**
 * GraphicsContext.java
 * 
 * Rationale: 
 * - Create an abstraction of a painting object that fits on painting
 * frameworks like SVG, AWT (G2D), SWT etc.
 * - Create a stack of contexts for painting, such that a child context
 * inherits form a parent, can modify it, but after popping, the parent is unmodified.
 * - This is similar to SVG G-elements
 * - Each renderer can push a context, set its own layout, and pop afterward.
 * - Configuring and Drawing should not be mixed within one context
 *
 * @author Jeroen van Grondelle
 */
public interface GraphicsContext 
{
	// context pushing and popping
	public void newContext();
	public void endContext();
	
	// configuring
	public void setColor(String cssColor);
	public void setLineStyle(String cssAttribute, String value);
	public void setFillStyle(String cssAttribute, String value);
	
	// drawing
	public void drawString(double x1, double x2, String string);
	public void drawLine(double x1, double y1, double x2, double y2);
	public void drawCircle(double x1, double x2, double radius);
	public void fillCircle(double x1, double x2, double radius);
	
	// managing resources
	public String loadImage();
	
	// calculations
	public double stringWidth(String string);
}
