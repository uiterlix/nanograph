/* 
 * NanoGraph, a small footprint java graph drawing component
 * 
 * Copyright (C) 2004 Jeroen van Grondelle
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * You can contact the author at jeroen@nanoworks.nl.
 * 
 */ 

package nl.nanoworks.nanograph.experiments.graphics.awt;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;

import nl.nanoworks.nanograph.experiments.graphics.GraphicsContext;
import nl.nanoworks.nanograph.experiments.graphics.util.AWTCSSSupport;

/**
 * AWTGraphicsContext.java
 *
 * @author Jeroen van Grondelle
 */
public class AWTGraphicsContext implements GraphicsContext {

	Graphics2D g2d = null;
	
	public AWTGraphicsContext(Graphics2D g2d)
	{
		this.g2d = g2d;
	}
	
	public void newContext() {
		
	}


	public void endContext() {
		// TODO Auto-generated method stub

	}

	public void setColor(String cssColor) {
		Color c = AWTCSSSupport.getPaint(cssColor);
		
		if(c != null)
		{
			g2d.setColor(c);
		}
	}

	/* (non-Javadoc)
	 * @see nl.nanoworks.nanograph.experiments.drawing.GraphicsContext#setLineStyle(java.lang.String, java.lang.String)
	 */
	public void setLineStyle(String cssAttribute, String value) {
		// TODO Auto-generated method stub

	}
	
	/* (non-Javadoc)
	 * @see nl.nanoworks.nanograph.experiments.drawing.GraphicsContext#setFillStyle(java.lang.String, java.lang.String)
	 */
	public void setFillStyle(String cssAttribute, String value) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see nl.nanoworks.nanograph.experiments.drawing.GraphicsContext#drawString(double, double, java.lang.String)
	 */
	public void drawString(double x1, double x2, String string) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see nl.nanoworks.nanograph.experiments.drawing.GraphicsContext#drawLine(double, double, double)
	 */
	public void drawLine(double x1, double y1, double x2, double y2) {
		g2d.draw(new Line2D.Double(x1, y1, x2, y2));

	}

	/* (non-Javadoc)
	 * @see nl.nanoworks.nanograph.experiments.drawing.GraphicsContext#drawCircle(double, double, double)
	 */
	public void drawCircle(double x1, double x2, double radius) {
		// TODO Auto-generated method stub

	}
	
	/* (non-Javadoc)
	 * @see nl.nanoworks.nanograph.experiments.drawing.GraphicsContext#drawCircle(double, double, double)
	 */
	public void fillCircle(double x1, double x2, double radius) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see nl.nanoworks.nanograph.experiments.drawing.GraphicsContext#loadImage()
	 */
	public String loadImage() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.nanoworks.nanograph.experiments.drawing.GraphicsContext#stringWidth(java.lang.String)
	 */
	public double stringWidth(String string) {
		// TODO Auto-generated method stub
		return 0;
	}

	private Color parseColor(String color)
	{
		
		return null;
	}
}
