package nl.nanoworks.nanograph.experiments.graphics.svg;

import nl.nanoworks.nanograph.experiments.graphics.GraphicsContext;

public class SVGGraphicsContext implements GraphicsContext {
    
    // Stack context objecten
    /*
     * class Context {
     *  String lineStyle, color, etc.
     *  int mode = -1;
     *  constate CONFIGURING = 0;
     *  constante DRAWING = 1;
     *  }
     *  
     *  Bij iedere aanroep van drawing methoden wordt gekeken of de mode 
     *  wel drawing is, zo niet dan wordt die gezet en wordt het G-element
     *  en bijbehorende css style gerendered ter voorbereiding van de operatie.
     *  daarbij wordt voor iedere style waarde de stack gelezen en de bovenste
     *  waarde gebruikt (zo wordt overerving geimplementeerd)
     *  
     *  Bij iedere aanroep van configure methode wordt gekeken of de mode
     *  van de bovenste context wel configuring
     *  is, zo niet dan wordt de methode genegeerd/exception
     *  zo ja, dan wordt de configure uitgevoerd op de bovenste context.
     */

    public void newContext() {
        // TODO Auto-generated method stub

    }

    public void endContext() {
        // TODO Auto-generated method stub

    }

    public void setColor(String cssColor) {
        // TODO Auto-generated method stub

    }

    public void setLineStyle(String cssAttribute, String value) {
        // TODO Auto-generated method stub

    }

    public void setFillStyle(String cssAttribute, String value) {
        // TODO Auto-generated method stub

    }

    public void drawString(double x1, double x2, String string) {
        // TODO Auto-generated method stub

    }

    public void drawLine(double x1, double y1, double x2, double y2) {
        // TODO Auto-generated method stub

    }

    public void drawCircle(double x1, double x2, double radius) {
        // TODO Auto-generated method stub

    }

    public void fillCircle(double x1, double x2, double radius) {
        // TODO Auto-generated method stub

    }

    public String loadImage() {
        // TODO Auto-generated method stub
        return null;
    }

    public double stringWidth(String string) {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }

}
