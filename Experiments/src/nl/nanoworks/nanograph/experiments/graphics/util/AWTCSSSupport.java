/* 
 * NanoGraph, a small footprint java graph drawing component
 * 
 * Copyright (C) 2004 Jeroen van Grondelle
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * You can contact the author at jeroen@nanoworks.nl.
 * 
 */
package nl.nanoworks.nanograph.experiments.graphics.util;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

/**
 * CSSSupport.java
 * 
 * @author Jeroen van Grondelle
 */
public class AWTCSSSupport {

	public final String supportedAtttributes = "color,line-width,line-style";

	public boolean isSupportedAtttribute(String atttribute) {
		return supportedAtttributes.indexOf(atttribute) > -1;
	}
	static private Map constants = new HashMap();

	static {
		constants.put("lightGray", Color.lightGray);
		constants.put("blue", Color.blue);
		constants.put("red", Color.red);
		constants.put("green", Color.green);
		constants.put("black", Color.black);
		constants.put("yellow", Color.yellow);
	}

	/*
	 * XHTML Colors Note: Should be interpreted case INSENSITIVE
	 * 
	 * Aqua #00FFFF Black #000000 Blue #0000FF Fuchsia #FF00FF
	 * 
	 * Gray #808080 Green #008000 Lime #00FF00 Maroon #800000
	 * 
	 * Navy #000080 Olive #808000 Purple #800080 Red #FF0000
	 * 
	 * Silver #C0C0C0 Teal #008080 White #FFFFFF Yellow #FFFF00
	 */

	public static Color getPaint(String paint) {
		System.err.println("Parsing color: " + paint);

		if (paint.equals("none")) {
			return null;
		} else if (constants.containsKey(paint)) {
			return (Color) constants.get(paint);
		} else if (paint.startsWith("rgb")) {
			int r = Integer.parseInt(paint.substring(paint.indexOf("(") + 1,
					paint.indexOf(",")));
			int g = Integer.parseInt(paint.substring(paint.indexOf(",") + 1,
					paint.lastIndexOf(",")));
			int b = Integer.parseInt(paint.substring(
					paint.lastIndexOf(",") + 1, paint.indexOf(")")));

			return new Color(r, g, b);
		} else if (paint.startsWith("#")) {
			return Color.decode(paint);
		} else {
			// SHOULD NOT OCCURR
			return Color.DARK_GRAY;
		}
	}
}