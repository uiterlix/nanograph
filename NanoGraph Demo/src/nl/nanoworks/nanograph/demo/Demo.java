/* 
 * NanoGraph, a small footprint java graph drawing component
 * 
 * Copyright (C) 2004 Jeroen van Grondelle
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * You can contact the author at jeroen@nanoworks.nl.
 * 
 */

package nl.nanoworks.nanograph.demo;

import nl.nanoworks.nanograph.components.opengl.SWTOpenGLJOGLNanoGraphPanel;
import nl.nanoworks.nanograph.components.swt.SWTNanoGraphPanel;
import nl.nanoworks.nanograph.demo.caffeine.AtomRenderer;
import nl.nanoworks.nanograph.demo.caffeine.Caffeine;
import nl.nanoworks.nanograph.demo.caffeine.MoleculeVertexRenderer;
import nl.nanoworks.nanograph.drawing.NanoGraph;
import nl.nanoworks.nanograph.drawing.docking.CenterDockingStrategy;
import nl.nanoworks.nanograph.drawing.layout.forcedirected.ForceDirectedLayout;
import nl.nanoworks.nanograph.model.GraphModel;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.Shell;

public class Demo {

    GraphModel model = new Caffeine();

    NanoGraph graph = new NanoGraph();

    public Demo() {

        graph.setModel(model);
        graph.setDefaultNodeRenderer(new AtomRenderer());
        graph.setDefaultEdgeRenderer(new MoleculeVertexRenderer());
        graph.setDefaultDockingStrategy(new CenterDockingStrategy());

        Display display = new Display();

        Shell shell = new Shell(display);
        shell.setSize(10, 10);
        shell.setLayout(new FillLayout());

        shell.setText("SWT/JOGL Nanograph test 3D screen");
        shell.setSize(400, 400);
        shell.setLocation(display.getMonitors()[0].getClientArea().width - 400, 0);

        CTabFolder tabs = new CTabFolder(shell, SWT.NONE);
        CTabItem drieD = new CTabItem(tabs, SWT.CLOSE);
        drieD.setText("3D");

        CTabItem tweeD = new CTabItem(tabs, SWT.CLOSE);
        tweeD.setText("2D");

        screen2D(tabs, tweeD);
        screen3D(tabs, drieD);

        shell.open();

        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }

        display.dispose();

    }

    private void screen3D(CTabFolder tabs, CTabItem item) {
        // Create the actual graphpanel
        Composite body = new Composite(tabs, SWT.NONE);
        item.setControl(body);
        Layout opengllayout = new FillLayout();
        body.setLayout(opengllayout);
        SWTOpenGLJOGLNanoGraphPanel joglpanel = new SWTOpenGLJOGLNanoGraphPanel(body);
        joglpanel.getGraphicsAdapter().setSphereRadius(AtomRenderer.DIAMETER / 100);
        joglpanel.setNanoGraph(graph);
    }

    private void screen2D(CTabFolder tabs, CTabItem item) {
        // Create the actual graphpanel
        ScrolledComposite sc = new ScrolledComposite(tabs, SWT.H_SCROLL | SWT.V_SCROLL);
        item.setControl(sc);
        SWTNanoGraphPanel panel = new SWTNanoGraphPanel(sc, SWT.DOUBLE_BUFFERED);
        sc.setContent(panel);
        panel.getNanoGraph().setDefaultNodeRenderer(new AtomRenderer());
        panel.getNanoGraph().setDefaultEdgeRenderer(new MoleculeVertexRenderer());
        panel.getNanoGraph().setDefaultDockingStrategy(new CenterDockingStrategy(AtomRenderer.DIAMETER));
        panel.setModel(model);
    }

    public static void main(String args[]) {
        new Demo();
    }

}
