/* 
 * NanoGraph, a small footprint java graph drawing component
 * 
 * Copyright (C) 2004 Jeroen van Grondelle
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * You can contact the author at jeroen@nanoworks.nl.
 * 
 */ 

package nl.nanoworks.nanograph.demo.caffeine;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

public class Atom {
    private String element = null;
    private Molecule molecule = null;
    private Point2D location = null;
    private int id;
    
    List connectedAtoms = new ArrayList();
    
    public Atom(Molecule molecule, String element, int id)
    {
        this.element = element;
        this.molecule = molecule;
        this.id = id;
        
        molecule.add(this);
    }
    
    public Atom connect(Atom atom)
    {
        connectedAtoms.add(atom);
        return this;
    }
    
    public String toString()
    {
        return element;
    }
    
    public String getElement() {
        return element;
    }

    public int getId() {
        return id;
    }

    public Point2D getLocation() {
        if (location == null) {
            location = new Point2D.Double(10,10);
        }
        return location;
    }

    public void setLocation(Point2D location) {
        this.location = location;
    }

    public Molecule getMolecule() {
        return molecule;
    }

    public List getConnectedAtoms() {
        return connectedAtoms;
    }
}
