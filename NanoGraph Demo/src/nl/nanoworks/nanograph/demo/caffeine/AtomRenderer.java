/* 
 * NanoGraph, a small footprint java graph drawing component
 * 
 * Copyright (C) 2004 Jeroen van Grondelle
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * You can contact the author at jeroen@nanoworks.nl.
 * 
 */ 

package nl.nanoworks.nanograph.demo.caffeine;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Properties;

import nl.nanoworks.nanograph.drawing.graphicsadapter.GraphicsAdapter;
import nl.nanoworks.nanograph.drawing.noderenderer.NodeRenderer;

/**
 * 
 * 
 * H : #ffffff;
 * C : #909090;
 * N : #3050F8;
 * O : #FF0D0D
 * 
 * AtomRenderer.java
 *
 * @author Jeroen van Grondelle
 */

public class AtomRenderer implements NodeRenderer {
    
    Properties colors = new Properties();
    
    public static final double DIAMETER = 50;
    
    public AtomRenderer()
    {
        colors.put("H", "#ffffff");
        colors.put("C", "#909090");
        colors.put("N", "#3050F8");
        colors.put("O", "#FF0D0D");
    }

    public Rectangle2D getNodeBounds(GraphicsAdapter g, Object node, Point2D location) {
        return new Rectangle2D.Double(location.getX(), location.getY(), DIAMETER, DIAMETER);
    }

    public void render(GraphicsAdapter g, Object node, Point2D location) {
        String element = "H";
        if(node instanceof Atom) {
            element = ((Atom)node).getElement();
        }
        g.setColor("black");   
        g.setFillColor(colors.getProperty(element, "H"));
        g.fillOval(location.getX(), location.getY(), DIAMETER, DIAMETER);
        g.drawOval(location.getX(), location.getY(), DIAMETER, DIAMETER);
    }

    public void renderSelected(GraphicsAdapter g, Object node, Point2D location) {
        render(g, node, location);
    }
}
