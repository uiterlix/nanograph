/* 
 * NanoGraph, a small footprint java graph drawing component
 * 
 * Copyright (C) 2004 Jeroen van Grondelle
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * You can contact the author at jeroen@nanoworks.nl.
 * 
 */
package nl.nanoworks.nanograph.demo.caffeine;

import java.awt.geom.Point2D;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Caffeine extends Molecule {

    public Caffeine() {
        Atom n1 = new Atom(this, "N", 1).connect(new Atom(this, "C", 2).connect(new Atom(this, "H", 3)).connect(new Atom(this, "H", 4)).connect(new Atom(this, "H", 5)));

        Atom c2 = new Atom(this, "C", 6).connect(new Atom(this, "O", 7));

        Atom n3 = new Atom(this, "N", 8).connect(new Atom(this, "C", 9).connect(new Atom(this, "H", 10)).connect(new Atom(this, "H", 11)).connect(new Atom(this, "H", 12)));

        Atom c4 = new Atom(this, "C", 13).connect(new Atom(this, "O", 14));

        Atom c5 = new Atom(this, "C", 15);

        Atom n6 = new Atom(this, "N", 16).connect(new Atom(this, "C", 17).connect(new Atom(this, "H", 18)).connect(new Atom(this, "H", 19)).connect(new Atom(this, "H", 20)));

        Atom c7 = new Atom(this, "C", 21).connect(new Atom(this, "H", 22));

        Atom n8 = new Atom(this, "N", 23);

        Atom c9 = new Atom(this, "C", 24);

        n1.connect(c2);

        c2.connect(n3);
        n3.connect(c4);
        c4.connect(c5);
        c5.connect(n6);
        n6.connect(c7);
        c7.connect(n8);
        n8.connect(c9);
        c9.connect(n1);

        c5.connect(c9);

        Map locations = new HashMap();
        locations.put(new Integer(1), new Point2D.Double(350.0, 519.0));
        locations.put(new Integer(2), new Point2D.Double(205.0, 520.0));
        locations.put(new Integer(3), new Point2D.Double(118.0, 624.0));
        locations.put(new Integer(4), new Point2D.Double(56.0, 523.0));
        locations.put(new Integer(5), new Point2D.Double(114.0, 390.0));
        locations.put(new Integer(6), new Point2D.Double(449.0, 675.0));
        locations.put(new Integer(7), new Point2D.Double(346.0, 771.0));
        locations.put(new Integer(8), new Point2D.Double(605.0, 694.0));
        locations.put(new Integer(9), new Point2D.Double(721.0, 772.0));
        locations.put(new Integer(10), new Point2D.Double(671.0, 900.0));
        locations.put(new Integer(11), new Point2D.Double(867.0, 751.0));
        locations.put(new Integer(12), new Point2D.Double(809.0, 867.0));
        locations.put(new Integer(13), new Point2D.Double(721.0, 562.0));
        locations.put(new Integer(14), new Point2D.Double(864.0, 555.0));
        locations.put(new Integer(15), new Point2D.Double(656.0, 374.0));
        locations.put(new Integer(16), new Point2D.Double(746.0, 203.0));
        locations.put(new Integer(17), new Point2D.Double(886.0, 161.0));
        locations.put(new Integer(18), new Point2D.Double(1012.0, 218.0));
        locations.put(new Integer(19), new Point2D.Double(1012.0, 121.0));
        locations.put(new Integer(20), new Point2D.Double(929.0, 26.0));
        locations.put(new Integer(21), new Point2D.Double(587.0, 113.0));
        locations.put(new Integer(22), new Point2D.Double(607.0, 0.0));
        locations.put(new Integer(23), new Point2D.Double(433.0, 200.0));
        locations.put(new Integer(24), new Point2D.Double(474.0, 375.0));
        for (Iterator iter = atoms.iterator(); iter.hasNext();) {
            Atom atom = (Atom) iter.next();
            atom.setLocation((Point2D) locations.get(new Integer(atom.getId())));
        }
    }

}
