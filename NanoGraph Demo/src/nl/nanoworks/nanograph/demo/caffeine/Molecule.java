/* 
 * NanoGraph, a small footprint java graph drawing component
 * 
 * Copyright (C) 2004 Jeroen van Grondelle
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * You can contact the author at jeroen@nanoworks.nl.
 * 
 */ 
package nl.nanoworks.nanograph.demo.caffeine;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import nl.nanoworks.nanograph.model.GraphModel;

public class Molecule implements GraphModel 
{
    List atoms = new ArrayList();
    
    public void add(Atom a)
    {
        atoms.add(a);
    }
    
    public Object getDestinationNode(Object node, Object edgeType, int index) {
        return ((Atom)node).connectedAtoms.get(index);
    }

    public Object getEdge(Object node, Object edgeType, int index) {
        // edges are not in the object model
        return "a bond";
    }

    public int getEdgeCount(Object node, Object edgeType) {
        return ((Atom)node).connectedAtoms.size();
    }

    public Object getEdgeType(Object node, int index) {
        // There is only one edgetype.
        return "bond";
    }

    public int getEdgeTypeCount(Object node) {
        // There is only one edgetype.
        return 1;
    }

    public Point2D getLocation(Object node) {
        Atom a = (Atom)node;
        return a.getLocation();
    }

    public Object getNode(int index) {
        return atoms.get(index);
    }

    public int getNodeCount() {
        return atoms.size();
    }

    public void setLocation(Object node, Point2D location) {
       ((Atom)node).setLocation(location);
//       System.out.println(">>> location data");
//       for (int i=0; i < getNodeCount(); i++) {
//           Atom atom = (Atom) getNode(i);
//           System.out.println("locations.put(new Integer("+atom.getId()+"),new Point2D.Double("+atom.getLocation().getX()+","+atom.getLocation().getY()+"));");
//       }
//       System.out.println(">>> location data");
    }

    public void setLocation(Object node, double x, double y) {
        setLocation(node, new Point2D.Double(x,y));
    }
}
