//$copyright
/*
 * Created on 8-dec-2006
 */
package nl.nanoworks.nanograph.demo.caffeine;

import java.awt.geom.Point2D;

import nl.nanoworks.nanograph.drawing.edgerenderer.AbstractEdgeRenderer;
import nl.nanoworks.nanograph.drawing.graphicsadapter.GraphicsAdapter;

public class MoleculeVertexRenderer extends AbstractEdgeRenderer {
    protected int getType(Object edge) {
        return LINE;
    }

    protected void render(GraphicsAdapter g, Object edge, Point2D from, Point2D to, boolean selected) {
        g.setColor("#000000");
        g.drawLine(from.getX(), from.getY(), to.getX(), to.getY(), GraphicsAdapter.CONNECTOR_STYLE_NONE, GraphicsAdapter.CONNECTOR_STYLE_NONE, GraphicsAdapter.LINESTYLE_SOLID);

        int type = getType(edge);
        // text
        if ((type & TEXT) == TEXT) {
            String string = edge.toString();
            float width = 0;
            g.setColor(textColor);
            g.drawString(string, (from.getX() + to.getX() - width) / 2, ((from.getY() + to.getY()) / 2) - 4);
        }
    }
}
