/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
library ForceDirectedLayout;

import 'dart:html';
import 'dart:math';
import 'graphmodel.dart';
import 'graph.dart';

class Node {

    String id;
    double dx = 0.0;
    double dy = 0.0;
    GraphModel model = null;
    Object data = null;
    bool justMadeLocal = false;
    bool markedForRemoval = false;
    double massfade = 1.0;
    int repulsion = 60;

    Node(String id, Object newData, GraphModel newModel) {
        data = newData;
        model = newModel;
        this.id = id;
    }

    void setID(String id) {
        this.id = id;
    }

    String getID() {
        return id;
    }

    void setLocation(double x, double y) {
        model.setPosition(data, x.toInt(), y.toInt());
    }

    Point getLocation() {
       return model.getPosition(data);
    }
}

class Edge {

    static int DEFAULT_LENGTH = 200;

    Node from;

    Node to;

    int length;

    bool visible;

    String id = null;

    Edge(Node f, Node t) {
        from = f;
        to = t;
        length = DEFAULT_LENGTH;
        visible = false;
    }

    Node getFrom() {
        return from;
    }

    Node getTo() {
        return to;
    }

    String getID() {
        return id;
    }

    void setID(String id) {
        this.id = id;
    }

    int getLength() {
        return length;
    }

    void setLength(int len) {
        length = len;
    }

    void setVisible(bool v) {
        visible = v;
    }

    bool isVisible() {
        return visible;
    }
}


class ForceDirectedLayout {

    static double def_damper = 0.7;

    Map nodeCache = new Map();
    Map edgeCache = new Map();
    double maxMotion = 0.0; // Keep an eye on the fastest moving node to see if the graph is stabilizing
    double lastMaxMotion = 0.0;
    double motionRatio = 0.0; // It's sort of a ratio, equal to lastMaxMotion/maxMotion-1
    double damper = def_damper; // A low damper value causes the
    bool damping = true; // When damping is true, the damper
    double rigidity = 1.0; // Rigidity has the same effect as
    double newRigidity = 1.0;
    Graph graph = null;
    GraphModel model = null;
    bool keepMoving = false;

    ForceDirectedLayout(Graph graph) {
      this.graph = graph;
      this.model = graph.graphModel;
    }

    void layout() {
        init();
        keepMoving = true;
        window.requestAnimationFrame(applyLayout);
    }

    String createEdgeName(Node n1, Node n2) {
      if (n1 == null || n2 == null) {
        return "";
      }
      return n1.getID() + n2.getID();
    }

    void init() {
        // cache all edges and nodes
        nodeCache.clear();
        edgeCache.clear();
        for (int t = 0; t < model.getNodeCount(); t++) {
            final Object n = model.getNode(t);
            final Node node = new Node(n.toString(), n, model);
            nodeCache.putIfAbsent(n, () => node);
        }
        for (int t = 0; t < model.getNodeCount(); t++) {
            Object n = model.getNode(t);
            for (int e = 0; e < model.getVertexCount(n); e++) {
              Object n2 = model.getTargetNode(n, e);
              Node node1 = nodeCache[n] as Node;
              Node node2 = nodeCache[n2] as Node;
              Edge edge = new Edge(node1, node2);
              edgeCache.putIfAbsent(createEdgeName(node1, node2), () => edge);
            }
        }
    }


    void setRigidity(double r) {
        newRigidity = r; // update rigidity at the end of the relax()
    }

    // relaxEdges is more like tense edges up. All edges pull nodes
    // closes together;
    void relaxEdges() {

      Point fromLocation;
      Point toLocation;
        for (int t = 0; t < model.getNodeCount(); t++) {
            final Object n = model.getNode(t);
                for (int ed = 0; ed < model.getVertexCount(n); ed++) {
                    Object n2 = model.getTargetNode(n, ed);
                    Node node1 = nodeCache[n] as Node;
                    Node node2 = nodeCache[n2] as Node;

                    Edge e = edgeCache[createEdgeName(node1, node2)] as Edge;
                    if (e == null) {
                        e = new Edge(node1, node2);
                        edgeCache.putIfAbsent(createEdgeName(node1, node2), () => e);
                    }

                    fromLocation = e.from.getLocation();
                    toLocation = e.to.getLocation();
                    double vx = ((toLocation.x - fromLocation.x) as int).toDouble();
                    double vy = ((toLocation.y - fromLocation.y) as int).toDouble();
                    double len = sqrt(vx * vx + vy * vy);
                    double dx = vx * rigidity; // rigidity makes edges tighter
                    double dy = vy * rigidity;
                    dx /= (e.getLength() * 100);
                    dy /= (e.getLength() * 100);

                    // Edges pull directly in proportion to the distance
                    // between the nodes. This is good,
                    // because we want the edges to be stretchy. The edges
                    // are ideal rubberbands. They
                    // They don't become springs when they are too short.
                    // That only causes the graph to
                    // oscillate.

                    if (e.to.justMadeLocal || e.to.markedForRemoval ||
                        (!e.from.justMadeLocal && !e.from.markedForRemoval)) {
                        e.to.dx = e.to.dx - dx * len;
                        e.to.dy = e.to.dy - dy * len;
                    } else {
                        double massfade = (e.from.markedForRemoval ?
                                           e.from.massfade : 1 - e.from.massfade);
                        massfade *= massfade;
                        e.to.dx = e.to.dx - dx * len * massfade;
                        e.to.dy = e.to.dy - dy * len * massfade;
                    }
                    if (e.from.justMadeLocal || e.from.markedForRemoval ||
                        (!e.to.justMadeLocal && !e.to.markedForRemoval)) {
                        e.from.dx = e.from.dx + dx * len;
                        e.from.dy = e.from.dy + dy * len;
                    } else {
                        double massfade = (e.to.markedForRemoval ? e.to.massfade :
                                           1 - e.to.massfade);
                        massfade = massfade * massfade;
                        e.from.dx = e.from.dx + dx * len * massfade;
                        e.from.dy = e.from.dy + dy * len * massfade;
                    }
                }
        }
    }


    /*
     * private synchronized void avoidLabels() { for (int i = 0 ; i <
     * graphEltSet.nodeNum() ; i++) { Node n1 = graphEltSet.nodeAt(i);
     * double dx = 0; double dy = 0;
     *
     * for (int j = 0 ; j < graphEltSet.nodeNum() ; j++) { if (i == j) {
     * continue; // It's kind of dumb to do things this way. j should go
     * from i+1 to nodeNum. } Node n2 = graphEltSet.nodeAt(j); double vx =
     * n1.x - n2.x; double vy = n1.y - n2.y; double len = vx * vx + vy * vy; //
     * so it's length squared if (len == 0) { dx += Math.random(); // If two
     * nodes are right on top of each other, randomly separate dy +=
     * Math.random(); } else if (len <600*600) { //600, because we don't
     * want deleted nodes to fly too far away dx += vx / len; // If it was
     * sqrt(len) then a single node surrounded by many others will dy += vy /
     * len; // always look like a circle. This might look good at first, but
     * I think // it makes large graphs look ugly + it contributes to
     * oscillation. A // linear function does not fall off fast enough, so
     * you get rough edges // in the 'force field'
     *  } } n1.dx += dx*100*rigidity; // rigidity makes nodes avoid each
     * other more. n1.dy += dy*100*rigidity; // I was surprised to see that
     * this exactly balances multiplying edge tensions // by the rigidity,
     * and thus has the effect of slowing the graph down, while // keeping
     * it looking identical.
     *  } }
     */

    void avoidLabels() {
      
      Point n1Location;
      Point n2Location;
      
        for (int t = 0; t < model.getNodeCount(); t++) {
            final Object node1 = model.getNode(t);

            Node n1 = nodeCache[node1] as Node;
            if (n1 == null) {
                n1 = new Node(node1.toString(), node1, model);
                nodeCache.putIfAbsent(node1, () => n1);
            }

            for (int r = 0; r < model.getNodeCount(); r++) {
                final Object node2 = model.getNode(r);
                Node n2 = nodeCache[node2] as Node;
                if (n2 == null) {
                    n2 = new Node(node2.toString(), node2, model);
                    nodeCache.putIfAbsent(node2, () => n2);
                }

                double dx = 0.0;
                double dy = 0/0;
                
                n1Location = n1.getLocation();
                n2Location = n2.getLocation();
                
                double vx = ((n1Location.x - n2Location.y) as int).toDouble();
                double vy = ((n1Location.x - n2Location.y) as int).toDouble();
                double len = vx * vx + vy * vy; // so it's length
                // squared
                if (len == 0) {
                    Random rng = new Random();
                    dx = rng.nextDouble(); // If two nodes are right on top
                    // of each other, randomly
                    // separate
                    dy = rng.nextDouble();
                } else if (len < 600 * 600) { // 600, because we don't
                    // want deleted nodes to fly
                    // too far away
                    dx = vx / len; // If it was sqrt(len) then a single
                    // node surrounded by many others
                    // will
                    dy = vy / len; // always look like a circle. This
                    // might look good at first, but I
                    // think
                    // it makes large graphs look ugly +
                    // it contributes to oscillation. A
                    // linear function does not fall off
                    // fast enough, so you get rough
                    // edges
                    // in the 'force field'
                }

                int repSum = (n1.repulsion * n2.repulsion ~/ 100).toInt();
                if (n1.justMadeLocal || n1.markedForRemoval ||
                    (!n2.justMadeLocal && !n2.markedForRemoval)) {
                    n1.dx = n1.dx + dx * repSum * rigidity;
                    n1.dy = n1.dy + dy * repSum * rigidity;
                } else {
                    double massfade = (n2.markedForRemoval ? n2.massfade :
                                       1 - n2.massfade);
                    massfade *= massfade;
                    n1.dx = n1.dx + dx * repSum * rigidity * massfade;
                    n1.dy = n1.dy + dy * repSum * rigidity * massfade;
                }
                if (n2.justMadeLocal || n2.markedForRemoval ||
                    (!n1.justMadeLocal && !n1.markedForRemoval)) {
                    n2.dx = n2.dx - dx * repSum * rigidity;
                    n2.dy = n2.dy - dy * repSum * rigidity;
                } else {
                    double massfade = (n1.markedForRemoval ? n1.massfade :
                                       1 - n1.massfade);
                    massfade = massfade * massfade;
                    n2.dx = n2.dx - dx * repSum * rigidity * massfade;
                    n2.dy = n2.dy - dy * repSum * rigidity * massfade;
                }
            }
        }
    }

    void startDamper() {
        damping = true;
    }

    void stopDamper() {
        damping = false;
        damper = 1.0; //A value of 1.0 means no damping
    }

    void resetDamper() { //reset the damper, but don't keep damping.
        damping = true;
        damper = def_damper;
        window.requestAnimationFrame(applyLayout);
    }

    void stopMotion() { // stabilize the graph, but do so gently by setting the damper to a low value
        damping = true;
        if (damper > 0.3) {
            damper = 0.3;
        } else {
            damper = 0.0;
        }
    }

    void damp() {
        if (damping) {
            if (motionRatio <= 0.001) { //This is important.  Only damp when the graph starts to move faster
                //When there is noise, you damp roughly half the time. (Which is a lot)
                //
                //If things are slowing down, then you can let them do so on their own,
                //without damping.

                //If max motion<0.2, damp away
                //If by the time the damper has ticked down to 0.9, maxMotion is still>1, damp away
                //We never want the damper to be negative though
                if ((maxMotion < 0.2 || (maxMotion > 1 && damper < 0.9)) && damper > 0.01) {
                    damper -= 0.01;
                }
                //If we've slowed down significanly, damp more aggresively (then the line two below)
                else if (maxMotion < 0.4 && damper > 0.003) {
                    damper -= 0.003;
                }
                //If max motion is pretty high, and we just started damping, then only damp slightly
                else if (damper > 0.0001) {
                    damper -= 0.0001;
                }
            }
        }
        if (maxMotion < 0.001 && damping) {
            damper = 0.0;
        }
    }

    void moveNodes() {
        lastMaxMotion = maxMotion;
        double maxMotionA = 0.0;
        Point nLocation;

        for (int t = 0; t < model.getNodeCount(); t++) {
            final Object node1 = model.getNode(t);

            Node n = nodeCache[node1] as Node;
            if (n == null) {
                n = new Node(node1.toString(), node1, model);
                nodeCache.putIfAbsent(node1, () => n);
            }

            double dx = n.dx;
            double dy = n.dy;
            dx = dx * damper; //The damper slows things down.  It cuts down jiggling at the last moment, and optimizes
            dy = dy * damper; //layout.  As an experiment, get rid of the damper in these lines, and make a
            //long straight line of nodes.  It wiggles too much and doesn't straighten out.

            n.dx = dx / 2; //Slow down, but don't stop.  Nodes in motion store momentum.  This helps when the force
            n.dy = dy / 2; //on a node is very low, but you still want to get optimal layout.

            double distMoved = sqrt(dx * dx + dy * dy); //how far did the node actually move?

            nLocation = n.getLocation();
            double xLoc = (nLocation.x as int).toDouble();
            xLoc = xLoc + max( -30, min(30, dx)); //don't move faster then 30 units at a time.
            if(xLoc < 0.0 || xLoc.toString() == "NaN"){
               xLoc = 0.0 ;
            }
            double yLoc = (nLocation.y as int).toDouble();
            if(yLoc < 0.0 || yLoc.toString() == "NaN"){
               yLoc = 0.0 ;
            }
            
            if (dx.toString() == "NaN") {
              dx = 0.0;
            }
            if (dy.toString() == "NaN") {
              dy = 0.0;
            }

            yLoc = yLoc + max( -30, min(30, dy)); //I forget when this is important.  Stopping severed nodes from flying away?
            n.setLocation(xLoc, yLoc);

            maxMotionA = max(distMoved, maxMotionA);
        }

        maxMotion = maxMotionA;
        if (maxMotion > 0) {
            motionRatio = lastMaxMotion / maxMotion - 1; //subtract 1 to make a positive value mean that
        } else {
            motionRatio = 0.0; //things are moving faster
        }

        damp();

    }

    void relax() {
        for (int i = 0; i < 10; i++) {
            relaxEdges();
            avoidLabels();
            moveNodes();
        }
        if (rigidity != newRigidity) {
            rigidity = newRigidity; // update
        }
    }
    
    void applyLayout(num _) {
      
        relax();
        graph.redraw();  
        if (!((damper < 0.1 && damping || maxMotion < 0.01) || maxMotion.toString() == "NaN")) {
          window.requestAnimationFrame(applyLayout);
        }
        // We're done
    }
    
    void stopLayout() {
      keepMoving = false;
    }
}
