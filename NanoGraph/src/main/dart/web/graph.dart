/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
library Graph;

import 'dart:html';
import 'dart:math';
import 'graphmodel.dart';
import 'nodepainter.dart';
import 'selection.dart';
import 'vertexpainter.dart';
import 'forcedirectedlayout.dart';
import 'dart:convert';

ButtonElement toggleButton;
ButtonElement zoomInButton;
ButtonElement zoomOutButton;
ButtonElement forceDirectedLayoutButton;
ButtonElement reloadButton;
Graph graph;


void main() {
  var config = JSON.decode(querySelector("#graph-config").innerHtml);
  print(config.toString());
  
  bool zoomIn = booleanValue(config["zoom-in"]);
  bool zoomOut = booleanValue(config["zoom-out"]);
  bool layout = booleanValue(config["layout"]);
  
  graph = new Graph(config);
  toggleButton = new ButtonElement();
  toggleButton..id = 'toggle'
      ..text = 'Move mode'
      ..onClick.listen((e) => graph.toggleMode());
  querySelector('#graph-buttons')..children.add(toggleButton);
  
  if (zoomIn) {
    zoomInButton = new ButtonElement();
    zoomInButton..id = 'zoom-in'
        ..text = '+'
        ..onClick.listen((e) => graph.zoomIn());
    querySelector('#graph-buttons')..children.add(zoomInButton);
  }
  
  if (zoomOut) {
    zoomOutButton = new ButtonElement();
    zoomOutButton..id = 'zoom-out'
        ..text = '-'
        ..onClick.listen((e) => graph.zoomOut());
    querySelector('#graph-buttons')..children.add(zoomOutButton);
  }
  
  if (layout) {
    forceDirectedLayoutButton = new ButtonElement();
    forceDirectedLayoutButton..id = 'force-directed-layout'
        ..text = 'layout'
        ..onClick.listen((e) => graph.applyForceDirectedLayout());
    querySelector('#graph-buttons')..children.add(forceDirectedLayoutButton);
  }
  
  reloadButton = new ButtonElement();
  reloadButton..id = 'reload-model'
      ..text = 'reload'
      ..onClick.listen((e) => graph.reload());
  querySelector('#graph-buttons')..children.add(reloadButton);
  
  graph.eventLogLabel = new LabelElement();
  querySelector('#eventlog')..children.add(graph.eventLogLabel);
  
  graph.drawCanvas();
  
}

bool booleanValue(bool boolean) {
  if (boolean != null) {
    return boolean;
  }
  return false;
}

class Graph {

  CanvasElement canvasElement;
  CanvasRenderingContext2D context;
  DefaultGraphModel graphModel;
  NodePainter nodePainter = new NodePainter();
  VertexPainter vertexPainter = new VertexPainter();
  SelectionManager selectionManager = new SelectionManager();
  bool mouseDown = false;
  bool makeSelection = false;
  bool dragged = false;
  Point dragStart = null;
  int offsetX = 0;
  int offsetY = 0;
  double scale = 1.0;

  Point mouseLocation;
  Object hoveredVertex;
  MouseMode mouseMode = MouseMode.SELECT; // 1 is moving canvas rather than selecting items
  LabelElement eventLogLabel;
  List latestEvents = new List();
  Point latestTouchPoint;
  Map boundsCache = new Map();
  var config;
  var inlineData;
  
  Graph(config) {
    this.config = config;
    if (config["load"]["data"] != null) {
      inlineData = config["load"]["data"];
    }
  }
  
  void toggleMode() {
    if (mouseMode == MouseMode.SELECT) {
      mouseMode = MouseMode.MOVE;
      toggleButton..text = 'Select mode';
    } else {
      mouseMode = MouseMode.SELECT;
      toggleButton..text = 'Move mode';
    }
  }
  
  void zoomIn() {
    // first reset scale to original
    context.scale(1 / scale, 1 / scale);
    scale = scale + 0.2;
    context.scale(scale, scale);
    redraw();
  }
  
  void zoomOut() {
    // first reset scale to original
    context.scale(1 / scale, 1 / scale);
    scale = scale - 0.2;
    context.scale(scale, scale);
    redraw();
  }
  
  void applyForceDirectedLayout() {
    ForceDirectedLayout layout = new ForceDirectedLayout(this);
    layout.layout();
  }
  
  void drawCanvas() {
    canvasElement = document.getElementById('graph-canvas');
    context = canvasElement.getContext('2d');
    
    canvasElement.addEventListener("mousedown", onMouseDown, false);
    canvasElement.addEventListener("mouseup", onMouseUp, false);
    canvasElement.addEventListener("mousemove", onMouseMove, false);
    
    canvasElement.addEventListener("touchstart", onTouchStart, false);
    canvasElement.addEventListener("touchmove", onTouchMove, false);
    canvasElement.addEventListener("touchend", onTouchEnd, false);
    
    graphModel = new DefaultGraphModel();
    
    reload();
  }
  
  updateAndRefreshEventLog(String event) {
    latestEvents.add(event);
    if (latestEvents.length > 10) {
      latestEvents.removeAt(0);
    }
    String logText = "";
    for (int i = (latestEvents.length - 1); i >= 0; i--) {
      logText = logText + latestEvents.elementAt(i);
      if (i > 0) {
        logText = logText + ", ";
      }
    }
    eventLogLabel.text = logText;
  }
  
  void onMouseDown(MouseEvent e) {
    Point point = getMousePoint(e);
    updateAndRefreshEventLog("mouse down (" + point.x.toString() + "," + point.y.toString() + ")");
    onSelectorDown(point);
  }
  
  void onTouchStart(TouchEvent e) {
    Point point = getTouchPoint(e);
    updateAndRefreshEventLog("touch start (" + point.x.toString() + "," + point.y.toString() + ")");
    onSelectorDown(point);
    latestTouchPoint = point;
    e.preventDefault();
  }
  
  void onSelectorDown(Point point) {
    mouseDown = true;
    dragged = false;
    dragStart = point;
    // check if selected objects still part of current position, otherwise
    Object nodeForPoint = getNode(dragStart);
    if (nodeForPoint == null || !selectionManager.isSelected(nodeForPoint)) {
      selectionManager.clearSelection();
      if (nodeForPoint != null) {
        selectionManager.addNodeToSelection(nodeForPoint);
      }
    }
    if (selectionManager.getSelectedObjects().length == 0) {
      makeSelection = true;
    }
  }
  
  void onMouseUp(MouseEvent e) {
    updateAndRefreshEventLog("mouse up");
    onSelectorUp(getMousePoint(e));
  }
  
  void onTouchEnd(TouchEvent e) {
    updateAndRefreshEventLog("touch end");
    onSelectorUp(latestTouchPoint);
  //  e.preventDefault();
  }
  
  void onSelectorUp(Point point) {
    updateAndRefreshEventLog("up (" + point.x.toString() + "," + point.y.toString() + ")");
    mouseDown = false;
    makeSelection = false;
    if (!dragged) {
      // single selection click
      selectionManager.clearSelection();
      Object node = getNode(point);
      if (node != null) {
        selectionManager.addNodeToSelection(node);
      }
    }
    redraw();
  }
  
  void onMouseMove(MouseEvent e) {
  //  updateAndRefreshEventLog("mouse move");
    onSelectorMove(getMousePoint(e));
  }
  
  void onTouchMove(TouchEvent e) {
  //  updateAndRefreshEventLog("touch move");
    Point point = getTouchPoint(e);
    latestTouchPoint = point;
    onSelectorMove(point);
    e.preventDefault();
  }
  
  void onSelectorMove(Point point) {
    if (mouseDown) {
      dragged = true;
      Point currentPoint = point;
      if (mouseMode == MouseMode.SELECT) {
        if (makeSelection) {
          // create selection
          List selectedNodes = getNodes(dragStart, currentPoint);
          selectionManager.clearSelection();
          for (Object node in selectedNodes) {
            selectionManager.addNodeToSelection(node);
          }
          redraw();
        } else {
          // drag selection      
          int moveX = currentPoint.x - dragStart.x;
          int moveY = currentPoint.y - dragStart.y;
          moveSelection((moveX * (1/scale)).toInt(), (moveY * (1/scale)).toInt());
          dragStart = currentPoint;
          redraw();
        }
      } else {
        // move canvas
        int moveX = currentPoint.x - dragStart.x;
        int moveY = currentPoint.y - dragStart.y;
        offsetX += moveX;
        offsetY += moveY;
        dragStart = currentPoint;
        context.translate(moveX * (1/scale), moveY * (1/scale));
        redraw();
      }
    } else {
      handleMouseOver(point);
    }
  }
  
  void handleMouseOver(Point point) {
    Point currentPoint = point;
    mouseLocation = currentPoint;
    
    bool vertexFound = false;
    for (int i = 0; i < graphModel.getNodeCount(); i++) {
      Object node = graphModel.getNode(i);
      // render vertices
      for (int j = 0; j < graphModel.getVertexCount(node); j++) {
        // We're not doing anything with this vertex info yet, but will be useful later on.
        Object vertex = graphModel.getVertex(node, j);
        Object target = graphModel.getTargetNode(node, j);
        
        Point nodeConnectionPoint = getConnectionPoint(node, getNodeCenter(target)); 
        Point targetConnectionPoint = getConnectionPoint(target, getNodeCenter(node)); 
        
        if (matchesCurrentMouseLocation(nodeConnectionPoint, targetConnectionPoint)
            && (isPointInViewPort(nodeConnectionPoint) || isPointInViewPort(targetConnectionPoint))) {
          // only hover one vertex at the same time
          vertexFound = true;
          hoveredVertex = vertex;
        } 
      }
    }
    if (vertexFound == true) {
      redraw();
    } else if (hoveredVertex != null) {
      hoveredVertex = null;
      redraw();
    }
  }
  
  void moveSelection(int moveX, int moveY) {
    for (Object node in selectionManager.getSelectedObjects()) {
      Point position = graphModel.getPosition(node);
      Point newPosition = new Point(position.x + moveX, position.y + moveY);
      setPosition(node, newPosition.x, newPosition.y);
    }
  }
  
  Point getMousePoint(MouseEvent e) {
    return getTranslatedPoint(e.page);
  }
  
  Point getTouchPoint(TouchEvent e) {
    return getTranslatedPoint(new Point(e.targetTouches.first.page.x, e.targetTouches.first.page.y));
  }
  
  Point getTranslatedPoint(Point p) {
    var element = canvasElement;
    var cOffsetX = 0, cOffsetY = 0;
    if (element.offsetParent != null) {
      do {
        cOffsetX += element.offsetLeft;
        cOffsetY += element.offsetTop;
      } while ((element = element.offsetParent) != null);
    }
    int x = p.x - cOffsetX; 
    int y = p.y - cOffsetY;
    return new Point(x, y);  
  }
  
  void redraw() {
    int w = (context.canvas.width * (1 / scale)).toInt();
    int h = (context.canvas.height * (1 / scale)).toInt();
    context.clearRect(normX(0), normY(0), w, h);
    
    Point hoveredVertexStart;
    Point hoveredVertexEnd;
    Object hoveredVertexFromNode;
    Object hoveredVertexToNode;
    
    // render vertices
    for (int i = 0; i < graphModel.getNodeCount(); i++) {
      Object node = graphModel.getNode(i);
      // render vertices
      for (int j = 0; j < graphModel.getVertexCount(node); j++) {
        // We're not doing anything with this vertex info yet, but will be useful later on.
        Object vertex = graphModel.getVertex(node, j);
        Object target = graphModel.getTargetNode(node, j);
        
        Point nodeConnectionPoint = getConnectionPoint(node, getNodeCenter(target)); 
        Point targetConnectionPoint = getConnectionPoint(target, getNodeCenter(node)); 
        
        if (vertex == hoveredVertex) {
          // we'll draw this one later
          hoveredVertexStart = nodeConnectionPoint;
          hoveredVertexEnd = targetConnectionPoint;
          hoveredVertexFromNode = node;
          hoveredVertexToNode = target;
        } else {
          // TODO only paint vertex if it actually crosses the viewport
          // first attempt: at lease one should be in the viewport
          if (isPointInViewPort(nodeConnectionPoint) || isPointInViewPort(targetConnectionPoint)) {
            vertexPainter.paint(context, vertex, nodeConnectionPoint, targetConnectionPoint);
          } 
        }
      }
    }
  
    // render nodes
    List selectedNodes = new List();
    List hoverNodes = new List();
    for (int i = 0; i < graphModel.getNodeCount(); i++) {
      Object node = graphModel.getNode(i);
      Point position = graphModel.getPosition(node);
      if (selectionManager.isSelected(node)) {
        // pospone drawing so selections will end on top
        selectedNodes.add(node);
      } else {
        if (hoveredVertex == null || ((hoveredVertexFromNode != node) && hoveredVertexToNode != node)) {
          if (isNodeInViewPort(context, node)) {
            nodePainter.paint(context, node, graphModel.getPosition(node));
          }
        } else {
          // part of hovered vertex
          hoverNodes.add(node);
        }
      }
    }  
    for (Object selectedNode in selectedNodes) {
      if (isNodeInViewPort(context, selectedNode)) {
        nodePainter.paintSelected(context, selectedNode, graphModel.getPosition(selectedNode));
      }
    }
    // draw hovered vertex and nodes on top of everything
    for (Object selectedNode in hoverNodes) {
      if (isNodeInViewPort(context, selectedNode)) {
        nodePainter.paintHovered(context, selectedNode, graphModel.getPosition(selectedNode));
      }
    }
    if (hoveredVertex != null) {
        vertexPainter.paintHovered(context, hoveredVertex, hoveredVertexStart, hoveredVertexEnd, normPoint(mouseLocation));
    }
  }
  
  bool isNodeInViewPort(context, node) {
    Point position = graphModel.getPosition(node);
    int width = getNodeWidth(context, node);
    int height = getNodeHeight(context, node);
    Point upperLeft = new Point(position.x, position.y);
    Point upperRight = new Point(position.x + width, position.y);
    Point lowerLeft = new Point(position.x, position.y + height);
    Point lowerRight = new Point(position.x + width, position.y + height);
    return isPointInViewPort(upperLeft) || isPointInViewPort(upperRight) 
        || isPointInViewPort(lowerLeft) || isPointInViewPort(lowerRight);
  }
  
  bool isPointInViewPort(Point point) {
    int x = normX(0);
    int y = normY(0);
    int w = (context.canvas.width * (1 / scale)).toInt();
    int h = (context.canvas.height * (1 / scale)).toInt();
    return point.x > x && point.x < (x + w) && point.y > y && point.y < (y + h);
  }
  
  bool matchesCurrentMouseLocation(Point from, Point to) {
    // y = ax + b
    if (mouseLocation != null) {
      Point nMouseLocation = normPoint(mouseLocation);
      // are we in range of the vertex anyway, should be between x and y ?
      if (nMouseLocation.x >= min(from.x, to.x) && nMouseLocation.x <= max(from.x, to.x)
          && nMouseLocation.y >= min(from.y, to.y) && nMouseLocation.y <= max(from.y, to.y)) {
      
        int diffX = (to.x - from.x);
        int diffY = (to.y - from.y);
        if (diffX != 0) {
          double a = (to.y - from.y) / diffX;
          double b = from.y - (a * from.x);
          double expectedY = a * nMouseLocation.x + b;
          // TODO Could use improvement on tolerance
          if (nMouseLocation.y < (expectedY.toInt() + 5) &&
              nMouseLocation.y > (expectedY.toInt() - 5)) {
            return true;
          }
        } 
      }
  
    }
    return false;
  }
  
  Point getConnectionPoint(Object node, Point targetCenterPoint) {
    Point nodeCenter = getNodeCenter(node);
    
    double centerX = (nodeCenter.x as int).toDouble();
    double centerY = (nodeCenter.y as int).toDouble();  
    double x = centerX;
    double y = centerY;
    double width = getNodeWidth(context, node).toDouble();
    double height = getNodeHeight(context, node).toDouble();
    double maxX = (nodeCenter.x + (width / 2));
    double minX = (nodeCenter.x - (width / 2));
    double maxY = (nodeCenter.y + (height / 2));
    double minY = (nodeCenter.y - (height / 2));
    
    // center of other node is below
    double slope = (centerY - targetCenterPoint.y) / (centerX - targetCenterPoint.x);
    double offset = centerY - (centerX * slope);
  
    if (targetCenterPoint.y > y) {
      y = maxY;
    } else {
      y = minY;
    }
    x = (y - offset) / slope;
    if (x >= maxX) {
      x = maxX;
      y = slope * x + offset;
    } else if (x <= minX) {
      x = minX;
      y = slope * x + offset;
    } 
    if (((targetCenterPoint.x > centerX) && (x == minX ))) {
      x = maxX;
    }
    if (((targetCenterPoint.x < centerX) && (x == maxX ))) {
      x = minX;
    }
    if (x == double.NEGATIVE_INFINITY) {
      x = minX;
    }
    if (x == double.INFINITY) {
      x = maxX;
    }   
    // TODO: figure out why == double.NAN does not work.
    if (x.toString() == "NaN") {
      x = centerX;
    }
    return new Point(x.toInt(), y.toInt());
  }
  
  Point getNodeCenter(Object node) {
    Point nodePoint = graphModel.getPosition(node);
    int nodeWidth = getNodeWidth(context, node);
    int nodeHeight = getNodeHeight(context, node);
    
    int centerX = nodePoint.x + (nodeWidth ~/ 2).toInt();
    int centerY = nodePoint.y + (nodeHeight ~/ 2).toInt();  
    return new Point(centerX, centerY);
  }
  
  Object getNode(Point clickPosition) {
    for (int i = 0; i < graphModel.getNodeCount(); i++) {
      Object node = graphModel.getNode(i);
      Point nodePosition = graphModel.getPosition(node);
      int width = getNodeWidth(context, node);
      int height = getNodeHeight(context, node);
      // is the location in these bounds ?
      int x = normX(clickPosition.x);
      int y = normY(clickPosition.y);
      if (x >= nodePosition.x && x <= (nodePosition.x + width)
          && y >= nodePosition.y && y <= (nodePosition.y + height)) {
        return node;
      }
    }
  }
  
  Point normPoint(Point p) {
    return new Point(normX(p.x), normY(p.y));
  }
  
  // translate point to before transformation
  int normX(int x) {
    return ((x * (1 / scale)) - (offsetX * (1 / scale)) as double).toInt();
  }
  
  // translate point to before transformation
  int normY(int y) {
    return ((y * (1 / scale)) - (offsetY * (1 / scale)) as double).toInt();
  }
  
  List getNodes(Point start, Point end) {
    List nodes = new List();
    int x = min(normX(start.x), normX(end.x));
    int y = min(normY(start.y), normY(end.y));
    int width = max(normX(start.x), normX(end.x)) - x;
    int height = max(normY(start.y), normY(end.y)) - y;
    for (int i = 0; i < graphModel.getNodeCount(); i++) {
      Object node = graphModel.getNode(i);
      Point nodePosition = graphModel.getPosition(node);
      int nodeWidth = getNodeWidth(context, node);
      int nodeHeight = getNodeHeight(context, node);
      // does the node 'touch' the area
      if (nodePosition.x >= x && nodePosition.x <= (x + width)
          && nodePosition.y >= y && nodePosition.y <= (y + height)) {
        // ignore the bounds of the node for now, TODO: improve
        nodes.add(node);
      }
    }
    return nodes;
  }
  
  int getNodeWidth(context, node) {
    if (boundsCache.containsKey(node)) {
      return boundsCache[node].x;  
    } else {
      int width = nodePainter.getWidth(context, node);
      int height = nodePainter.getHeight(context, node);
      boundsCache.putIfAbsent(node, () => new Point(width, height));
      return width;
    }
  }
  
  int getNodeHeight(context, node) {
    if (boundsCache.containsKey(node)) {
      return boundsCache[node].y;  
    } else {
      int width = nodePainter.getWidth(context, node);
      int height = nodePainter.getHeight(context, node);
      boundsCache.putIfAbsent(node, () => new Point(width, height));
      return height;
    }    
  }
  
  void reload() {
    if (inlineData != null) {
      jsonToGraphModel(inlineData);
    } else {
      var url = config["load"]["url"];
      load(url);
    }
  }
  
  void load(String url) {
    var request = HttpRequest.getString(url).then(onDataLoaded);
  }
  
  // print the raw json response text from the server
  void onDataLoaded(String responseText) {
    var jsonString = responseText;
    Map data = JSON.decode(jsonString); 
    // add nodes
    jsonToGraphModel(data);
  }
  
  void jsonToGraphModel(json) {
    graphModel.clear();
    Map nodes = new Map();
    for (String nodeId in json.keys) {
      GraphNode node = new GraphNode(nodeId);
      int x = json[nodeId]["x"].toInt();
      int y = json[nodeId]["y"].toInt();
      graphModel.addNode(node);
      setPosition(node, x, y);
      nodes.putIfAbsent(nodeId, () => node);
    }
    
    // add dependencies
    for (String nodeId in json.keys) {
      List dependencies = json[nodeId]["outgoing-dependencies"];
      if (dependencies != null) {
        for (Map dependency in dependencies) {
          String source = dependency["source"];
          String target = dependency["target"];
          GraphNode sourceNode = nodes[source];
          GraphNode targetNode = nodes[target];
          sourceNode.addRelation(targetNode);
        }
      }
    }
    window.requestAnimationFrame(applyLayout);    
  }
  
  void applyLayout(num _) {
    redraw();  
  }
  
  void setPosition(Object node, int x, int y) {
    graphModel.setPosition(node, x, y);
    boundsCache.remove(node);
  }
  
}

class MouseMode {
  static const SELECT = const MouseMode._(0);
  static const MOVE = const MouseMode._(1);

  static get values => [SELECT, MOVE];

  final int value;

  const MouseMode._(this.value);
}
