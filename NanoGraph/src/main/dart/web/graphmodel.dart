/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
library GraphModel;

import 'dart:math';

abstract class GraphModel {
  
  int getNodeCount();
  Object getNode(int position);
  int getVertexCount(Object node);
  Object getVertex(Object node, int position);
  Object getTargetNode(Object node, int vertexPosition);
  void setPosition(Object node, int x, int y);
  Point getPosition(Object node);
  
}

class DefaultGraphModel implements GraphModel {
  
  var nodes = new List();
  var positions = new Map<Object, Point>();
  
  DefaultGraphModel() {

  }
  
  void addNode(GraphNode node) {
    nodes.add(node);
  }
  
  void clear() {
    nodes.clear();
    positions.clear();
  }
  
  int getNodeCount() {
    return nodes.length;
  }
  
  Object getNode(int position) {
    return nodes.elementAt(position);
  }
  
  int getVertexCount(Object node) {
    GraphNode graphNode = node as GraphNode;
    return graphNode.getRelationCount();
  }
  
  Object getVertex(Object node, int position) {
    GraphNode graphNode = node as GraphNode;
    return graphNode.getRelation(position);
  }
  
  Object getTargetNode(Object node, int vertexPosition) {
    GraphNode graphNode = node as GraphNode;
    Object relationObject = graphNode.getRelation(vertexPosition);
    GraphVertex vertex = relationObject as GraphVertex;
    return vertex.getTargetNode();
  }
  
  void setPosition(Object node, int x, int y) {
    Point position;
    if (positions.containsKey(node)) {
      positions.remove(node);
    } 
    positions.putIfAbsent(node, () => new Point(x,y));
  }
  
  Point getPosition(Object node) {
    return positions[node];
  }
  
}

class GraphNode {
  
  String label;
  List relations = new List();
  
  GraphNode(String label) {
    this.label = label;
  }
  
  void addRelation(Object target) {
    relations.add(new GraphVertex(this, target));  
  }
  
  int getRelationCount() {
    return relations.length;
  }
  
  GraphVertex getRelation(int position) {
    return relations.elementAt(position);
  }
  
  String getLabel() {
    return label;  
  }
  
  String toString() {
    return label;
  }
}

class GraphVertex {
  
  Object source;
  Object target;
  
  GraphVertex(Object source, Object target) {
    this.source = source;
    this.target = target;
  }
  
  Object getTargetNode() {
    return target;
  }
}