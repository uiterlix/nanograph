/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
library NodePainter;

import 'dart:math';
import 'dart:html';

class NodePainter {
  
  int defaultWidth = 60;
  int defaultHeight = 20;
  
  void paint(CanvasRenderingContext2D context, Object node, Point position) {
    int width = getWidth(context, node);
    int height = getHeight(context, node);
    
    context.beginPath();
    context.lineWidth = 1;
    context.fillStyle = "white";
    context.fillRect(position.x, position.y, width, height);
    context.strokeStyle = "#FFA500"; 
    context.rect(position.x, position.y, width, height);
    context.stroke();
    context.closePath();
    
    paintText(context, node, position.x, position.y, width);
    
  }
  
  void paintSelected(CanvasRenderingContext2D context, Object node, Point position) {
    int width = getWidth(context, node);
    int height = getHeight(context, node);
    
    context.beginPath();
    context.lineWidth = 2;
    context.fillStyle = "white";
    context.fillRect(position.x, position.y, width, height);
    context.strokeStyle = "red"; 
    context.rect(position.x, position.y, width, height);   
    context.stroke();
    context.closePath();
    
    paintText(context, node, position.x, position.y, width);
    
  }
  
  void paintHovered(CanvasRenderingContext2D context, Object node, Point position) {
    int width = getWidth(context, node);
    int height = getHeight(context, node);
    
    context.beginPath();
    context.lineWidth = 2;
    context.fillStyle = "white";
    context.fillRect(position.x, position.y, width, height);
    context.strokeStyle = "black"; 
    context.rect(position.x, position.y, width, height);   
    context.stroke();
    context.closePath();
    
    paintText(context, node, position.x, position.y, width);
    
  }
  
  void paintText(CanvasRenderingContext2D context, Object node, int x, int y, width) {
    context.beginPath();
    context.lineWidth = 1;
    context.strokeStyle = 'black';
    context.fillStyle = 'black';
    TextMetrics metrics = context.measureText(node.toString());
    int offset = (((width - metrics.width) / 2) as double).toInt();
    context.fillText(node.toString(), x + offset, y + 14);
//    context.stroke();
    context.closePath();
  }
  
  int getWidth(CanvasRenderingContext2D context, Object node) {
    TextMetrics metrics = context.measureText(node.toString());
    double width = metrics.width + 10;
    if (width < defaultWidth) {
      return defaultWidth;
    }
    return width.toInt();
  }
  
  int getHeight(CanvasRenderingContext2D context, Object node) {
    return defaultHeight;
  }
}