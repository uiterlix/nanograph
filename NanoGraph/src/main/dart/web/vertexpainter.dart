/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
library VertexPainter;

import 'dart:math';
import 'dart:html';

class VertexPainter {
  
  void paint(CanvasRenderingContext2D context, Object vertex, Point from, Point to) {
    paintVertex(context, vertex, from, to, "#7AA1E6");
  }
  
  void paintHovered(CanvasRenderingContext2D context, Object vertex, Point from, Point to, Point mouseLocation) {
    paintVertex(context, vertex, from, to, "black");
    
    // paint tooltip
    TextMetrics metrics = context.measureText(vertex.toString());
    // draw rect
    context.beginPath();
    context.fillStyle = 'yellow';
    context.fillRect(mouseLocation.x + 15, mouseLocation.y, metrics.width + 10, 20);
    context.closePath();
    
    context.beginPath();
    context.lineWidth = 1;
    context.strokeStyle = 'black';
    context.fillStyle = 'black';
    
    
    context.fillText(vertex.toString(), mouseLocation.x + 20, mouseLocation.y + 12);
    context.closePath();
  }
  
  void paintVertex(CanvasRenderingContext2D context, Object vertex, Point from, Point to, String color) {
    context.beginPath();
    context.lineWidth = 1;
    context.strokeStyle = color; 

    context.moveTo(from.x, from.y);
    context.lineTo(to.x, to.y);
    
    double deltaY = (to.y as int).toDouble() - (from.y as int).toDouble();
    double deltaX = (to.x as int).toDouble() - (from.x as int).toDouble();
    double angle = atan2(deltaY, deltaX);
    
    // right arrowhead
    context.save();
    context.translate(to.x, to.y);
    context.rotate(angle);
    context.lineTo(-10, 5);
    // back to line end
    context.lineTo(0, 0);
    // left arrowhead
    context.lineTo(-10, -5);
    context.rotate(angle * -1);
    context.restore();
    
    String hoverText = null;
    context.stroke();
    context.closePath();
  }
  
}