/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.components;

import org.nanograph.drawing.NanoGraph;
import org.nanograph.interaction.InteractionManager;
import org.nanograph.model.GraphModel;

public interface NanoGraphPanel {
	
    public final static double ZOOM_STEP = 0.05; 
    
	public NanoGraph getNanoGraph();

	public InteractionManager getInteractionManager();

	public double getZoomFactor();

	public void setZoomFactor(double zoomFactor);
	
	public void setModel(GraphModel g);

	public void repaint();
	
}
