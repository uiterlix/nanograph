/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.components.applet;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.StringTokenizer;

import javax.swing.JApplet;
import javax.swing.border.LineBorder;

import org.nanograph.components.swing.SwingNanoGraphPanel;
import org.nanograph.drawing.background.GridBackGround;
import org.nanograph.drawing.layout.CircularLayoutAlgorithm;
import org.nanograph.drawing.layout.ForceDirectedLayoutAlgorithm;
import org.nanograph.drawing.noderenderer.DefaultNodeRenderer;
import org.nanograph.model.DefaultGraphModel;

/**
 * GraphApplet.java
 *
 * @author Jeroen van Grondelle
 */
public class NanoGraphApplet extends JApplet {
	
	SwingNanoGraphPanel p = null;
	
	public void init() {
	    
		DefaultGraphModel g = new DefaultGraphModel();
		p = new SwingNanoGraphPanel();
		p.setBorder(new LineBorder(Color.darkGray, 1, true));
		p.addMouseListener(new MouseAdapter(){
			
			public void mouseClicked(MouseEvent e)
			{
				p.getNanoGraph().setLayout(new ForceDirectedLayoutAlgorithm(p));
			}
		}
		);
		
		p.setModel(g);
		
		if (getParameter("nodes") != null) 
		{
			// parse graph from parameters
			//
			StringTokenizer st = new StringTokenizer(
					this.getParameter("nodes"), ",");

			while (st.hasMoreElements())
				g.addNode(st.nextToken());

			st = new StringTokenizer(this.getParameter("edges"), ",-");

			while (st.hasMoreElements())
				g.addEdge("edge", st.nextToken(), st.nextToken());
		} 
		else 
		{
			// default graph
			g.addNode("Java");
			g.addNode("Python");
			g.addNode("C++");
			g.addNode("Perl");
			g.addNode("C#");
			g.addNode("Programming Language");

			g.addEdge("is a", "Java", "Programming Language");
			g.addEdge("is a", "Java", "Python");
			g.addEdge("is a", "C#", "Programming Language");
			g.addEdge("is a", "C++", "Programming Language");
			g.addEdge("is a", "Perl", "Programming Language");
			g.addEdge("is a", "Python", "Programming Language");
		}
		
		p.getNanoGraph().setLayout(new CircularLayoutAlgorithm(200));
		DefaultNodeRenderer r = new DefaultNodeRenderer();
//		r.setBodyColor(new GradientPaint(0,0,Color.YELLOW, 600, 600, Color.RED));
		p.getNanoGraph().registerNodeRenderer(String.class, r);
		p.getNanoGraph().setBackground(new GridBackGround());
		
		this.getContentPane().add(p);
	}
}