/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.components.image;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.OutputStream;
import java.io.Writer;

import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.nanograph.components.swing.SwingGraphicsAdapter;
import org.nanograph.drawing.NanoGraph;
import org.nanograph.drawing.graphicsadapter.GraphicsAdapter;
import org.nanograph.model.GraphModel;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

/**
 * GraphImage.java
 * 
 * @author Jeroen van Grondelle
 */

// TODO: scale zit nog niet in graphicsadapter
public class NanoGraphImage {

    public final static String SVG_NAMESPACE = "http://www.w3.org/2000/svg";
    public final static String XLINK_NAMESPACE = "http://www.w3.org/1999/xlink";    
    public final static String SVG_NAMESPACE_PREFIX = "svg";
    
    private NanoGraph nanograph = null;

    int height = 0, width = 0;

    public NanoGraph getNanograph() {
        return nanograph;
    }

    public void setNanograph(NanoGraph nanograph) {
		this.nanograph = nanograph;
	}

	public NanoGraphImage(int width, int height) {
        nanograph = new NanoGraph();

        this.height = height;
        this.width = width;
    }

    public NanoGraph getNanoGraph() {
        return nanograph;
    }

    public void setModel(GraphModel model) {
        nanograph.setModel(model);
    }

    /**
     * Paint nanograph to Graphics2D object. Scales down to specified size if
     * necessary, never scales up.
     * 
     * @param g
     */
    private void paintImage(GraphicsAdapter g) {
        nanograph.paintGraph(g);
    }
    
    private double getScale(NanoGraph nanoGraph, GraphicsAdapter g) {
    	double scale = 1.0;
    	
        double xscale = 1.0;
        double yscale = 1.0;

        nanograph.calculateBounds(g);

        double prefWidth = nanograph.getGraphWidth();
        double prefHeight = nanograph.getGraphHeight();

        if (prefWidth > width) {
            xscale = (width / prefWidth);
        }
        if (prefHeight > height) {
            yscale = (height / prefHeight);
        }
        scale = (xscale < yscale) ? xscale : yscale;
        
        return scale;
    }

    /**
     * Store image as JPEG using AWT JPEG Encoder.
     * 
     * @param out
     */
    public void storeAsJpeg(OutputStream out) {
        BufferedImage myImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = myImage.createGraphics();
    	g.setPaint(Color.WHITE);
    	g.fillRect(0,0, width, height);
        
        SwingGraphicsAdapter sg = new SwingGraphicsAdapter(g);
        double scale = getScale(nanograph, sg);
        g.scale(scale, scale);        
        paintImage(sg);
        try {
            JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
            encoder.encode(myImage);
            out.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Store image as SVG to a Writer. Implemented using the SVG graphics adapter
     * 
     * @param out
     */
    public void storeAsSVG(Writer out) {
    	try {
            OutputFormat outputFormat = new OutputFormat("xml", "UTF-8", true);
    		
            XMLSerializer serializer = new XMLSerializer(out, outputFormat);
            serializer.setNamespaces(true);
            serializer.startDocument();
            storeAsSVG(serializer);
            serializer.endDocument();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Store image as an SVG document to a ContentHandler.
     * 
     * Implemented using storeAsSVG(Writer) method.
     * 
     * @param handler
     */
    public void storeAsSVG(ContentHandler handler) {
		try {
			SVGGraphicsAdapter svgGraphicsAdapter = new SVGGraphicsAdapter(handler);

			handler.startPrefixMapping("svg", SVG_NAMESPACE);
			handler.startPrefixMapping("xlink", XLINK_NAMESPACE);
			AttributesImpl attributes = new AttributesImpl();
			attributes.addAttribute("", "width", "", "", String.valueOf(width));
			attributes.addAttribute("", "height", "", "", String.valueOf(height));
			handler.startElement(SVG_NAMESPACE, "svg", "", attributes);
			
			nanograph.calculateBounds(svgGraphicsAdapter);
			double scale = getScale(nanograph, svgGraphicsAdapter);
			attributes = new AttributesImpl();
			
//			attributes.addAttribute("", "width", "", "", String.valueOf(nanograph.getGraphWidth()));
//			attributes.addAttribute("", "height", "", "", String.valueOf(nanograph.getGraphHeight()));
  			
			if (scale != 1.0) {	
				attributes.addAttribute("", "transform", "", "", "scale(" + scale + ")");
			}
			
			handler.startElement(SVG_NAMESPACE, "g", "svg:g", attributes);
			nanograph.paintGraph(svgGraphicsAdapter);
			handler.endElement(SVG_NAMESPACE, "g", "svg:g");
			handler.endElement(SVG_NAMESPACE, "svg", "svg:svg");
			handler.endPrefixMapping("xlink");
			handler.endPrefixMapping("svg");
//			handler.endDocument();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}    		

    }
    
    public static String getLocalName(String qname) {
    	return SVG_NAMESPACE_PREFIX + ":" + qname;
    }

}