/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.components.image;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

import org.nanograph.components.swing.SwingResourceManager;
import org.nanograph.drawing.graphicsadapter.AbstractGraphicsAdapter;
import org.nanograph.drawing.graphicsadapter.GraphicsAdapter;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

public class SVGGraphicsAdapter extends AbstractGraphicsAdapter implements GraphicsAdapter {

    public final static String SVG_NAMESPACE = "http://www.w3.org/2000/svg";
    public final static String XLINK_NAMESPACE = "http://www.w3.org/1999/xlink";  
    
	ContentHandler handler;
	int currentLineStyle;
	
	// Graphics2D backing object for font calculations
	Graphics2D g2d;
	
	public SVGGraphicsAdapter(ContentHandler handler) {
		this.handler = handler;
		Image img = new BufferedImage(1,1,BufferedImage.TYPE_INT_RGB);
		g2d = (Graphics2D) img.getGraphics();
	}
	
	public void handleSetAlpha(int alpha) {

	}

	public void handleSetColor(String color) {
		// TODO Auto-generated method stub

	}

	public void handleSetFillColor(String color) {
		// TODO Auto-generated method stub
	}

	public void handleSetFillStyle(String cssAttribute, String value) {
		// TODO Auto-generated method stub

	}

	public void handleSetFont(String fontFace, int fontSize, int style) {
		g2d.setFont(SwingResourceManager.getFont(fontFace, style, fontSize));
	}

	public void handleSetLineStyle(int lineStyle) {
		this.currentLineStyle = lineStyle;
	}
	
	private int getPreferredLineStyle() {
//		return (currentLineStyle != getLineStyle()) ? currentLineStyle : getLineStyle();
		return currentLineStyle;
	}

	public void drawImage(String uri, double x, double y) {
		Image image = SwingResourceManager.getImage(uri);
		AttributesImpl attributes = new AttributesImpl();
		attributes.addAttribute("", "x", "", "", String.valueOf(x));
		attributes.addAttribute("", "y", "", "", String.valueOf(y));
		attributes.addAttribute("", "width", "", "", String.valueOf(image.getWidth(null)));
		attributes.addAttribute("", "height", "", "", String.valueOf(image.getHeight(null)));
		attributes.addAttribute("", "xlink:href", "", "", SVGResourceManager.getImageAsBase64EncodedString(uri));
		try {
			handler.startElement(SVG_NAMESPACE, "image", "svg:image", attributes);
			handler.endElement(SVG_NAMESPACE, "image", "svg:image");
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public void drawImage(String id, byte[] data, double x, double y) {
		Image image = SwingResourceManager.getImage(id, data);
		AttributesImpl attributes = new AttributesImpl();
		attributes.addAttribute("", "x", "", "", String.valueOf(x));
		attributes.addAttribute("", "y", "", "", String.valueOf(y));
		attributes.addAttribute("", "width", "", "", String.valueOf(image.getWidth(null)));
		attributes.addAttribute("", "height", "", "", String.valueOf(image.getHeight(null)));
		attributes.addAttribute("", "xlink:href", "", "", SVGResourceManager.getImageAsBase64EncodedString(id, data));
		try {
			handler.startElement(SVG_NAMESPACE, "image", "svg:image", attributes);
			handler.endElement(SVG_NAMESPACE, "image", "svg:image");
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	

	public void drawLine(double x1, double y1, double x2, double y2) {
		AttributesImpl attributes = new AttributesImpl();
		attributes.addAttribute("", "x1", "", "", String.valueOf(x1));
		attributes.addAttribute("", "x2", "", "", String.valueOf(x2));
		attributes.addAttribute("", "y1", "", "", String.valueOf(y1));
		attributes.addAttribute("", "y2", "", "", String.valueOf(y2));
		attributes.addAttribute("", "stroke", "", "", getColor());
		attributes.addAttribute("", "style", "", "", SVGResourceManager.getLineStyle(getPreferredLineStyle()));
		try {
			handler.startElement(SVG_NAMESPACE, "line", "svg:line", attributes);
			handler.endElement(SVG_NAMESPACE, "line", "svg:line");
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void drawArc(double x, double y, int arcWidth, int arcHeight, int startAngle, int arcAngle) {
		// TODO: implement this method
	}

	public void drawOval(double cx, double cy, double rx, double ry) {
		// translating origin and size to center and radius
		AttributesImpl attributes = new AttributesImpl();
		attributes.addAttribute("", "cx", "", "", String.valueOf(cx + rx / 2));
		attributes.addAttribute("", "cy", "", "", String.valueOf(cy + ry / 2));
		attributes.addAttribute("", "rx", "", "", String.valueOf(rx / 2));
		attributes.addAttribute("", "ry", "", "", String.valueOf(ry / 2));
		attributes.addAttribute("", "fill", "", "", "none");
		attributes.addAttribute("", "stroke", "", "", getColor());
		try {
			handler.startElement(SVG_NAMESPACE, "ellipse", "svg:ellipse", attributes);
			handler.endElement(SVG_NAMESPACE, "ellipse", "svg:ellipse");
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void drawPolygon(int[] values, int[] values2) {
		AttributesImpl attributes = new AttributesImpl();
		
		attributes.addAttribute("", "points", "", "", getPolyPoints(values, values2));
		attributes.addAttribute("", "fill", "", "", "none");
		attributes.addAttribute("", "stroke", "", "", getColor());
		try {
			handler.startElement(SVG_NAMESPACE, "polygon", "svg:polygon", attributes);
			handler.endElement(SVG_NAMESPACE, "polygon", "svg:polygon");
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	

	}

	public void drawRectangle(double x, double y, double width, double height) {
		AttributesImpl attributes = new AttributesImpl();
		attributes.addAttribute("", "x", "", "", String.valueOf(x));
		attributes.addAttribute("", "y", "", "", String.valueOf(y));
		attributes.addAttribute("", "width", "", "", String.valueOf(width));
		attributes.addAttribute("", "height", "", "", String.valueOf(height));
		attributes.addAttribute("", "fill", "", "", "none");
		attributes.addAttribute("", "stroke", "", "", getColor());
		try {
			handler.startElement(SVG_NAMESPACE, "rect", "svg:rect", attributes);
			handler.endElement(SVG_NAMESPACE, "rect", "svg:rect");
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void drawRoundRectangle(double x, double y, double width, double height,
			double j, double k) {
		AttributesImpl attributes = new AttributesImpl();
		attributes.addAttribute("", "x", "", "", String.valueOf(x));
		attributes.addAttribute("", "y", "", "", String.valueOf(y));
		attributes.addAttribute("", "width", "", "", String.valueOf(width));
		attributes.addAttribute("", "height", "", "", String.valueOf(height));
		attributes.addAttribute("", "rx", "", "", String.valueOf(j/2));
		attributes.addAttribute("", "fill", "", "", "none");
		attributes.addAttribute("", "stroke", "", "", getColor());
		try {
			handler.startElement(SVG_NAMESPACE, "rect", "svg:rect", attributes);
			handler.endElement(SVG_NAMESPACE, "rect", "svg:rect");
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		

	}

	public void drawString(String string, double x1, double x2) {
		AttributesImpl attributes = new AttributesImpl();
		attributes.addAttribute("", "x", "", "", String.valueOf(x1));
		double y = x2 + stringHeight(string) / 3 * 2;

		attributes.addAttribute("", "y", "", "", String.valueOf(y));
		attributes.addAttribute("", "font-family", "", "", getFontFace());
		attributes.addAttribute("", "font-size", "", "", String.valueOf(getFontSize()) + "px");
		attributes.addAttribute("", "font-style", "", "", SVGResourceManager.getFontStyle(getFontStyle()));
		attributes.addAttribute("", "font-weight", "", "", SVGResourceManager.getFontWeight(getFontStyle()));
		attributes.addAttribute("", "fill", "", "", getColor());
		try {
			handler.startElement(SVG_NAMESPACE, "text", "svg:text", attributes);
			char[] chars = string.toCharArray();
			handler.characters(chars, 0, chars.length);
			handler.endElement(SVG_NAMESPACE, "text", "svg:text");
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	

	}

	public void fillOval(double cx, double cy, double rx, double ry) {
		// translating origin and size to center and radius
		AttributesImpl attributes = new AttributesImpl();
		attributes.addAttribute("", "cx", "", "", String.valueOf(cx + rx / 2));
		attributes.addAttribute("", "cy", "", "", String.valueOf(cy + ry / 2));
		attributes.addAttribute("", "rx", "", "", String.valueOf(rx / 2));
		attributes.addAttribute("", "ry", "", "", String.valueOf(ry / 2));
		attributes.addAttribute("", "fill", "", "", getFillColor());
		attributes.addAttribute("", "stroke", "", "", getFillColor());
		try {
			handler.startElement(SVG_NAMESPACE, "ellipse", "svg:ellipse", attributes);
			handler.endElement(SVG_NAMESPACE, "ellipse", "svg:ellipse");
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void fillPolygon(int[] values, int[] values2) {
		AttributesImpl attributes = new AttributesImpl();
		
		attributes.addAttribute("", "points", "", "", getPolyPoints(values, values2));
		attributes.addAttribute("", "fill", "", "", getFillColor());
		attributes.addAttribute("", "stroke", "", "", getColor());
		try {
			handler.startElement(SVG_NAMESPACE, "polygon", "svg:polygon", attributes);
			handler.endElement(SVG_NAMESPACE, "polygon", "svg:polygon");
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			

	}
	
	private String getPolyPoints(int[] valuesx, int[] valuesy) {
		StringBuffer result = new StringBuffer();
		for (int i = 0; i < valuesx.length; i++) {
			result.append(valuesx[i]);
			result.append(",");
			result.append(valuesy[i]);
			result.append(" ");
		}
		return result.toString();
	}

	public void fillRectangle(double x, double y, double width, double height) {
		AttributesImpl attributes = new AttributesImpl();
		attributes.addAttribute("", "x", "", "", String.valueOf(x));
		attributes.addAttribute("", "y", "", "", String.valueOf(y));
		attributes.addAttribute("", "width", "", "", String.valueOf(width));
		attributes.addAttribute("", "height", "", "", String.valueOf(height));
		attributes.addAttribute("", "fill", "", "", getFillColor());
		attributes.addAttribute("", "stroke", "", "", getFillColor());
		try {
			handler.startElement(SVG_NAMESPACE, "rect", "svg:rect", attributes);
			handler.endElement(SVG_NAMESPACE, "rect", "svg:rect");
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	

	}

	public void fillRoundRectangle(double x, double y, double width, double height,
			double j, double k) {
		AttributesImpl attributes = new AttributesImpl();
		attributes.addAttribute("", "x", "", "", String.valueOf(x));
		attributes.addAttribute("", "y", "", "", String.valueOf(y));
		attributes.addAttribute("", "width", "", "", String.valueOf(width));
		attributes.addAttribute("", "height", "", "", String.valueOf(height));
		attributes.addAttribute("", "rx", "", "", String.valueOf(j/2));
		attributes.addAttribute("", "fill", "", "", getFillColor());
		attributes.addAttribute("", "stroke", "", "", getFillColor());
		try {
			handler.startElement(SVG_NAMESPACE, "rect", "svg:rect", attributes);
			handler.endElement(SVG_NAMESPACE, "rect", "svg:rect");
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		

	}

	public double imageHeight(String uri) {
		Image image = SwingResourceManager.getImage(uri);
		return image.getHeight(null);
	}

	public double imageWidth(String uri) {
		Image image = SwingResourceManager.getImage(uri);
		return image.getWidth(null);
	}

	public void setLineStyle(String cssAttribute, String value) {
		// TODO Auto-generated method stub

	}

	public double stringHeight(String string) {
		return g2d.getFontMetrics().getStringBounds(string, g2d).getHeight();
	}

	public double stringWidth(String string) {
//		return g2d.getFontMetrics().stringWidth(string);
		return g2d.getFontMetrics().getStringBounds(string, g2d).getWidth();
	}

	public double imageHeight(String id, byte[] data) {
		Image image = SwingResourceManager.getImage(id, data);
		return image.getHeight(null);
	}

	public double imageWidth(String id, byte[] data) {
		Image image = SwingResourceManager.getImage(id, data);
		return image.getWidth(null);
	}

}
