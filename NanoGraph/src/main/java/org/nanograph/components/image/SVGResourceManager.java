/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.components.image;

import java.awt.Image;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import org.nanograph.components.swing.SwingResourceManager;
import org.nanograph.drawing.graphicsadapter.GraphicsAdapter;
import org.nanograph.util.io.Base64;

public class SVGResourceManager {

	private static Map base64Images = new HashMap();
	private static Map base64dataImages = new HashMap();
	
	public static String getImageAsBase64EncodedString(String uri) {
		String imageAsString = null;
		if (base64Images.containsKey(uri)) {
			imageAsString = (String) base64Images.get(uri);
		} else {
			StringBuffer result = new StringBuffer();
			result.append("data:image/png;base64,");
			// Now add the base64 encoded data
			try {
				/* Here we use ImageIO.read because we need a image compatible with RenderedImage
				 * The ImageIcon method doesn't supply one.
				 */
				Image image = ImageIO.read(SwingResourceManager.class.getResource(uri));
				ByteArrayOutputStream bout = new ByteArrayOutputStream();
				ImageIO.write((RenderedImage)image, "png", bout);
				result.append(Base64.encodeBytes(bout.toByteArray()));
				imageAsString = result.toString();
				base64Images.put(uri, imageAsString);				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return imageAsString;
	}	
	
	public static String getImageAsBase64EncodedString(String id, byte[] data) {
		String imageAsString = null;
		if (base64dataImages.containsKey(id)) {
			imageAsString = (String) base64dataImages.get(id);
		} else {
			StringBuffer result = new StringBuffer();
			result.append("data:image/png;base64,");
			// Now add the base64 encoded data
			try {
				/* Here we use ImageIO.read because we need a image compatible with RenderedImage
				 * The ImageIcon method doesn't supply one.
				 */				
				Image image = ImageIO.read(new ByteArrayInputStream(data));
				ByteArrayOutputStream bout = new ByteArrayOutputStream();
				ImageIO.write((RenderedImage)image, "png", bout);
				result.append(Base64.encodeBytes(bout.toByteArray()));
				imageAsString = result.toString();
				base64dataImages.put(id, imageAsString);				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return imageAsString;
	}		
	
	// TODO: Accept css font style/weight as input
	public static String getFontWeight(int fontStyle) {
		String weight = "normal";
		if ((fontStyle | GraphicsAdapter.FONT_STYLE_BOLD) == fontStyle) {
			weight = "bold";
		}
		return weight;
	}
	
	public static String getFontStyle(int fontStyle) {
		String style = "normal";
		if ((fontStyle | GraphicsAdapter.FONT_STYLE_ITALIC) == fontStyle) {
			style = "italic";
		}
		return style;
	}
	
	public static String getLineStyle(int lineStyle) {
		String style="solid";
		if (lineStyle == GraphicsAdapter.LINESTYLE_DASH) {
			style = "stroke-dasharray: 7,5";
		} else if (lineStyle == GraphicsAdapter.LINESTYLE_DOT) {
			style = "stroke-dasharray: 2,3";
		}
		return style;
	}
}
