/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.components.opengl;

import java.io.IOException;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;

import org.nanograph.drawing.graphicsadapter.AbstractGraphicsAdapter;
import org.nanograph.drawing.graphicsadapter.GraphicsAdapter;
import org.nanograph.util.css.CSSSupport;

import com.sun.opengl.util.GLUT;

public class JOGLGraphicsAdapter extends AbstractGraphicsAdapter implements GraphicsAdapter {

	private static final double TUBE_DIAMETER = 0.05d;

	private static final double ARROW_HEAD_LENGTH = 0.2d;

	public static final double SPHERE_RADIUS = 0.28d;

	private static final int LINEAR = 0;

	private static final int NEAREST = 2;

	private static final int MIP = 4;

	private int mode = LINEAR;

	protected static final int OVAL_TEXTURE = 0;

	protected static final int LINE_TEXTURE = 1;

	protected static final int DOME_TEXTURE = 2;

	private String[] textureNames = new String[] {
	// texture files should be PO2 files, jpg, png, gif or any other by JOGL
	// supported format
			"textures/para.PNG", "textures/paper.jpg",
			// "textures/flowers.jpg",
			"textures/flower.jpg" };

	// storage for textures
	private int textures[] = new int[textureNames.length];

	private GL gl;

	private GLU glu;

	private GLUT glut;

	private int shapeNames = 0;

	private double sphereRadius = SPHERE_RADIUS;

	private double z = 0.0d;

	private int[] sizeToFont = new int[] {
	// init fonts
			GLUT.BITMAP_8_BY_13,//
			GLUT.BITMAP_8_BY_13,//
			GLUT.BITMAP_8_BY_13,//
			GLUT.BITMAP_8_BY_13,//
			GLUT.BITMAP_8_BY_13,//
			GLUT.BITMAP_8_BY_13,//
			GLUT.BITMAP_8_BY_13,//
			GLUT.BITMAP_8_BY_13,//
			GLUT.BITMAP_8_BY_13,//
			GLUT.BITMAP_9_BY_15,//
			GLUT.BITMAP_HELVETICA_10,//
			GLUT.BITMAP_HELVETICA_10,//
			GLUT.BITMAP_HELVETICA_12,//
			GLUT.BITMAP_HELVETICA_12,//
			GLUT.BITMAP_HELVETICA_12,//
			GLUT.BITMAP_HELVETICA_12,//
			GLUT.BITMAP_HELVETICA_12,//
			GLUT.BITMAP_HELVETICA_12,//
			GLUT.BITMAP_HELVETICA_18,//
			GLUT.BITMAP_HELVETICA_18,//
			GLUT.BITMAP_HELVETICA_18,//
			GLUT.BITMAP_HELVETICA_18,//
			GLUT.BITMAP_HELVETICA_18,//
			GLUT.BITMAP_HELVETICA_18,//
			GLUT.BITMAP_TIMES_ROMAN_24 //
	};

	public JOGLGraphicsAdapter(GL gl) {
		this.gl = gl;
		glu = new GLU();
		glut = new GLUT();
		try {
			loadGLTextures(gl);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void loadGLTextures(GL gl) throws IOException {
		// Create Textures
		gl.glGenTextures(textureNames.length, textures, 0); // 6 textures
		for (int i = 0; i < textureNames.length; i++) {
			String textureName = textureNames[i];
			TextureReader.Texture texture = TextureReader.readTexture(textureName);
			if (mode == NEAREST) {
				// Create Nearest Filtered Texture
				gl.glBindTexture(GL.GL_TEXTURE_2D, textures[i]);
				gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
				gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
				gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGB8, texture.getWidth(), texture.getHeight(), 0, GL.GL_RGB, GL.GL_UNSIGNED_BYTE, texture.getPixels());
			} else if (mode == LINEAR) {
				// Create Linear Filtered Texture
				gl.glBindTexture(GL.GL_TEXTURE_2D, textures[i]);
				gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
				gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
				gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGB8, texture.getWidth(), texture.getHeight(), 0, GL.GL_RGB, GL.GL_UNSIGNED_BYTE, texture.getPixels());
			} else if (mode == MIP) {
				// Create MipMapped Texture
				gl.glBindTexture(GL.GL_TEXTURE_2D, textures[i]);
				gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
				gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR_MIPMAP_NEAREST);
				gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGB8, texture.getWidth(), texture.getHeight(), 0, GL.GL_RGB, GL.GL_UNSIGNED_BYTE, texture.getPixels());
			}
		}
	}

	public double getSphereRadius() {
		return sphereRadius;
	}

	public void setSphereRadius(double sphereRadius) {
		this.sphereRadius = sphereRadius;
	}

	public void handleSetAlpha(int alpha) {
		// TODO: implement this method
	}

	public void handleSetColor(String color) {
		float[] newcolor = rgb2float(color);
		gl.glColor3f(newcolor[0], newcolor[1], newcolor[2]);
	}

	public void handleSetFillColor(String color) {
		float[] newcolor = rgb2float(color);
		gl.glColor3f(newcolor[0], newcolor[1], newcolor[2]);
	}

	protected static float[] rgb2float(String rgb) {
		rgb = rgb.toLowerCase();
		if (rgb.startsWith("#")) {
			rgb = rgb.substring(1);
		} else {
			rgb = CSSSupport.getCssColor(rgb).substring(1);
		}
		float red = new Float(Integer.parseInt(rgb.substring(0, 2), 16)).floatValue() / 255;
		float green = new Float(Integer.parseInt(rgb.substring(2, 4), 16)).floatValue() / 255;
		float blue = new Float(Integer.parseInt(rgb.substring(4, 6), 16)).floatValue() / 255;

		float[] result = { red, green, blue };
		return result;
	}

	public void handleSetFillStyle(String cssAttribute, String value) {
		// TODO: implement this method
	}

	public void handleSetFont(String fontFace, int fontSize, int style) {
		// TODO: implement this method
	}

	public void handleSetLineStyle(int lineStyle) {
		// TODO: implement this method
	}

	public void drawImage(String uri, double x, double y) {
		// TODO: implement this method
	}

	public void drawArc(double x, double y, int arcWidth, int arcHeight, int startAngle, int arcAngle) {
		// TODO: implement this method
	}

	public void drawLine(double x1, double y1, double x2, double y2) {
		drawLine(x1, y1, x2, y2, GraphicsAdapter.CONNECTOR_STYLE_NONE, GraphicsAdapter.CONNECTOR_STYLE_NONE, GraphicsAdapter.LINESTYLE_SOLID);
	}

	private double calclAngle(double x1, double y1, double x2, double y2) {
		double a = x2 - x1;
		double b = y2 - y1;
		double alpha = Math.atan(b / a);
		if (x2 < x1) {
			alpha += Math.PI;
		}
		double result = Math.toDegrees(alpha);
		return result + 90.0d;
	}

	private double calcLength(double x1, double y1, double x2, double y2) {
		double a = x2 - x1;
		double b = y2 - y1;
		double c = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
		return c;
	}

	public void drawOval(double x1, double y1, double x2, double y2) {
	}

	public void drawPolygon(int[] xvalues, int[] yvalues) {
	}

	public void drawRectangle(double x1, double y1, double width, double height) {
		drawLine(x1, y1, x1 + width, y1, GraphicsAdapter.CONNECTOR_STYLE_NONE, GraphicsAdapter.CONNECTOR_STYLE_NONE, GraphicsAdapter.LINESTYLE_SOLID);
		drawLine(x1 + width, y1, x1 + width, y1 + height, GraphicsAdapter.CONNECTOR_STYLE_NONE, GraphicsAdapter.CONNECTOR_STYLE_NONE, GraphicsAdapter.LINESTYLE_SOLID);
		drawLine(x1 + width, y1 + height, x1, y1 + height, GraphicsAdapter.CONNECTOR_STYLE_NONE, GraphicsAdapter.CONNECTOR_STYLE_NONE, GraphicsAdapter.LINESTYLE_SOLID);
		drawLine(x1, y1 + height, x1, y1, GraphicsAdapter.CONNECTOR_STYLE_NONE, GraphicsAdapter.CONNECTOR_STYLE_NONE, GraphicsAdapter.LINESTYLE_SOLID);
	}

	public void drawRoundRectangle(double x1, double y1, double x2, double y2, double j, double k) {
		drawRectangle(x1, y1, x2 - x1, y2 - y1);
	}

	public void drawString(String string, double x1, double x2) {
		float fromx = (float) x1 / 100;
		float fromy = (float) x2 / 100;
		// fromx -= glut.glutBitmapLength(sizeToFont[getFontSize()], string) /
		// 200;
		// fromy += 0.25f;
		gl.glPushMatrix();
		gl.glRasterPos3d(fromx, fromy, z - 0.3d); // set position
		glut.glutBitmapString(sizeToFont[getFontSize()], string);
		gl.glPopMatrix();
	}

	public void fillOval(double x1, double y1, double x2, double y2) {
		float fromx = (float) x1;
		float fromy = (float) y1;
		float tox = (float) x2;
		float toy = (float) y2;

		// texture begin
		gl.glEnable(GL.GL_TEXTURE_GEN_S); // Enable Texture Coord Generation
		// For S (NEW)
		gl.glEnable(GL.GL_TEXTURE_GEN_T); // Enable Texture Coord Generation
		// For T (NEW)
		gl.glBindTexture(GL.GL_TEXTURE_2D, textures[OVAL_TEXTURE]);
		gl.glPushMatrix();

		gl.glPushName(shapeNames++);
		GLUquadric qobj0 = glu.gluNewQuadric();

		double x = fromx + (tox / 2.0d);
		double y = fromy + (toy / 2.0d);

		gl.glTranslated(x / 100.0d, y / 100.0d, z);
		gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL.GL_FILL);
		glu.gluQuadricDrawStyle(qobj0, GLU.GLU_FILL);
		glu.gluQuadricNormals(qobj0, GLU.GLU_SMOOTH);
		glu.gluSphere(qobj0, sphereRadius - 0.1d, 16, 16);
		glu.gluDeleteQuadric(qobj0);
		gl.glPopMatrix();

		// texture end
		gl.glDisable(GL.GL_TEXTURE_GEN_S);
		gl.glDisable(GL.GL_TEXTURE_GEN_T);
	}

	public void fillPolygon(int[] values, int[] values2) {
		// TODO: implement this method
	}

	public void fillRectangle(double x1, double y1, double width, double height) {
		gl.glPushMatrix();
		gl.glPushName(shapeNames++);
		GLUquadric qobj0 = glu.gluNewQuadric();

		gl.glTranslated(((x1 + (width / 2)) / 100.0d), ((y1 + (width / 2)) / 100.0d), z);
		gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL.GL_FILL);
		glu.gluQuadricDrawStyle(qobj0, GLU.GLU_FILL);
		glu.gluQuadricNormals(qobj0, GLU.GLU_SMOOTH);
		glut.glutSolidCube((float) (width / 100.0));
		glu.gluDeleteQuadric(qobj0);
		gl.glPopMatrix();
	}

	// draw a node
	public void fillRoundRectangle(double x1, double y1, double x2, double y2, double j, double k) {
		fillOval(x1, y1, x2, y2);
		// fillRectangle(x1, y1, x2, y2);
	}

	public double imageHeight(String uri) {
		return 0.0d;
	}

	public double imageWidth(String uri) {
		return 0.0d;
	}

	public void setLineStyle(String cssAttribute, String value) {
	}

	public double stringHeight(String string) {
		return (double) sizeToFont[getFontSize()];
	}

	public double stringWidth(String string) {
		return (double) glut.glutBitmapLength(sizeToFont[getFontSize()], string);
	}

	public void drawImage(String id, byte[] data, double x, double y) {
	}

	public double imageHeight(String id, byte[] data) {
		return 0.0d;
	}

	public double imageWidth(String id, byte[] data) {
		return 0.0d;
	}

	public void drawLine(double x1, double y1, double x2, double y2, int connectorBeginStyle, int connectorEndStyle, int lineStyle) {
		float fromx = (float) (x1 / 100);
		float fromy = (float) (y1 / 100);
		float tox = (float) (x2 / 100);
		float toy = (float) (y2 / 100);
		double lineLength = calcLength(fromx, fromy, tox, toy);
		if (connectorBeginStyle == GraphicsAdapter.CONNECTOR_STYLE_ARROW || connectorBeginStyle == GraphicsAdapter.CONNECTOR_STYLE_ARROW_FILLED) {
			lineLength -= JOGLGraphicsAdapter.ARROW_HEAD_LENGTH - 0.1d;
		}
		if (connectorEndStyle == GraphicsAdapter.CONNECTOR_STYLE_ARROW || connectorEndStyle == GraphicsAdapter.CONNECTOR_STYLE_ARROW_FILLED) {
			lineLength -= JOGLGraphicsAdapter.ARROW_HEAD_LENGTH - 0.1d;
		}

		// texture begin
		gl.glEnable(GL.GL_TEXTURE_GEN_S); // Enable Texture Coord Generation
		// For S (NEW)
		gl.glEnable(GL.GL_TEXTURE_GEN_T); // Enable Texture Coord Generation
		// For T (NEW)
		gl.glBindTexture(GL.GL_TEXTURE_2D, textures[LINE_TEXTURE]);

		// draw tube
		gl.glPushMatrix();

		float[] newcolor = rgb2float(getColor());
		gl.glColor3f(newcolor[0], newcolor[1], newcolor[2]);

		gl.glPushName(shapeNames++);
		GLUquadric qobj0 = glu.gluNewQuadric();
		gl.glTranslated(fromx, fromy, z);
		// rotate 90 degrees on the x axis
		gl.glRotated(90.0f, 1.0f, 0.0f, 0.0f);
		// rotate on the y axis
		gl.glRotated(calclAngle(fromx, fromy, tox, toy), 0.0f, 1.0f, 0.0f);
		gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL.GL_FILL);
		glu.gluQuadricDrawStyle(qobj0, GLU.GLU_FILL);
		glu.gluQuadricNormals(qobj0, GLU.GLU_SMOOTH);

		if (connectorBeginStyle == GraphicsAdapter.CONNECTOR_STYLE_ARROW || connectorBeginStyle == GraphicsAdapter.CONNECTOR_STYLE_ARROW_FILLED) {
			glu.gluQuadricOrientation(qobj0, GLU.GLU_INSIDE);
			glu.gluCylinder(qobj0, 0.0d, JOGLGraphicsAdapter.TUBE_DIAMETER * 2, JOGLGraphicsAdapter.ARROW_HEAD_LENGTH, 16, 16);
			gl.glTranslated(0.0d, 0.0d, JOGLGraphicsAdapter.ARROW_HEAD_LENGTH);
			glu.gluQuadricOrientation(qobj0, GLU.GLU_OUTSIDE);
			glu.gluDisk(qobj0, 0.0d, TUBE_DIAMETER * 2, 16, 16);
		}

		glu.gluCylinder(qobj0, JOGLGraphicsAdapter.TUBE_DIAMETER, JOGLGraphicsAdapter.TUBE_DIAMETER, lineLength, 16, 16);

		glu.gluQuadricOrientation(qobj0, GLU.GLU_INSIDE);
		glu.gluDisk(qobj0, 0.0d, TUBE_DIAMETER, 16, 16);

		gl.glTranslated(0.0d, 0.0d, lineLength);
		glu.gluQuadricOrientation(qobj0, GLU.GLU_OUTSIDE);

		if (connectorEndStyle == GraphicsAdapter.CONNECTOR_STYLE_ARROW || connectorEndStyle == GraphicsAdapter.CONNECTOR_STYLE_ARROW_FILLED) {
			glu.gluCylinder(qobj0, JOGLGraphicsAdapter.TUBE_DIAMETER * 2, 0.0d, JOGLGraphicsAdapter.ARROW_HEAD_LENGTH, 16, 16);
			glu.gluQuadricOrientation(qobj0, GLU.GLU_INSIDE);
			glu.gluDisk(qobj0, 0.0d, TUBE_DIAMETER * 2, 16, 16);
		}
		glu.gluDeleteQuadric(qobj0);
		gl.glPopMatrix();

		// texture end
		gl.glDisable(GL.GL_TEXTURE_GEN_S);
		gl.glDisable(GL.GL_TEXTURE_GEN_T);
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}

	public int[] getTextures() {
		return textures;
	}
}