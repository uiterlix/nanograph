/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.components.opengl;

import java.awt.geom.Rectangle2D;

import javax.media.opengl.GL;
import javax.media.opengl.GLContext;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.opengl.GLCanvas;
import org.eclipse.swt.widgets.Display;
import org.nanograph.drawing.layout.forcedirected.ForceDirectedLayout;
import org.nanograph.interaction.InteractionManager;

public class JOGLRefresher implements Runnable, KeyListener, MouseListener {

	private GLCanvas canvas;

	private Display display;

	private GLContext context;

	private GLU glu;

	private GL gl;

	private SWTOpenGLJOGLNanoGraphPanel panel;

	// cameras
	private double spinningCameraX = 0.0d;

	private double spinningCameraY = 0.0d;

	private double spinningCameraZ = 0.0d;

	private double counter = 0;

	// moving around
	private double angle = 0.0d, deltaAngle = 0.0d;

	private double zAngle = 0.0d, deltaZAngle = 0.0d;

	private double x = 0.0d, y = 0.0d, z = 10.0d;

	private double lx = 0.0d, ly = 0.0d, lz = -1.0d;

	private double deltaMove = 0;

	private double distance = 15.0d;

	private boolean rollingCameraEnabeld = true;

	private Rectangle2D boundsForNodes;

	private static final int RENDER_MODE = 0;

	// private static final int SELECTION_MODE = 1;

	private int mode = RENDER_MODE;

	private boolean showCameras = false;

	private boolean showDome = true;

	public JOGLRefresher(SWTOpenGLJOGLNanoGraphPanel panel, GLCanvas canvas, Display display, GLContext context) {
		this.panel = panel;
		this.canvas = canvas;
		this.display = display;
		this.context = context;
		this.glu = new GLU();
		gl = context.getGL();
	}

	public void run() {
		if (!canvas.isDisposed()) {
			canvas.setCurrent();
			if (panel.getNanoGraph().getHighlightNodeSet() != null && panel.getNanoGraph().getHighlightNodeSet().size() == 1) {
				boundsForNodes = panel.getNanoGraph().getBoundsForNodes(panel.getNanoGraph().getHighlightNodeSet());
			} else {
				boundsForNodes = panel.getNanoGraph().getBoundsForNodes(panel.getNanoGraph().getNodes());
			}
			if (boundsForNodes == null) {
				boundsForNodes = new Rectangle2D.Double(0, 0, 0, 0);
			}

			counter += 0.006d;
			spinningCameraX = Math.cos(counter) * distance;
			spinningCameraY = Math.cos(counter + 0.5) * distance;
			spinningCameraZ = Math.sin(counter) * distance;
			context.makeCurrent();
			gl = context.getGL();
			gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
			gl.glMatrixMode(GL.GL_MODELVIEW);
			gl.glLoadIdentity();

			if (!isRollingCameraEnabeld()) {
				if (deltaMove != 0)
					moveMeFlat(deltaMove);
				if (deltaAngle != 0) {
					angle += deltaAngle;
					orientMe(angle);
				}
				if (deltaZAngle != 0) {
					zAngle += deltaZAngle;
					if (zAngle > 1) {
						zAngle = 1;
					} else if (zAngle < -1) {
						zAngle = -1;
					}
					orientMeZ(zAngle);
				}
			}

			if (rollingCameraEnabeld) {
				glu.gluLookAt(spinningCameraX, spinningCameraY, spinningCameraZ, 0, 0, 0, 0, -1.0d, 0);
			} else {
				glu.gluLookAt(x, y, z, x + lx, y + ly, z + lz, 0.0f, -1.0f, 0.0f);
			}
			if (mode == RENDER_MODE) {
				drawScene();
			}
			canvas.swapBuffers();
			context.release();

			if (panel.getNanoGraph().getNodes().size() == 0) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			display.asyncExec(this);
		}
	}

	private void orientMeZ(double angle) {
		ly = Math.asin(angle);
	}

	private void orientMe(double ang) {
		lx = Math.sin(ang);
		lz = -Math.cos(ang);
	}

	private void moveMeFlat(double i) {
		x = x + i * (lx) * 0.1;
		z = z + i * (lz) * 0.1;
		y = y + i * (ly) * 0.1;
	}

	private void drawScene() {
		if (showDome) {
			drawDome();
		}
		if (showCameras) {
			drawCameras();
		}

		gl.glTranslated(-boundsForNodes.getMaxX() / 200, -boundsForNodes.getMaxY() / 200, 0.0d);
		panel.drawNanograph();
	}

	private void drawCameras() {
		//
		// // texture begin
		// gl.glEnable(GL.GL_TEXTURE_GEN_S); // Enable Texture Coord Generation
		// // For S (NEW)
		// gl.glEnable(GL.GL_TEXTURE_GEN_T); // Enable Texture Coord Generation
		// // For T (NEW)
		// gl.glBindTexture(GL.GL_TEXTURE_2D,
		// panel.getGraphicsAdapter().getTextures()[2]);

		gl.glPushMatrix();
		gl.glColor3f(0.0f, 0.8f, 0.2f);
		GLUquadric qobj0 = glu.gluNewQuadric();
		gl.glTranslated(spinningCameraX, spinningCameraY, spinningCameraZ);
		gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL.GL_FILL);
		glu.gluQuadricDrawStyle(qobj0, GLU.GLU_LINE);
		glu.gluQuadricNormals(qobj0, GLU.GLU_SMOOTH);
		glu.gluSphere(qobj0, 0.3d, 8, 8);
		glu.gluDeleteQuadric(qobj0);
		gl.glPopMatrix();

		gl.glPushMatrix();
		gl.glColor3f(0.8f, 0.2f, 0.0f);
		GLUquadric qobj1 = glu.gluNewQuadric();
		gl.glTranslated(x, y, z);
		gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL.GL_FILL);
		glu.gluQuadricDrawStyle(qobj1, GLU.GLU_LINE);
		glu.gluQuadricNormals(qobj1, GLU.GLU_SMOOTH);
		glu.gluSphere(qobj1, 0.3d, 8, 8);
		glu.gluDeleteQuadric(qobj1);
		gl.glPopMatrix();

		// // texture end
		// gl.glDisable(GL.GL_TEXTURE_GEN_S);
		// gl.glDisable(GL.GL_TEXTURE_GEN_T);
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	private void drawDome() {
		// texture begin
		gl.glEnable(GL.GL_TEXTURE_GEN_S); // Enable Texture Coord Generation
		// For S (NEW)
		gl.glEnable(GL.GL_TEXTURE_GEN_T); // Enable Texture Coord Generation
		// For T (NEW)
		gl.glBindTexture(GL.GL_TEXTURE_2D, panel.getGraphicsAdapter().getTextures()[JOGLGraphicsAdapter.DOME_TEXTURE]);

		gl.glPushMatrix();
		gl.glColor3f(0.1f, 1.0f, 1.0f);
		GLUquadric qobj0 = glu.gluNewQuadric();
		gl.glTranslatef(0, 0, 0);
		gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL.GL_FILL);
		glu.gluQuadricDrawStyle(qobj0, GLU.GLU_FILL);
		glu.gluQuadricNormals(qobj0, GLU.GLU_SMOOTH);
		glu.gluQuadricOrientation(qobj0, GLU.GLU_INSIDE);
		// rotate on the y axis
		glu.gluSphere(qobj0, 20.0d, 32, 32);
		glu.gluDeleteQuadric(qobj0);
		gl.glPopMatrix();

		// texture end
		gl.glDisable(GL.GL_TEXTURE_GEN_S);
		gl.glDisable(GL.GL_TEXTURE_GEN_T);
	}

	public boolean isRollingCameraEnabeld() {
		return rollingCameraEnabeld;
	}

	public void setRollingCameraEnabeld(boolean rollingCameraEnabeld) {
		this.rollingCameraEnabeld = rollingCameraEnabeld;
	}

	public void keyPressed(KeyEvent event) {
		char ch = (char) event.keyCode;
		int modifiers = 0;
		if ((event.stateMask & SWT.CTRL) != 0) {
			modifiers |= InteractionManager.MODIFIER_CTRL;
		}
		if ((event.stateMask & SWT.SHIFT) != 0) {
			modifiers |= InteractionManager.MODIFIER_SHIFT;
		}
		if ((event.stateMask & SWT.ALT) != 0) {
			modifiers |= InteractionManager.MODIFIER_ALT;
		}
		if (ch == '0') {
			setDistance(0);
		}
		if (ch == 'f') {
			canvas.getShell().setMaximized(!canvas.getShell().getMaximized());
		}
		if (ch == '1') {
			setRollingCameraEnabeld(!isRollingCameraEnabeld());
		}
		if (ch == 'l') {
			panel.getNanoGraph().setLayout(new ForceDirectedLayout(panel));
		}
		if (ch == 'c') {
			showCameras = !showCameras;
		}
		if (ch == 'd') {
			showDome = !showDome;
		}

		// camera control
		if (ch == '+' || ch == '=') {
			setDistance(getDistance() - 0.2d);
		}
		if (ch == '-') {
			setDistance(getDistance() + 0.2d);
		}

		// left
		if (event.keyCode == SWT.ARROW_LEFT) {
			deltaAngle = 0.03f;
		}
		// right
		if (event.keyCode == SWT.ARROW_RIGHT) {
			deltaAngle = -0.03f;
		}
		// up
		if (event.keyCode == SWT.ARROW_UP) {
			deltaMove += 0.2d;
		}
		// down
		if (event.keyCode == SWT.ARROW_DOWN) {
			deltaMove -= 0.2d;
		}
		if (event.keyCode == SWT.PAGE_UP) {
			deltaZAngle = -0.03f;
		}
		if (event.keyCode == SWT.PAGE_DOWN) {
			deltaZAngle = 0.03f;
		}
	}

	public void keyReleased(KeyEvent event) {
		if (event.keyCode == SWT.ARROW_LEFT || event.keyCode == SWT.ARROW_RIGHT) {
			deltaAngle = 0.0d;
		}
		if (event.keyCode == SWT.ARROW_UP || event.keyCode == SWT.ARROW_DOWN) {
			deltaMove = 0;
		}
		if (event.keyCode == SWT.PAGE_UP || event.keyCode == SWT.PAGE_DOWN) {
			deltaZAngle = 0;
		}
	}

	public void mouseDoubleClick(MouseEvent arg0) {
	}

	public void mouseDown(MouseEvent e) {
		// mode = SELECTION_MODE;
		int mouse_x = e.x;
		int mouse_y = e.y;
		// int mouse_y = e.y;
		deltaMove = 1.0d;
		if (mouse_x < (canvas.getBounds().width / 2 - 50)) {
			deltaAngle = 0.03f;
		} else if (mouse_x > (canvas.getBounds().width / 2 + 50)) {
			deltaAngle = -0.03f;
		}
		if (mouse_y < (canvas.getBounds().height / 2 - 50)) {
			deltaZAngle = -0.03f;
		} else if (mouse_y > (canvas.getBounds().height / 2 + 50)) {
			deltaZAngle = 0.03f;
		}
	}

	public void mouseUp(MouseEvent arg0) {
		deltaMove = 0;
		deltaAngle = 0.0d;
		deltaZAngle = 0.0d;
	}
}
