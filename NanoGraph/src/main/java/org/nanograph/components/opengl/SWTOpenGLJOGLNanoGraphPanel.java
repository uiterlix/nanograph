/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.components.opengl;

import javax.media.opengl.GL;
import javax.media.opengl.GLContext;
import javax.media.opengl.GLDrawableFactory;
import javax.media.opengl.glu.GLU;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.opengl.GLCanvas;
import org.eclipse.swt.opengl.GLData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.nanograph.components.NanoGraphPanel;
import org.nanograph.components.swt.SWTResourceManager;
import org.nanograph.drawing.NanoGraph;
import org.nanograph.drawing.docking.CenterDockingStrategy;
import org.nanograph.interaction.InteractionManager;
import org.nanograph.model.GraphModel;

public class SWTOpenGLJOGLNanoGraphPanel implements NanoGraphPanel {

    private InteractionManager interactionManager = null;

    private NanoGraph nanoGraph = null;

    private double zoomfactor = 0.0d;

    private GLContext context;

    private JOGLGraphicsAdapter graphicsAdapter;

    private GL gl;

    private JOGLRefresher refresher;

    private GLCanvas canvas;

    public SWTOpenGLJOGLNanoGraphPanel(Composite c) {
        SWTResourceManager.create(c.getDisplay());

        // JOGL stuff
        GLData data = new GLData();
        data.doubleBuffer = true;

        canvas = new GLCanvas(c, SWT.NONE, data);
        nanoGraph = new NanoGraph();
        interactionManager = new InteractionManager(nanoGraph);
        nanoGraph.setDefaultDockingStrategy(new CenterDockingStrategy((JOGLGraphicsAdapter.SPHERE_RADIUS) * 100));

        canvas.setCurrent();
        context = GLDrawableFactory.getFactory().createExternalGLContext();

        refresher = new JOGLRefresher(this, canvas, c.getDisplay(), context);

        canvas.addListener(SWT.Resize, new Listener() {
            private GL gl;

            private GLU glu;

            public void handleEvent(Event event) {
                System.out.println(">>> resize");
                Rectangle bounds = canvas.getBounds();
                float fAspect = (float) bounds.width / (float) bounds.height;
                canvas.setCurrent();
                context.makeCurrent();
                gl = context.getGL();
                gl.glViewport(0, 0, bounds.width, bounds.height);
                gl.glMatrixMode(GL.GL_PROJECTION);
                gl.glLoadIdentity();
                glu = new GLU();
                glu.gluPerspective(45.0f, fAspect, 0.5f, 400.0f);
                gl.glMatrixMode(GL.GL_MODELVIEW);
                gl.glLoadIdentity();
                context.release();
            }
        });

        canvas.addKeyListener(refresher);
        canvas.addMouseListener(refresher);

        context.makeCurrent();
        gl = context.getGL();
        graphicsAdapter = new JOGLGraphicsAdapter(gl);
        init();
        context.release();

        c.getDisplay().asyncExec(refresher);
    }

    public void init() {
        initMaterial();
        initLight();
        gl.glPolygonMode(GL.GL_FRONT, GL.GL_FILL);
        gl.glEnable(GL.GL_LINE_SMOOTH);
        gl.glShadeModel(GL.GL_SMOOTH); // Enables Smooth Color Shading to
        // create graduate color
        gl.glClearColor(1.0f, 1.0f, 1.0f, 1.0f); // This Will Clear The
        // Background Color To Black
        gl.glClearDepth(1.0); // Enables Clearing Of The Depth Buffer
        gl.glEnable(GL.GL_DEPTH_TEST); // Enables Depth Testing
        gl.glDepthFunc(GL.GL_LEQUAL); // The Type Of Depth Test To Do

        gl.glHint(GL.GL_PERSPECTIVE_CORRECTION_HINT, GL.GL_NICEST); // Really
        // Nice
        // Perspective
        // Calculations
        // hide the back face (face view when we are inside of the cube)
        gl.glCullFace(GL.GL_BACK);
        gl.glEnable(GL.GL_CULL_FACE);
        
        gl.glEnable(GL.GL_TEXTURE_2D);
        gl.glTexGeni(GL.GL_S, GL.GL_TEXTURE_GEN_MODE, GL.GL_SPHERE_MAP); // Set The Texture Generation Mode For S To Sphere Mapping (NEW)
        gl.glTexGeni(GL.GL_T, GL.GL_TEXTURE_GEN_MODE, GL.GL_SPHERE_MAP); // Set The Texture Generation Mode For T To Sphere Mapping (NEW)
        
    }

    private void initMaterial() {
        // float no_mat[] = { 0.0f, 0.0f, 0.0f, 1.0f };
        // float mat_ambient[] = { 0.7f, 0.7f, 0.7f, 1.0f };
        // // float mat_ambient_color[] = { 0.8f, 0.8f, 0.2f, 1.0f };
        // float mat_ambient_color[] = { color[0], color[1], color[2], 1.0f };

        // float mat_diffuse[] = { 0.1f, 0.5f, 0.8f, 1.0f };
        // float mat_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
        // float no_shininess[] = { 0.0f };
        // float low_shininess[] = { 5.0f };
        // float high_shininess[] = { 100.0f };
        // float mat_emission[] = { 0.3f, 0.2f, 0.2f, 0.0f };

        // goud
        // float mat_ambient[] = { 0.24f, 0.19f, 0.07f, 1.0f };
        // float mat_diffuse[] = { 0.75f, 0.60f, 0.22f, 1.0f };
        // float mat_specular[] = { 0.62f, 0.55f, 0.36f, 1.0f };
        // float mat_shininess = 51.2f;

        // float mat_ambient[] = { color[0], color[1], color[2], 1.0f };
        // float mat_diffuse[] = { color[0], color[1], color[2], 1.0f };
        float mat_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
        float mat_emission[] = { 0.2f, 0.2f, 0.2f, 1.0f };
        float mat_shininess = 51.2f;

        gl.glColorMaterial(GL.GL_FRONT_AND_BACK, GL.GL_AMBIENT_AND_DIFFUSE);
        //gl.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, mat_ambient, 0);
        // gl.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, mat_diffuse, 0);
        gl.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, mat_specular, 0);
        gl.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, mat_emission, 0);
        gl.glMaterialf(GL.GL_FRONT, GL.GL_SHININESS, mat_shininess);
    }

    /*
     * Initialize all the lights
     */
    public void initLight() {
        // light properties
        float[] ambient = { 0.01f, 0.01f, 0.01f, 1.0f };
        float[] diffuse = { 1.0f, 1.0f, 1.0f, 1.0f };
        float[] specular = { 1.0f, 1.0f, 1.0f, 1.0f };
        float[] position = { 1.0f, 1.0f, 0.0f, 0.0f };
        

        gl.glLightfv(GL.GL_LIGHT0, GL.GL_AMBIENT, ambient, 0);
        gl.glLightfv(GL.GL_LIGHT0, GL.GL_DIFFUSE, diffuse, 0);
        gl.glLightfv(GL.GL_LIGHT0, GL.GL_SPECULAR, specular, 0);
        gl.glLightfv(GL.GL_LIGHT0, GL.GL_POSITION, position, 0);

        float[] position2 = { -1.0f, -1.0f, 0.0f, 0.0f };

        gl.glLightfv(GL.GL_LIGHT1, GL.GL_AMBIENT, ambient, 0);
        gl.glLightfv(GL.GL_LIGHT1, GL.GL_DIFFUSE, diffuse, 0);
        gl.glLightfv(GL.GL_LIGHT1, GL.GL_SPECULAR, specular, 0);
        gl.glLightfv(GL.GL_LIGHT1, GL.GL_POSITION, position2, 0);

        // Only outside face because we don't see the inside of the spheres
        gl.glLightModeli(GL.GL_LIGHT_MODEL_TWO_SIDE, 1);

        gl.glEnable(GL.GL_LIGHT0);
        gl.glEnable(GL.GL_LIGHT1);
        gl.glEnable(GL.GL_LIGHTING);

        gl.glEnable(GL.GL_COLOR_MATERIAL);
    }

    public void drawNanograph() {
        nanoGraph.paintGraph(graphicsAdapter);
    }

    public InteractionManager getInteractionManager() {
        return interactionManager;
    }

    public NanoGraph getNanoGraph() {
        return nanoGraph;
    }

    public double getZoomFactor() {
        return zoomfactor;
    }

    public void repaint() {
    }

    public void setModel(GraphModel graph) {
        nanoGraph.setModel(graph);
        nanoGraph.calculateBounds(graphicsAdapter);
        interactionManager.reset();
    }

    public void setZoomFactor(double zoomFactor) {
        // check if between minzoom and maxzoom?
        zoomfactor = zoomFactor;
    }

    public void setInteractionManager(InteractionManager interactionManager) {
        this.interactionManager = interactionManager;
    }

    public void setNanoGraph(NanoGraph nanoGraph) {
        this.nanoGraph = nanoGraph;
    }

    public JOGLGraphicsAdapter getGraphicsAdapter() {
        return graphicsAdapter;
    }

    public void setGraphicsAdapter(JOGLGraphicsAdapter gc) {
        this.graphicsAdapter = gc;
    }

    public JOGLRefresher getRefresher() {
        return refresher;
    }

    public void setRefresher(JOGLRefresher refresher) {
        this.refresher = refresher;
    }
}
