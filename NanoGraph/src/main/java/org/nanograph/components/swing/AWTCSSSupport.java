/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.components.swing;

import java.awt.Color;

import org.nanograph.util.css.CSSSupport;

/**
 * CSSSupport.java
 * 
 * @author Jeroen van Grondelle
 */
public class AWTCSSSupport extends CSSSupport {

	public final String supportedAtttributes = "color,line-width,line-style";

	public boolean isSupportedAtttribute(String atttribute) {
		return supportedAtttributes.indexOf(atttribute) > -1;
	}

	/*
	 * XHTML Colors Note: Should be interpreted case INSENSITIVE
	 * 
	 * Aqua #00FFFF Black #000000 Blue #0000FF Fuchsia #FF00FF
	 * 
	 * Gray #808080 Green #008000 Lime #00FF00 Maroon #800000
	 * 
	 * Navy #000080 Olive #808000 Purple #800080 Red #FF0000
	 * 
	 * Silver #C0C0C0 Teal #008080 White #FFFFFF Yellow #FFFF00
	 */
	public static Color getPaint(String paint) {
		String color = getCssColor(paint);
		if (color.equals("none")) {
			return null;
		} else {
			return Color.decode(color);
		}
	}
	
	public static Color getPaint(String paint, int alpha) {
		Color c = getPaint(paint);
		return new Color(c.getRed(), c.getGreen(), c.getBlue(), alpha);
	}
}