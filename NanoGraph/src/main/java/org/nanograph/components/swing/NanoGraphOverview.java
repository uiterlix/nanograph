/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.components.swing;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JComponent;

import org.nanograph.drawing.NanoGraph;
import org.nanograph.interaction.events.GraphSelectionEvent;
import org.nanograph.interaction.events.GraphSelectionListener;

/** 
 * Implements an outline view on the NanoGraphPanel.
 * 
 * The graph's outline is rendered and the visual part of the
 * graph in the mother panel is highlighted.
 * 
 * @author Jeroen van Grondelle
 */
public class NanoGraphOverview extends JComponent implements GraphSelectionListener
{
    SwingNanoGraphPanel panel = null;
   
    protected NanoGraphOverview(SwingNanoGraphPanel panel)
    {
        this.panel = panel;
    }
    
    public void paintComponent(Graphics g)
    {
        NanoGraph nanograph = panel.getNanoGraph();
        
        if(nanograph.getGraphHeight() == 0 || 
                nanograph.getGraphWidth() == 0)
            return;
        
        Graphics2D g2d = (Graphics2D) g;
        
        // clear
        g2d.setColor(Color.WHITE);
        g2d.fillRect(0,0,getWidth(), getHeight());
        
        // compute scale
        double scale = (double)Math.min(
                getHeight()/(double)nanograph.getGraphHeight(),
                getWidth()/(double)nanograph.getGraphWidth()
        );
        g2d.scale(scale, scale);
        
        // draw outline
		g2d.setColor(Color.lightGray);
		
		SwingGraphicsAdapter gc = new SwingGraphicsAdapter(g2d);
		panel.getNanoGraph().paintOutline(gc);
		
		//TODO Draw visible range of NanoGraphPanel
		
    }
    
    public void selectionMove(GraphSelectionEvent e)
    {
        repaint();
    }
    
    public void selectionReset(GraphSelectionEvent e)
    {
        
    }
    public void selectionChanged(GraphSelectionEvent e)
    {
        
    }
}