/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.components.swing;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Stroke;

import org.nanograph.drawing.graphicsadapter.AbstractGraphicsAdapter;
import org.nanograph.drawing.graphicsadapter.GraphicsAdapter;

public class SwingGraphicsAdapter extends AbstractGraphicsAdapter implements GraphicsAdapter {

	public Graphics2D g2d;

	int alpha = 255;

	String paint;

	Stroke dotStroke = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 50, new float[] { 2.0f, 2.0f }, 0);

	Stroke dashedStroke = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 50, new float[] { 10.0f, 5.0f }, 0);

	Stroke solidStroke = new BasicStroke();

	public SwingGraphicsAdapter(Graphics2D g2d) {
		this.g2d = g2d;
	}
	
	public void drawArc(double x, double y, int arcWidth, int arcHeight, int startAngle, int arcAngle) {
		g2d.drawArc((int) x, (int) y, arcWidth, arcHeight, startAngle, arcAngle);
	}

	public void drawLine(double x1, double y1, double x2, double y2) {
		g2d.drawLine((int) x1, (int) y1, (int) x2, (int) y2);
	}

	public void drawString(String string, double x1, double x2) {
		double y = x2 + stringHeight(string) / 3 * 2;
		g2d.drawString(string, (int) (x1), (int) (y));
	}

	public void handleSetColor(String cssColor) {
		this.paint = cssColor;
		g2d.setColor(AWTCSSSupport.getPaint(cssColor, alpha));
	}

	public void handleSetFillStyle(String cssAttribute, String value) {
		// TODO: XUI Implement this!
		throw new UnsupportedOperationException("This method is not yet implemented");
	}

	public double stringWidth(String string) {
		return g2d.getFontMetrics().stringWidth(string);
	}

	public void handleSetFillColor(String cssColor) {
		g2d.setPaint(AWTCSSSupport.getPaint(cssColor));
	}

	public void drawOval(double x1, double y1, double x2, double y2) {
		g2d.drawOval((int) x1, (int) y1, (int) x2, (int) y2);
	}

	public void fillOval(double x1, double y1, double x2, double y2) {
		g2d.fillOval((int) x1, (int) y1, (int) x2, (int) y2);
	}

	public void drawPolygon(int[] xValues, int[] yValues) {
		g2d.setPaint(AWTCSSSupport.getPaint("#000000"));
		g2d.drawPolygon(xValues, yValues, xValues.length);
	}

	public void drawRectangle(double x1, double y1, double x2, double y2) {
		g2d.drawRect((int) x1, (int) y1, (int) x2, (int) y2);
	}

	public void fillPolygon(int[] xValues, int[] yValues) {
		g2d.fillPolygon(xValues, yValues, xValues.length);
	}

	public void fillRectangle(double x1, double y1, double x2, double y2) {
		g2d.fillRect((int) x1, (int) y1, (int) x2, (int) y2);
	}

	public void fillRoundRectangle(double x1, double y1, double x2, double y2, double j, double k) {
		g2d.fillRoundRect((int) x1, (int) y1, (int) x2, (int) y2, (int) j, (int) k);
	}

	public void handleSetAlpha(int alpha) {
		this.alpha = alpha;
	}

	public void handleSetFont(String face, int size, int style) {
		g2d.setFont(SwingResourceManager.getFont(face, style, size));
	}

	public double stringHeight(String string) {
		return g2d.getFontMetrics().getHeight();
	}

	public void drawRoundRectangle(double x1, double y1, double x2, double y2, double j, double k) {
		g2d.drawRoundRect((int) x1, (int) y1, (int) x2, (int) y2, (int) j, (int) k);
	}

	public void drawImage(String uri, double x, double y) {
		g2d.drawImage(SwingResourceManager.getImage(uri), (int) x, (int) y, null);
	}

	public double imageHeight(String uri) {
		return SwingResourceManager.getImage(uri).getHeight(null);
	}

	public double imageWidth(String uri) {
		return SwingResourceManager.getImage(uri).getWidth(null);
	}

	public void handleSetLineStyle(int lineStyle) {
		switch (lineStyle) {
			case GraphicsAdapter.LINESTYLE_DOT:
				g2d.setStroke(dotStroke);
				break;
			case GraphicsAdapter.LINESTYLE_DASH:
				g2d.setStroke(dashedStroke);
				break;
			case GraphicsAdapter.LINESTYLE_SOLID:
				g2d.setStroke(solidStroke);
				break;
		}
	}

	public void setLineStyle(String cssAttribute, String value) {
		// TODO: XUI Implement this!
		throw new UnsupportedOperationException("This method is not yet implemented");
	}

	public void drawImage(String id, byte[] data, double x, double y) {
		g2d.drawImage(SwingResourceManager.getImage(id, data), (int) x, (int) y, null);
	}

	public double imageHeight(String id, byte[] data) {
		Image image = SwingResourceManager.getImage(id, data);
		return image.getHeight(null);
	}

	public double imageWidth(String id, byte[] data) {
		Image image = SwingResourceManager.getImage(id, data);
		return image.getWidth(null);
	}

}
