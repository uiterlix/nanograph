/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.components.swing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.VolatileImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;

import javax.swing.JPanel;

import org.nanograph.components.NanoGraphPanel;
import org.nanograph.drawing.NanoGraph;
import org.nanograph.interaction.InteractionManager;
import org.nanograph.model.GraphModel;

/**
 * A Swing component for viulaising a GraphModel adapter.
 * 
 * Implements zooming, printing and access to an outline panel.
 * 
 * @author Jeroen van Grondelle
 */
public class SwingNanoGraphPanel extends JPanel implements NanoGraphPanel, Printable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -5175907249308662962L;

	NanoGraph nanograph = null;

    InteractionManager manager = null;

    double zoomFactor = 1.0;
    
    public final static double ZOOM_STEP = 0.05; 

    VolatileImage bufferImage = null;

    // Image bufferImage = null;
    
    VolatileImage clearImage = null;

    Graphics2D gBuffer2D = null;
    
    int width = 0;
    int height = 0;

    public SwingNanoGraphPanel() {
        SelectionMouseListener sml = new SelectionMouseListener();
        this.addMouseListener(sml);
        this.addMouseMotionListener(sml);
        nanograph = new NanoGraph();
        nanograph.setBackground(null);
        manager = new InteractionManager(nanograph);
        nanograph.setHighlightNodeSet(manager.getNodeSelection().getSelectedObjects());
    }

    public void setSize(int width, int height) {
        super.setSize(width, height);
    }

    public VolatileImage getBufferImage() {
        return bufferImage;
    }

    public void flushImage() {
        if (bufferImage != null) {
            bufferImage.flush();
            bufferImage = null;
        }
    }

    protected void resizeBufferImage(int graphWidth, int graphHeight) {
        int newWidth = graphWidth > this.getWidth() ? graphWidth : this.getWidth();
        int newHeight = graphHeight > this.getHeight() ? graphHeight : this.getHeight();
        if (bufferImage == null || bufferImage.getWidth(this) != newWidth || bufferImage.getHeight(this) != newHeight) {
            if (bufferImage != null) {
                bufferImage.flush();
            }
            bufferImage = null;
            bufferImage = createVolatileImage(newWidth, newHeight);
            // bufferImage = createImage(newWidth, newHeight);
            gBuffer2D = ((Graphics2D) bufferImage.getGraphics());
            setSize(newWidth, newHeight);
        }
    }

    public void setModel(GraphModel graph) {
        nanograph.setModel(graph);
        // model has changed, so the interaction manager must reset the
        // selections
        manager.reset();
    }

    public NanoGraph getNanoGraph() {
        return nanograph;
    }
    
    public void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;

	  	int graphWidth = nanograph.getGraphWidth();
		int graphHeight = nanograph.getGraphHeight();
		
        resizeBufferImage(graphWidth, nanograph.getGraphHeight());
        
        SwingGraphicsAdapter gc = new SwingGraphicsAdapter(gBuffer2D);
        
        nanograph.calculateBounds(gc);
        
        if (clearImage == null || (clearImage.getWidth() != graphWidth || clearImage.getHeight() != graphHeight)) {
        	clearImage = createVolatileImage(graphWidth > 0 ? graphWidth : 1, graphHeight > 0 ? graphHeight : 1);
        	Graphics2D gI2d = (Graphics2D) clearImage.getGraphics();
        	gI2d.setPaint(Color.WHITE);
        	gI2d.fillRect(0,0,graphWidth > 0 ? graphWidth : 1, graphHeight > 0 ? graphHeight : 1);
        }
        
        if (zoomFactor != 1.0) {
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            gBuffer2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            gBuffer2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        }
        
        gBuffer2D.drawImage(clearImage, 0,0, this);
        g2d.drawImage(clearImage, 0, 0, this);
        
        nanograph.paintGraph(gc);
        manager.paintInteractionMask(gc);
        if (zoomFactor != 1.0) {
            g2d.scale(zoomFactor, zoomFactor);
        }
        if (bufferImage != null) {
            g2d.drawImage(bufferImage, 0, 0, this);
        }
    }

    public Dimension getPreferredSize() {
        return new Dimension((int) (zoomFactor * nanograph.getGraphWidth()), (int) (zoomFactor * nanograph.getGraphHeight()));
    }

    public int print(Graphics g, PageFormat pageFormat, int pageIndex) {
        if (pageIndex > 0) {
            return (Printable.NO_SUCH_PAGE);
        }
        Graphics2D g2d = (Graphics2D) g;
        g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
        double scale = Math.max(this.getWidth() / pageFormat.getImageableWidth(), this.getHeight() / pageFormat.getImageableHeight());
        g2d.scale(1 / scale, 1 / scale);
        // Turn off double buffering
        paint(g2d);
        // Turn double buffering back on
        return (PAGE_EXISTS);
    }

    public InteractionManager getInteractionManager() {
        return manager;
    }

    public class SelectionMouseListener extends MouseAdapter implements MouseMotionListener {

        public void mousePressed(MouseEvent e) {
            requestFocus();
            int modifiers = 0;
            if (e.isShiftDown())
                modifiers |= InteractionManager.MODIFIER_SHIFT;
            if (e.isControlDown())
                modifiers |= InteractionManager.MODIFIER_CTRL;
            if (e.isAltDown())
                modifiers |= InteractionManager.MODIFIER_ALT;
            if (e.getButton() == MouseEvent.BUTTON3)
                modifiers |= InteractionManager.MODIFIER_RIGHTCLICK;
            manager.handleMouseDown((int) (e.getX() / zoomFactor), (int) (e.getY() / zoomFactor), modifiers);
            repaint();
        }

        public void mouseDragged(MouseEvent e) {
            int modifiers = 0;
            if (e.isControlDown()) {
                // do nothing...
            } else if ((e.getModifiers() & InputEvent.BUTTON3_MASK) == InputEvent.BUTTON3_MASK) {
                modifiers |= InteractionManager.MODIFIER_RIGHTCLICK;
                manager.handleMouseDown((int) (e.getX() / zoomFactor), (int) (e.getY() / zoomFactor), modifiers);
            } else {
                manager.handleMouseDrag((int) (e.getX() / zoomFactor), (int) (e.getY() / zoomFactor));
                revalidate();
            }
            repaint();
        }

        public void mouseMoved(MouseEvent e) {
        }

        public void mouseReleased(MouseEvent e) {
            manager.handleMouseUp((int) (e.getX() / zoomFactor), (int) (e.getY() / zoomFactor));
            revalidate();
            repaint();
        }
    }

    public double getZoomFactor() {
        return zoomFactor;
    }

    public void setZoomFactor(double zoomFactor) {
        if (zoomFactor <= 1.0 && zoomFactor > 0.1) {
            this.zoomFactor = zoomFactor;
        }
        revalidate();
        repaint();
    }

    public Point getScreenCoords(double x, double y) {
        Point point = new Point();
        point.setLocation(x * zoomFactor, y * zoomFactor);
        return point;
    }

    public NanoGraphOverview getOverview() {
        NanoGraphOverview overview = new NanoGraphOverview(this);
        
        manager.addSelectionListener(overview);
        
        return overview;
    }

    public Graphics2D getGBuffer2D() {
        return gBuffer2D;
    }

}