/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.components.swing;

import java.awt.Font;
import java.awt.Image;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;

import org.nanograph.drawing.graphicsadapter.GraphicsAdapter;

public class SwingResourceManager {

	private static final String FONTKEY_SEPARATOR = ";";
	
	private static Map images = new HashMap();
	private static Map dataImages = new HashMap();
	private static Map fonts = new HashMap();
	
	public static Image getImage(String uri) {
		Image image = null;
		if (images.containsKey(uri)) {
			image = (Image) images.get(uri);
		} else {
		    // This method ensures that all pixels have been loaded before returning
		    image = new ImageIcon(SwingResourceManager.class.getResource(uri)).getImage();			
			images.put(uri, image);
		}
		return image;
	}
	
	public static Font getFont(String fontFace, int style, int size) {
		Font f = null;
		String key = getFontKey(fontFace, style, size);
		if (!fonts.containsKey(key)) {
			f = new Font(fontFace, getFontStyle(style), size);
			fonts.put(key, f);
		} else {
			f = (Font) fonts.get(key);
		}
		return f;
	}
	
	private static int getFontStyle(int style) {
		int fontStyle = Font.PLAIN;
		if ((style | GraphicsAdapter.FONT_STYLE_BOLD) == style) {
			fontStyle |= Font.BOLD;
		}
		if ((style | GraphicsAdapter.FONT_STYLE_ITALIC) == style) {
			fontStyle |= Font.ITALIC;
		}
		return fontStyle;
	}
	
	private static String getFontKey(String fontFace, int style, int size) {
		StringBuffer fontKey = new StringBuffer();
		fontKey.append(fontFace);
		fontKey.append(FONTKEY_SEPARATOR);
		fontKey.append(style);
		fontKey.append(FONTKEY_SEPARATOR);
		fontKey.append(size);
		return fontKey.toString();
	}

	public static Image getImage(String id, byte[] data) {
		Image image = null;
		if (dataImages.containsKey(id)) {
			image = (Image) dataImages.get(id);
		} else {
		    // This method ensures that all pixels have been loaded before returning
		    image = new ImageIcon(data).getImage();			
		    dataImages.put(id, image);
		}
		return image;
	}
	

}
