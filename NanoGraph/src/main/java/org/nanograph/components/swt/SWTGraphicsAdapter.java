/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.components.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;
import org.nanograph.drawing.graphicsadapter.AbstractGraphicsAdapter;
import org.nanograph.drawing.graphicsadapter.GraphicsAdapter;

public class SWTGraphicsAdapter extends AbstractGraphicsAdapter {

	public GC gc;

	public SWTGraphicsAdapter(GC gc) {
		this.gc = gc;
	}

	public void drawArc(double x, double y, int arcWidth, int arcHeight, int startAngle, int arcAngle) {
		gc.drawArc((int) x, (int) y, arcWidth, arcHeight, startAngle, arcAngle);
	}
	
	public void drawLine(double x1, double y1, double x2, double y2) {
		gc.drawLine((int) x1, (int) y1, (int) x2, (int) y2);
	}

	public void drawString(String string, double x1, double x2) {
		gc.drawText(string, (int) x1, (int) x2, SWT.DRAW_TRANSPARENT);
	}

	public void handleSetColor(String cssColor) {
		// get the color from the SWT resource manager
		gc.setForeground(SWTResourceManager.getInstance().getColor(cssColor));
	}

	public void handleSetFillStyle(String cssAttribute, String value) {
		// TODO: XUI Implement this!
		throw new UnsupportedOperationException("This method is not yet implemented");
	}

	public void handleSetLineStyle(String cssAttribute, String value) {
		// TODO: XUI Implement this!
		throw new UnsupportedOperationException("This method is not yet implemented");
	}

	public double stringWidth(String string) {
		return gc.textExtent(string).x;
	}

	public void handleSetFillColor(String cssColor) {
		gc.setBackground(SWTResourceManager.getInstance().getColor(cssColor));
	}

	public void drawOval(double x1, double y1, double x2, double y2) {
		gc.drawOval((int) x1, (int) y1, (int) x2, (int) y2);
	}

	public void fillOval(double x1, double y1, double x2, double y2) {
		gc.fillOval((int) x1, (int) y1, (int) x2, (int) y2);
	}

	public void drawPolygon(int[] xValues, int[] yValues) {
		int[] polyvalues = new int[xValues.length * 2];
		int pos = 0;
		for (int i = 0; i < xValues.length; i++) {
			polyvalues[pos] = xValues[i];
			polyvalues[pos + 1] = yValues[i];
			pos = pos + 2;
		}
		gc.drawPolygon(polyvalues);
	}

	public void drawRectangle(double x1, double y1, double x2, double y2) {
		gc.drawRectangle((int) x1, (int) y1, (int) x2, (int) y2);
	}

	public void fillPolygon(int[] xValues, int[] yValues) {
		int[] polyvalues = new int[xValues.length * 2];
		int pos = 0;
		for (int i = 0; i < xValues.length; i++) {
			polyvalues[pos] = xValues[i];
			polyvalues[pos + 1] = yValues[i];
			pos = pos + 2;
		}
		gc.fillPolygon(polyvalues);
	}

	public void fillRectangle(double x1, double y1, double x2, double y2) {
		gc.fillRectangle((int) x1, (int) y1, (int) x2, (int) y2);
	}

	public void fillRoundRectangle(double x1, double y1, double x2, double y2, double j, double k) {
		gc.fillRoundRectangle((int) x1, (int) y1, (int) x2, (int) y2, (int) j, (int) k);
	}

	public void handleSetAlpha(int alpha) {
		gc.setAlpha(alpha);
	}

	public void handleSetFont(String face, int size, int style) {
		gc.setFont(SWTResourceManager.getInstance().getFont(face, size, style));
	}

	public double stringHeight(String string) {
		return gc.textExtent(string).y;
	}

	public void drawRoundRectangle(double x1, double y1, double x2, double y2, double j, double k) {
		gc.drawRoundRectangle((int) x1, (int) y1, (int) x2, (int) y2, (int) j, (int) k);
	}

	public void drawImage(String uri, double x, double y) {
		gc.drawImage(SWTResourceManager.getInstance().getImage(uri), (int) x, (int) y);
	}

	public double imageHeight(String uri) {
		return SWTResourceManager.getInstance().getImageData(uri).height;
	}

	public double imageWidth(String uri) {
		return SWTResourceManager.getInstance().getImageData(uri).width;
	}

	public double imageHeight(String id, byte[] data) {
		return SWTResourceManager.getInstance().getImageData(id, data).height;
	}

	public double imageWidth(String id, byte[] data) {
		return SWTResourceManager.getInstance().getImageData(id, data).width;
	}

	public void handleSetLineStyle(int lineStyle) {
		int gcLineStyle = SWT.LINE_SOLID;
		switch (lineStyle) {
		case GraphicsAdapter.LINESTYLE_DOT:
			gcLineStyle = SWT.LINE_DOT;
			break;
		case GraphicsAdapter.LINESTYLE_DASH:
			gcLineStyle = SWT.LINE_DASH;
			break;
		case GraphicsAdapter.LINESTYLE_SOLID:
			gcLineStyle = SWT.LINE_SOLID;
			break;
		}
		gc.setLineStyle(gcLineStyle);
	}

	public void setLineStyle(String cssAttribute, String value) {
		// TODO: XUI Implement this!
		throw new UnsupportedOperationException("This method is not yet implemented");
	}

	public void drawImage(String id, byte[] data, double x, double y) {
		gc.drawImage(SWTResourceManager.getInstance().getImage(id, data), (int) x, (int) y);
	}

}
