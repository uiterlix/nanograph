/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.components.swt;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.print.PageFormat;
import java.awt.print.Printable;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.nanograph.components.NanoGraphPanel;
import org.nanograph.drawing.NanoGraph;
import org.nanograph.interaction.InteractionManager;
import org.nanograph.model.GraphModel;

/**
 * A Swing component for viulaising a GraphModel adapter.
 * 
 * Implements zooming, printing and access to an outline panel.
 * 
 * @author Jeroen van Grondelle
 */
public class SWTNanoGraphPanel extends Canvas implements NanoGraphPanel, Printable {
    NanoGraph nanograph = null;

    InteractionManager manager = null;

    double zoomFactor = 1.0;

    boolean reduceColordepthOnZoom;

    public final static double ZOOM_STEP = 0.05;

    int width = 0;

    int height = 0;
  
    public SWTNanoGraphPanel(Composite parent, int style, NanoGraph nanoGraph) {
    	super(parent, style);
    	this.nanograph = nanoGraph; 
    	SWTResourceManager.create(parent.getDisplay());

        GraphInputListener listener = new GraphInputListener();
        addMouseListener(listener);
        addMouseMoveListener(listener);

        nanograph.setBackground(null);
        manager = new InteractionManager(nanograph);
        nanograph.setHighlightNodeSet(manager.getNodeSelection().getSelectedObjects());

        this.addPaintListener(new GraphPaintListener());
    }
    
    public SWTNanoGraphPanel(Composite parent, int style) {
        this(parent, style, new NanoGraph());
    }

    class GraphPaintListener implements PaintListener {
        public void paintControl(PaintEvent event) {
            // Draw the background

            GC paintGC = null;
            Image bufferImage = null;

            Color whiteColor = SWTResourceManager.getInstance().getSystemColor(SWT.COLOR_WHITE);
            int graphWidth = nanograph.getGraphWidth();
            int graphHeight = nanograph.getGraphHeight();

            if (zoomFactor != 1) {
                bufferImage = (Image) getData("double-buffer-image");

                if (bufferImage == null || bufferImage.getBounds().width != graphWidth || bufferImage.getBounds().height != graphHeight) {

                    if (bufferImage != null) {
                        bufferImage.dispose();
                    }
                    if (reduceColordepthOnZoom) {
                        ImageData data = new ImageData(graphWidth, graphHeight, 8, SWTResourceManager.getInstance().getWebSafePaletteData());
                        bufferImage = new Image(getDisplay(), data);
                    } else {
                        bufferImage = new Image(getDisplay(), graphWidth, graphHeight);
                    }

                    setData("double-buffer-image", bufferImage);
                }
                paintGC = new GC(bufferImage);
            } else {
                paintGC = event.gc;
            }

            SWTGraphicsAdapter g = new SWTGraphicsAdapter(paintGC);
            paintGC.setBackground(whiteColor);
            paintGC.fillRectangle(0, 0, graphWidth, graphHeight);

            nanograph.paintGraph(g);
            manager.paintInteractionMask(g);

            if (bufferImage != null) {
                int width = bufferImage.getBounds().width;
                int height = bufferImage.getBounds().height;
                double newwidth = (int) width * zoomFactor;
                double newheight = (int) height * zoomFactor;
                event.gc.drawImage(bufferImage, 0, 0, width, height, 0, 0, (int) newwidth, (int) newheight);
                if (getSize().x != newwidth || getSize().y != newheight) {
                    setSize((int) newwidth, (int) newheight);
                }
                paintGC.dispose();
            } else {
                setSize(nanograph.getGraphWidth(), nanograph.getGraphHeight());
            }

        }
    }

    public void setSize(int width, int height) {
        super.setSize(width, height);
    }

    public void setModel(GraphModel graph) {
        nanograph.setModel(graph);
        nanograph.calculateBounds(null);
        setSize(nanograph.getGraphWidth(), nanograph.getGraphHeight());
        // model has changed, so the interaction manager must reset the
        // selections
        manager.reset();
    }

    public NanoGraph getNanoGraph() {
        return nanograph;
    }

    public Dimension getPreferredSize() {
        return new Dimension((int) (zoomFactor * nanograph.getGraphWidth()), (int) (zoomFactor * nanograph.getGraphHeight()));
    }

    public int print(Graphics g, PageFormat pageFormat, int pageIndex) {
        if (pageIndex > 0) {
            return (Printable.NO_SUCH_PAGE);
        }
        Graphics2D g2d = (Graphics2D) g;
        g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
        // double scale = Math.max(this.getWidth() /
        // pageFormat.getImageableWidth(), this.getHeight() /
        // pageFormat.getImageableHeight());
        // g2d.scale(1 / scale, 1 / scale);
        // Turn off double buffering
        // paint(g2d);
        // Turn double buffering back on
        return (PAGE_EXISTS);
    }

    public InteractionManager getInteractionManager() {
        return manager;
    }

    class GraphInputListener implements MouseListener, MouseMoveListener {

        

        boolean mousedown;

 

        boolean keydown;

 

        public void mouseDoubleClick(MouseEvent evt) {

        }

 

        public void mouseDown(MouseEvent evt) {

            mousedown = true;

            int modifiers = 0;

            if ((evt.stateMask & SWT.SHIFT) > 0)

                modifiers |= InteractionManager.MODIFIER_SHIFT;

            if ((evt.stateMask & SWT.CTRL) > 0)

                modifiers |= InteractionManager.MODIFIER_CTRL;

            if ((evt.stateMask & SWT.ALT) > 0)

                modifiers |= InteractionManager.MODIFIER_ALT;

            if (evt.button == 3)

                modifiers |= InteractionManager.MODIFIER_RIGHTCLICK;

            manager.handleMouseDown((int) (evt.x / zoomFactor), (int) (evt.y / zoomFactor), modifiers);

        }

 

        public void mouseUp(MouseEvent evt) {

            mousedown = false;

            manager.handleMouseUp((int) (evt.x / zoomFactor), (int) (evt.y / zoomFactor));

            redraw();

        }

 

        public void mouseMove(MouseEvent evt) {

            if (mousedown) {

                if ((evt.stateMask & SWT.CTRL) > 0) {

                    // do nothing

                } else {

                    manager.handleMouseDrag((int) (evt.x / zoomFactor), (int) (evt.y / zoomFactor));

                    redraw();

                }

            }

        }

 

    }

    public double getZoomFactor() {
        return zoomFactor;
    }

    public void setZoomFactor(double zoomFactor) {
        if (zoomFactor <= 1.0 && zoomFactor >= 0.1) {
            this.zoomFactor = zoomFactor;
        }
        repaint();
    }

    public Point getScreenCoords(double x, double y) {
        Point point = new Point();
        point.setLocation(x * zoomFactor, y * zoomFactor);
        return point;
    }

    public static void main(String args[]) {
        Display display = new Display();
        Shell shell = new Shell(display);
        shell.setSize(800, 600);
        shell.open();

        shell.setLayout(new FillLayout());
        new SWTNanoGraphPanel(shell, SWT.DOUBLE_BUFFERED);

        while (!shell.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }
        display.dispose();
    }

    public void repaint() {
        /*
         * Use syncexec to make sure any thread outside the UI thread can use
         * repaint()
         */
        getDisplay().syncExec(new Runnable() {
            public void run() {
                redraw();
            }
        });

    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public boolean isReduceColordepthOnZoom() {
        return reduceColordepthOnZoom;
    }

    public void setReduceColordepthOnZoom(boolean reduceColordepthOnScale) {
        this.reduceColordepthOnZoom = reduceColordepthOnScale;
    }

}