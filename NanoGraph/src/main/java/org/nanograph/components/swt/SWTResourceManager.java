/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.components.swt;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;
import org.nanograph.drawing.graphicsadapter.GraphicsAdapter;
import org.nanograph.util.css.CSSSupport;

public class SWTResourceManager {

	// This is an array containing all 216 netscape websafe colors
	private static int[][] websafePalette = {{0,0,0},{0,0,51},{0,0,102},{0,0,153},{0,0,204},{0,0,255},{0,51,0},{0,51,51},
											{0,51,102},{0,51,153},{0,51,204},{0,51,255},{0,102,0},{0,102,51},{0,102,102},
											{0,102,153},{0,102,204},{0,102,255},{0,153,0},{0,153,51},{0,153,102},
											{0,153,153},{0,153,204},{0,153,255},{0,204,0},{0,204,51},{0,204,102},
											{0,204,153},{0,204,204},{0,204,255},{0,255,0},{0,255,51},{0,255,102},
											{0,255,153},{0,255,204},{0,255,255},{51,0,0},{51,0,51},{51,0,102},{51,0,153},
											{51,0,204},{51,0,255},{51,51,0},{51,51,51},{51,51,102},{51,51,153},{51,51,204},
											{51,51,255},{51,102,0},{51,102,51},{51,102,102},{51,102,153},{51,102,204},
											{51,102,255},{51,153,0},{51,153,51},{51,153,102},{51,153,153},{51,153,204},
											{51,153,255},{51,204,0},{51,204,51},{51,204,102},{51,204,153},{51,204,204},
											{51,204,255},{51,255,0},{51,255,51},{51,255,102},{51,255,153},{51,255,204},
											{51,255,255},{102,0,0},{102,0,51},{102,0,102},{102,0,153},{102,0,204},{102,0,255},
											{102,51,0},{102,51,51},{102,51,102},{102,51,153},{102,51,204},{102,51,255},
											{102,102,0},{102,102,51},{102,102,102},{102,102,153},{102,102,204},{102,102,255},
											{102,153,0},{102,153,51},{102,153,102},{102,153,153},{102,153,204},{102,153,255},
											{102,204,0},{102,204,51},{102,204,102},{102,204,153},{102,204,204},{102,204,255},
											{102,255,0},{102,255,51},{102,255,102},{102,255,153},{102,255,204},{102,255,255},
											{153,0,0},{153,0,51},{153,0,102},{153,0,153},{153,0,204},{153,0,255},{153,51,0},
											{153,51,51},{153,51,102},{153,51,153},{153,51,204},{153,51,255},{153,102,0},
											{153,102,51},{153,102,102},{153,102,153},{153,102,204},{153,102,255},{153,153,0},
											{153,153,51},{153,153,102},{153,153,153},{153,153,204},{153,153,255},{153,204,0},
											{153,204,51},{153,204,102},{153,204,153},{153,204,204},{153,204,255},{153,255,0},
											{153,255,51},{153,255,102},{153,255,153},{153,255,204},{153,255,255},{204,0,0},
											{204,0,51},{204,0,102},{204,0,153},{204,0,204},{204,0,255},{204,51,0},{204,51,51},
											{204,51,102},{204,51,153},{204,51,204},{204,51,255},{204,102,0},{204,102,51},
											{204,102,102},{204,102,153},{204,102,204},{204,102,255},{204,153,0},{204,153,51},
											{204,153,102},{204,153,153},{204,153,204},{204,153,255},{204,204,0},{204,204,51},
											{204,204,102},{204,204,153},{204,204,204},{204,204,255},{204,255,0},{204,255,51},
											{204,255,102},{204,255,153},{204,255,204},{204,255,255},{255,0,0},{255,0,51},{255,0,102},
											{255,0,153},{255,0,204},{255,0,255},{255,51,0},{255,51,51},{255,51,102},{255,51,153},
											{255,51,204},{255,51,255},{255,102,0},{255,102,51},{255,102,102},{255,102,153},
											{255,102,204},{255,102,255},{255,153,0},{255,153,51},{255,153,102},{255,153,153},
											{255,153,204},{255,153,255},{255,204,0},{255,204,51},{255,204,102},{255,204,153},
											{255,204,204},{255,204,255},{255,255,0},{255,255,51},{255,255,102},{255,255,153},
											{255,255,204},{255,255,255}};
	
	private static SWTResourceManager sResourceManager;
	private Display display;
	private Map fonts = new HashMap();
	
	private Map colorMap = new HashMap();
	private Map imageMap = new HashMap();
	private Map imageDataMap = new HashMap();
	
	private Map byteImageMap = new HashMap();
	private Map byteImageDataMap = new HashMap();
	
	private ImageLoader imageLoader = new ImageLoader();
	
	private PaletteData webSafePaletteData;
	
	public static SWTResourceManager getInstance() {
		if (sResourceManager == null) {
			throw new RuntimeException("SWTResourceManager is not initialized");
		}
		return sResourceManager;
	}
	
	public static SWTResourceManager create(Display d) {
		sResourceManager = new SWTResourceManager();
		sResourceManager.setDisplay(d);
		sResourceManager.init();
		return sResourceManager;
	}
	
	private void setDisplay(Display d) {
		this.display = d;
	}
	
	public void init() {
		// Initialize the web safe colour palette
		RGB[] array = new RGB[websafePalette.length];
		RGB rgb = null;
		for (int i = 0; i < websafePalette.length; i++) {
			int[] currentcolor = websafePalette[i];
			rgb = new RGB(currentcolor[0], currentcolor[1], currentcolor[2]);
			array[i] = rgb;
		}
		webSafePaletteData = new PaletteData(array);
	}
	
	public Color getSystemColor(int code) {
		return display.getSystemColor(code);
	}
	
	public Color getColor(String cssColor) {
		Color color = null;
		String parsedColor = CSSSupport.getCssColor(cssColor);
		if (colorMap.containsKey(parsedColor)) {
			color = (Color) colorMap.get(parsedColor);
		} else {
			// parse the color
			int r = Integer.parseInt(parsedColor.substring(1, 3), 16);
			int g = Integer.parseInt(parsedColor.substring(3, 5), 16);
			int b = Integer.parseInt(parsedColor.substring(5, 7), 16);
			color = new Color(display, r, g, b);
			colorMap.put(parsedColor, color);
		}
		return color;
	}
	
    public Font getFont(String name, int height, int style) {
    	// translate to SWT font style
    	int swtStyle = SWT.NONE;
    	if ((style | GraphicsAdapter.FONT_STYLE_BOLD) == style) {
    		swtStyle |= SWT.BOLD;
    	}
    	if ((style | GraphicsAdapter.FONT_STYLE_ITALIC) == style) {
    		swtStyle |= SWT.ITALIC;
    	}
    	return getFont(name, height, swtStyle, false, false);
    }

	public Font getFont(String name, int size, int style, boolean strikeout, boolean underline) {
		String fontName = name + '|' + size + '|' + style + '|' + strikeout + '|' + underline;
        Font font = (Font) fonts.get(fontName);
        if (font == null) {
        	FontData fontData = new FontData(name, size, style);
    		if (strikeout || underline) {
    			try {
    				Class logFontClass = Class.forName("org.eclipse.swt.internal.win32.LOGFONT"); //$NON-NLS-1$
    				Object logFont = FontData.class.getField("data").get(fontData); //$NON-NLS-1$
    				if (logFont != null && logFontClass != null) {
    					if (strikeout) {
							logFontClass.getField("lfStrikeOut").set(logFont, new Byte((byte) 1)); //$NON-NLS-1$
						}
    					if (underline) {
							logFontClass.getField("lfUnderline").set(logFont, new Byte((byte) 1)); //$NON-NLS-1$
						}
    				}
    			} catch (Throwable e) {
    				e.printStackTrace();
    			}
    		}
    		font = new Font(Display.getCurrent(), fontData);
    		fonts.put(fontName, font);
        }
		return font;
	}    	
	
	public Image getImage(String uri) {
		Image image = null;
		if (imageMap.containsKey(uri)) {
			image = (Image) imageMap.get(uri);
		} else {
			image = new Image(display, this.getClass().getResourceAsStream(uri));
			imageMap.put(uri, image);
			imageDataMap.put(uri, image.getImageData());
		}
		return image;
	}
	
	public ImageData getImageData(String uri) {
		ImageData data = null;
		if (!imageDataMap.containsKey(uri)) {
			data = getImage(uri).getImageData();
		} else {
			data = (ImageData) imageDataMap.get(uri);
		}
		return data;
	}
	
	public ImageData getImageData(String id, byte[] data) {
		ImageData imageData = null;
		if (!byteImageDataMap.containsKey(id)) {
			imageData = getImage(id, data).getImageData();
		} else {
			imageData = (ImageData) byteImageDataMap.get(id);
		}
		return imageData;
	}

	public PaletteData getWebSafePaletteData() {
		return webSafePaletteData;
	}

	public Image getImage(String id, byte[] data) {
		Image image = null;
		
		if (byteImageMap.containsKey(id)) {
			image = (Image) byteImageMap.get(id);
		} else {
		
			ByteArrayInputStream is = new ByteArrayInputStream(data);
			
			ImageData[] imageData = imageLoader.load(is);
			if (imageData.length == 1) {
				image = new Image(display, imageData[0]);
				byteImageMap.put(id, image);
			}
		}
		return image;
	}

	public ImageData getByteImageData(String id, byte[] data) {
		ImageData imageData = null;
		if (!byteImageDataMap.containsKey(id)) {
			imageData = getImage(id, data).getImageData();
		} else {
			imageData = (ImageData) imageDataMap.get(id);
		}
		return imageData;
	}
}
