/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.drawing.background;

import java.awt.geom.Rectangle2D;

import org.nanograph.drawing.graphicsadapter.GraphicsAdapter;

/**
 * GridBackGround.java
 * 
 * @author Jeroen van Grondelle
 */
public class GridBackGround implements Background {
	int gridsize = 25;

	String color = "#BBBBBB";

	public GridBackGround() {
	}

	public GridBackGround(int gridsize, String color) {
		this.gridsize = gridsize;
		this.color = color;
	}

	public void drawBackground(GraphicsAdapter g, int width, int height) {
		g.setColor(color);
		for (int x = 0; x < width; x += gridsize) {
			for (int y = 0; y < height; y += gridsize) {
				g.drawOval(x, y, 0, 0);
			}
		}
	}

	public Rectangle2D getBounds(GraphicsAdapter g) {
		return null;
	}
}
