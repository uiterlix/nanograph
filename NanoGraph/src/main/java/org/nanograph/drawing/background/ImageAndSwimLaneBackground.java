/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.drawing.background;

import java.awt.geom.Rectangle2D;

import org.nanograph.drawing.graphicsadapter.GraphicsAdapter;

public class ImageAndSwimLaneBackground implements Background {

	private byte[] data;

	private String id;

	private String[] horizontalLanes;

	private String[] verticalLanes;

	private int[] horizontalWidth;

	private int[] verticalWidth;

	private int mode = -1;

	// iamge only
	public ImageAndSwimLaneBackground(byte[] data, String id) {
		super();
		this.data = data;
		this.id = id;
	}

	// swimlanes only
	public ImageAndSwimLaneBackground(String[] horizontalLanes, String[] verticalLanes, int[] horizontalWidth, int[] verticalWidth, int mode) {
		super();
		this.horizontalLanes = horizontalLanes;
		this.verticalLanes = verticalLanes;
		this.horizontalWidth = horizontalWidth;
		this.verticalWidth = verticalWidth;
		this.mode = mode;
	}

	// Both image and swimlanes
	public ImageAndSwimLaneBackground(byte[] data, String id, String[] horizontalLanes, String[] verticalLanes, int[] horizontalWidth, int[] verticalWidth, int mode) {
		super();
		this.data = data;
		this.id = id;
		this.horizontalLanes = horizontalLanes;
		this.verticalLanes = verticalLanes;
		this.horizontalWidth = horizontalWidth;
		this.verticalWidth = verticalWidth;
		this.mode = mode;
	}

	// default constructor
	public ImageAndSwimLaneBackground() {
	}

	public void drawBackground(GraphicsAdapter g, int width, int height) {
		if (data != null) {
			g.drawImage(id, data, 0, 0);
		}
		if (mode != -1) {
			g.setColor("#000000");
			// draw lines
			int horizontalCount = 0, verticalCount = 0;
			if (mode == SwimLaneBackground.MODE_HORIZONTAL || mode == SwimLaneBackground.MODE_BOTH) {
				if (mode == SwimLaneBackground.MODE_BOTH) {
					width = (int) getLanesWidth();
				}
				for (int i = 0; i < horizontalLanes.length; i++) {
					g.drawLine(0, horizontalCount, width, horizontalCount);
					int headerPos = horizontalCount + (new Double((horizontalWidth[i] / 2) - (g.stringWidth(horizontalLanes[i]) / 2))).intValue();
					g.drawString(horizontalLanes[i], 3, headerPos);
					horizontalCount += horizontalWidth[i];
				}
			}
			if (mode == SwimLaneBackground.MODE_VERTICAL || mode == SwimLaneBackground.MODE_BOTH) {
				if (mode == SwimLaneBackground.MODE_BOTH) {
					height = (int) getLanesHeight();
				}
				for (int i = 0; i < verticalLanes.length; i++) {
					g.drawLine(verticalCount, 0, verticalCount, height);
					int headerPos = verticalCount + (new Double((verticalWidth[i] / 2) - (g.stringWidth(verticalLanes[i]) / 2))).intValue();
					g.drawString(verticalLanes[i], headerPos, 15);
					verticalCount += verticalWidth[i];
				}
			}
			// last line
			if (mode == SwimLaneBackground.MODE_HORIZONTAL || mode == SwimLaneBackground.MODE_BOTH) {
				g.drawLine(0, horizontalCount, width, horizontalCount);
			}
			if (mode == SwimLaneBackground.MODE_VERTICAL || mode == SwimLaneBackground.MODE_BOTH) {
				g.drawLine(verticalCount, 0, verticalCount, height);
			}
		}
	}

	public Rectangle2D getBounds(GraphicsAdapter g) {
		double width = 0.0d, height = 0.0d;
		if (g != null && data != null) {
			width = g.imageWidth(id, data);
			height = g.imageHeight(id, data);
		}
		if (mode != -1) {
			double laneWidth = 0.0d, laneHeight = 0.0d;
			if (mode == SwimLaneBackground.MODE_HORIZONTAL || mode == SwimLaneBackground.MODE_BOTH) {
				laneHeight = getLanesHeight();
			}
			if (mode == SwimLaneBackground.MODE_VERTICAL || mode == SwimLaneBackground.MODE_BOTH) {
				laneWidth = getLanesWidth();
			}
			width = (laneWidth > width ? laneWidth : width) + 1;
			height = (laneHeight > height ? laneHeight : height) + 1;
		}
		return new Rectangle2D.Double(0, 0, width, height);
	}

	private double getLanesHeight() {
		double laneHeight = 0.0d;
		for (int i = 0; i < horizontalLanes.length; i++) {
			laneHeight += horizontalWidth[i];
		}
		return laneHeight;
	}

	private double getLanesWidth() {
		double laneWidth = 0.0d;
		for (int i = 0; i < verticalLanes.length; i++) {
			laneWidth += verticalWidth[i];
		}
		return laneWidth;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String[] getHorizontalLanes() {
		return horizontalLanes;
	}

	public void setHorizontalLanes(String[] lanes) {
		this.horizontalLanes = lanes;
	}

	public int getMode() {
		return mode;
	}

	public void setMode(int mode) {
		this.mode = mode;
	}

	public int[] getHorizontalWidth() {
		return horizontalWidth;
	}

	public String[] getVerticalLanes() {
		return verticalLanes;
	}

	public int[] getVerticalWidth() {
		return verticalWidth;
	}

	public void setHorizontalWidth(int[] horizontalWidth) {
		this.horizontalWidth = horizontalWidth;
	}

	public void setVerticalLanes(String[] verticalLanes) {
		this.verticalLanes = verticalLanes;
	}

	public void setVerticalWidth(int[] verticalWidth) {
		this.verticalWidth = verticalWidth;
	}
}
