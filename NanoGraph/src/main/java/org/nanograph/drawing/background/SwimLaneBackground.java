/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.drawing.background;

import java.awt.geom.Rectangle2D;

import org.nanograph.drawing.graphicsadapter.GraphicsAdapter;

/**
 * SwimLaneBackground.java
 */
public class SwimLaneBackground implements Background {
    
    public final static int MODE_HORIZONTAL = 0, MODE_VERTICAL = 1, MODE_BOTH = 2;
    
    private int swimlaneSize = 200;
    private String[] lanes = new String[] {"Swimlane 1", "Swimlane 2"};
    private int mode = MODE_HORIZONTAL;
    
    public SwimLaneBackground () {}
    public SwimLaneBackground (String[] lanes, int mode) {this.lanes = lanes; this.mode = mode;}
    public SwimLaneBackground (String[] lanes, int mode,  int size) {this.lanes = lanes; this.mode = mode; this.swimlaneSize = size;}
    
    public void drawBackground(GraphicsAdapter g, int width, int height)
    {
        g.setColor("#CCCCCC");
        
        g.drawRectangle(0,0,width, height);
        
        // draw lines
        for (int t=0; t < lanes.length; t++)
        {
           if(mode == MODE_HORIZONTAL)
           {
               g.drawLine(0, t*swimlaneSize, width, t*swimlaneSize);
               g.drawString(lanes[t], 3,t*swimlaneSize+15);
           }
           else
           {
               g.drawLine(t*swimlaneSize, 0, t*swimlaneSize, height); 
               g.drawString(lanes[t], t*swimlaneSize+3, 15);
           }  
        }
        // last line
        if(mode == MODE_HORIZONTAL)
        {
            g.drawLine(0,  lanes.length*swimlaneSize, width,  lanes.length*swimlaneSize);    
        }
        else
        {
            g.drawLine(lanes.length*swimlaneSize, 0,  lanes.length*swimlaneSize, height); 
        } 
    }
    
    public Rectangle2D getBounds(GraphicsAdapter g) {
		return null;
	}
}
