/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.drawing.docking;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

public class CenterDockingStrategy implements DockingStrategy {

    private double distanceFromCenter = 0.0d;

    public CenterDockingStrategy() {
    }

    public CenterDockingStrategy(double distanceFromCenter) {
        this.distanceFromCenter = distanceFromCenter;
    }

    public Point2D getDockingPoint(Rectangle2D nodeBounds, Point2D centerofOtherNode) {
        Point2D result = new Point2D.Double();
        double x1 = nodeBounds.getCenterX();
        double y1 = nodeBounds.getCenterY();
        if (distanceFromCenter == 0.0d) {
            result.setLocation(x1, y1);
        } else {
            double x2 = centerofOtherNode.getX();
            double y2 = centerofOtherNode.getY();
            double a = x2 - x1;
            double b = y2 - y1;
            double alpha = Math.atan(b / a);
            
            if (x2 < x1) {
                alpha += Math.PI;
            }

            double dockingX = distanceFromCenter * Math.cos(alpha) + x1;
            double dockingY = distanceFromCenter * Math.sin(alpha) + y1;

            result.setLocation(dockingX, dockingY);
        }
        return result;
    }

}
