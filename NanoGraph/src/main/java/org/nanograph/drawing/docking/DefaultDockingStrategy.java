/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.drawing.docking;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

/**
 * @author Xander
 *
 */
public class DefaultDockingStrategy implements DockingStrategy {

	/* (non-Javadoc)
	 * @see nl.nanoworks.nanograph.drawing.docking.DockingStrategy#getDockingPoint(java.awt.geom.Rectangle2D, java.awt.geom.Point2D)
	 */
	public Point2D getDockingPoint(Rectangle2D nodeBounds, Point2D centerofOtherNode)
	{
		// can return 4 mounting points
		
		// determine width
		int width = nodeBounds.getBounds().width;
		int height = nodeBounds.getBounds().height;
		int centerXOfThisNode = (int) nodeBounds.getBounds().getX() + (nodeBounds.getBounds().width / 2);
		int centerYOfThisNode = (int) nodeBounds.getBounds().getY() + (nodeBounds.getBounds().height / 2);
		
		// determine shortest Y
		double y = centerofOtherNode.getY()-centerYOfThisNode > 0 ?
				centerYOfThisNode + (height/2) : centerYOfThisNode - (height/2);
		
		// determine shortest X
		double x = centerofOtherNode.getX()-centerXOfThisNode > 0 ?
				centerXOfThisNode + (width / 2) : centerXOfThisNode - (width/2);
		
		// determine preferred mountingpoint
		double diff = Math.abs(centerofOtherNode.getY()-centerYOfThisNode) - Math.abs(centerofOtherNode.getX()-centerXOfThisNode);
		
		return new Point2D.Double(diff < 0 ? x : centerXOfThisNode, diff < 0 ? centerYOfThisNode : y);	
	}

}
