/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.drawing.docking;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

/**
 * A DockingStrategy implementation determines where an edge
 * should meet a node, based on the bounds of the node and the origin
 * of the edge.
 *
 * @author Jeroen van Grondelle
 */
public interface DockingStrategy {
	public Point2D getDockingPoint(Rectangle2D nodeBounds, Point2D centerofOtherNode);
}
