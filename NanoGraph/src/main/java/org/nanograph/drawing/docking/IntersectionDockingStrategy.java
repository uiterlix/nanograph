/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.drawing.docking;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

/**
 * @author Xander
 *
 */
public class IntersectionDockingStrategy implements DockingStrategy {

	/* (non-Javadoc)
	 * @see nl.nanoworks.nanograph.drawing.docking.DockingStrategy#getDockingPoint(java.awt.geom.Rectangle2D, java.awt.geom.Point2D)
	 */
	public Point2D getDockingPoint(Rectangle2D nodeBounds, Point2D centerofOtherNode)
	{
		// determine center of this node
		int centerx = (int) nodeBounds.getCenterX();
		int centery = (int) nodeBounds.getCenterY();

		/* 
		 * now intersect the line from this center to the other center
		 * with the nodebounds of this node
		 */
		double y = centery;
		double x = centerx;
		// center of other node is below
		double coef = (centery - centerofOtherNode.getY()) / (centerx - centerofOtherNode.getX());
		double offset = centery - (centerx * coef);

		//if ((int)centerofOtherNode.getY() >= (int)(nodeBounds.getY() + nodeBounds.getHeight())) {
		if ((int)centerofOtherNode.getY() > centery) {
			y = (double) nodeBounds.getMaxY();
		} else {
			y = (double) nodeBounds.getMinY();
		}

		x = (y - offset) / coef;

		if (x >= nodeBounds.getMaxX()) {
			/* snijpunt met linkerkant van rectangle */
			x = nodeBounds.getMaxX();
			/* y = ax + b */
			y = coef * x + offset;
		} else if (x <= nodeBounds.getMinX()) {
			/* snijpunt met rechterkant van rectangle */
			x = nodeBounds.getMinX();
			y = coef * x + offset;
		} 
		
		/* repair, there must be another way to fix this */
		if (((centerofOtherNode.getX() > centerx) && (x == nodeBounds.getMinX() ))) {
			x = nodeBounds.getMaxX();
		}
		if (((centerofOtherNode.getX() < centerx) && (x == nodeBounds.getMaxX() ))) {
			x = nodeBounds.getMinX();
		}
		
		if (x == Double.NEGATIVE_INFINITY) {
			x = nodeBounds.getMinX();
		}
		if (x == Double.POSITIVE_INFINITY) {
			x = nodeBounds.getMaxX();
		}		
		if ((int)x == 0) {
			x = centerx;
		}

		return new Point2D.Double((int) x, (int) y);	
	}

}
