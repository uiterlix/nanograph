/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.drawing.edgerenderer;

import java.awt.geom.Point2D;

import org.nanograph.drawing.graphicsadapter.GraphicsAdapter;

/**
 * The AbstractEdgeRenderer provides the basic edge rendering facilities. <br />
 * Rendering varieties:
 * <ul>
 * <li>Solid line or dashed lines</li>
 * <li>Arrow head</li>
 * <li>Reversed arrow (is rendered with an open head</li>
 * <li>edge labels</li>
 * <li>different rendering for selected edges</li>
 * </ul>
 */
public abstract class AbstractEdgeRenderer implements EdgeRenderer {

    protected static final int REVERSED = 4;

    protected String normalColor = DEFAULT_NORMAL_COLOR;

    protected String selectedColor = DEFAULT_SELECTED_COLOR;

    protected String textColor = DEFAULT_NORMAL_COLOR;
    
    protected String fillColor = DEFAULT_FILL_COLOR;

    protected abstract int getLineType(Object edge);
    
    protected abstract int getStartConnector(Object edge);
    
    protected abstract int getEndConnector(Object edge);
    
    protected abstract int getTextType(Object edge);
    

    public final void renderSelected(GraphicsAdapter g, Object edge, Point2D from, Point2D to) {
        render(g, edge, from, to, true);
    }

    public final void render(GraphicsAdapter g, Object edge, Point2D from, Point2D to) {
        render(g, edge, from, to, false);
    }

    protected void render(GraphicsAdapter g, Object edge, Point2D from, Point2D to, boolean selected) {
        
        if (selected) {
            g.setColor(getSelectedColor());
        } else {
            g.setColor(getNormalColor());
        }
        
        g.setFillColor(getFillColor());
        
    	g.drawLine(from.getX(), from.getY(), to.getX(), to.getY(), getStartConnector(edge), getEndConnector(edge), getLineType(edge));
        
        // text
        if ((getTextType(edge) & GraphicsAdapter.LABEL_STYLE_TEXT) == GraphicsAdapter.LABEL_STYLE_TEXT) {
            String string = edge.toString();
            float width = 0;
            g.setColor(textColor);
            g.drawString(string, (from.getX() + to.getX() - width) / 2, ((from.getY() + to.getY()) / 2) - 4);
        }
    }

    
    

    public String getNormalColor() {
        return normalColor;
    }

    public void setNormalColor(String normalColor) {
        this.normalColor = normalColor;
    }

    public String getSelectedColor() {
        return selectedColor;
    }

    public void setSelectedColor(String selectedColor) {
        this.selectedColor = selectedColor;
    }

    public String getFillColor() {
        return fillColor;
    }

    public void setFillColor(String fillColor) {
        this.fillColor = fillColor;
    }
}
