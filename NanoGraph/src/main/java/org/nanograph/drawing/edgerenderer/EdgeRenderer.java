/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.drawing.edgerenderer;

import java.awt.geom.Point2D;

import org.nanograph.drawing.graphicsadapter.GraphicsAdapter;

/**
 * An EdgeRender implementation is able to render an edge, given the edge object
 * obtained from the GraphModel and begin and end points calculated by the
 * docking strategy
 * 
 * @author Jeroen van Grondelle
 */
public interface EdgeRenderer {
	
    public static final String DEFAULT_SELECTED_COLOR = "#FF0000";

    public static final String DEFAULT_NORMAL_COLOR = "#000000";
    
    public static final String DEFAULT_TEXT_COLOR = "#000000";
    
    public static final String DEFAULT_FILL_COLOR = "#000000";
    
	public abstract void render(GraphicsAdapter g, Object edge, Point2D from,
			Point2D to);
	public abstract void renderSelected(GraphicsAdapter g, Object edge, Point2D from,
			Point2D to);
	
	public abstract void setNormalColor(String color);
	
	public abstract void setSelectedColor(String color);
}