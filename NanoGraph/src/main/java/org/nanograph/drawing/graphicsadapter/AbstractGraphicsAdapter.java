/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.drawing.graphicsadapter;

public abstract class AbstractGraphicsAdapter implements GraphicsAdapter {

    private int lineStyle;

    private String color;

    private String fillColor;

    private String fontFace;

    private int fontSize;

    private int fontStyle;

    private int alpha;

    private int xValues[] = new int[3];

    private int yValues[] = new int[3];

    private int al = 12; // Arrow length

    private int aw = 10; // Arrow width

    private int haw = aw / 2; // Half arrow width

    public abstract void handleSetColor(String color);

    public abstract void handleSetFillColor(String color);

    public abstract void handleSetFont(String fontFace, int fontSize, int style);

    public abstract void handleSetLineStyle(int lineStyle);

    public abstract void handleSetAlpha(int alpha);

    public abstract void handleSetFillStyle(String cssAttribute, String value);

    public void setAlpha(int alpha) {
        this.alpha = alpha;
        handleSetAlpha(alpha);
    }

    public void setColor(String cssColor) {
        this.color = cssColor;
        handleSetColor(cssColor);
    }

    public void setFillColor(String cssColor) {
        this.fillColor = cssColor;
        handleSetFillColor(cssColor);
    }

    public void setFillStyle(String cssAttribute, String value) {
        throw new UnsupportedOperationException("This method is not yet implemented");
    }

    public void setFont(String face, int size, int style) {
        this.fontFace = face;
        this.fontSize = size;
        this.fontStyle = style;
        handleSetFont(face, size, style);
    }

    public void setLineStyle(int lineStyle) {
        handleSetLineStyle(lineStyle);
    }

    public String getFontFace() {
        return fontFace;
    }

    public void setFontFace(String fontFace) {
        this.fontFace = fontFace;
    }

    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    public int getFontStyle() {
        return fontStyle;
    }

    public void setFontStyle(int fontStyle) {
        this.fontStyle = fontStyle;
    }

    public int getAlpha() {
        return alpha;
    }

    public String getColor() {
        return color;
    }

    public String getFillColor() {
        return fillColor;
    }

    public int getLineStyle() {
        return lineStyle;
    }

    protected void drawArrows(double x1, double y1, double x2, double y2, int connectorBeginStyle, int connectorEndStyle) {
        // first we draw begin connector
        drawConnector(x2, y2, x1, y1, connectorBeginStyle);
        // then we draw end connector
        drawConnector(x1, y1, x2, y2, connectorEndStyle);
    }

    private void drawConnector(double x1, double y1, double x2, double y2, int style) {
        int tempLineStyle = getLineStyle();
        String tempFillColor = getFillColor();
        switch (style) {
        case GraphicsAdapter.CONNECTOR_STYLE_ARROW:
            handleSetLineStyle(GraphicsAdapter.LINESTYLE_SOLID);
            calcArrowPolygonValues((int) x1, (int) y1, (int) x2, (int) y2);
            handleSetFillColor("#ffffff");
            setFillColor("#ffffff");
            fillPolygon(xValues, yValues);
            drawPolygon(xValues, yValues);
            break;
        case GraphicsAdapter.CONNECTOR_STYLE_ARROW_FILLED:
            handleSetLineStyle(GraphicsAdapter.LINESTYLE_SOLID);
            calcArrowPolygonValues((int) x1, (int) y1, (int) x2, (int) y2);
            handleSetFillColor(fillColor);
            setFillColor(fillColor);
            fillPolygon(xValues, yValues);
            drawPolygon(xValues, yValues);
            break;
        case GraphicsAdapter.CONNECTOR_STYLE_CIRCLE:
            break;
        case GraphicsAdapter.CONNECTOR_STYLE_CIRCLE_FILLED:
            break;
        case GraphicsAdapter.CONNECTOR_STYLE_DIAMOND:
            break;
        case GraphicsAdapter.CONNECTOR_STYLE_DIAMOND_FILLED:
            break;
        case GraphicsAdapter.CONNECTOR_STYLE_NONE:
            break;
        default:
            break;
        }
        if (tempFillColor != null) {
            handleSetFillColor(tempFillColor);
        } else {
            handleSetFillColor("#FFFFFF");
        }
        handleSetLineStyle(tempLineStyle);
    }

    public void drawLine(double x1, double y1, double x2, double y2, int connectorBeginStyle, int connectorEndStyle, int lineStyle) {
        setLineStyle(lineStyle);
        drawLine(x1, y1, x2, y2);
        drawArrows(x1, y1, x2, y2, connectorBeginStyle, connectorEndStyle);
    }

    /* CALC VALUES: Calculate x-y values. */
    private void calcArrowPolygonValues(int x1, int y1, int x2, int y2) {
        // North or south
        if (x1 == x2) {
            // North
            if (y2 < y1)
                arrowCoords(x2, y2, x2 - haw, y2 + al, x2 + haw, y2 + al);
            // South
            else
                arrowCoords(x2, y2, x2 - haw, y2 - al, x2 + haw, y2 - al);
            return;
        }
        // East or West
        if (y1 == y2) {
            // East
            if (x2 > x1)
                arrowCoords(x2, y2, x2 - al, y2 - haw, x2 - al, y2 + haw);
            // West
            else
                arrowCoords(x2, y2, x2 + al, y2 - haw, x2 + al, y2 + haw);
            return;
        }
        // Calculate quadrant

        calcValuesQuad(x1, y1, x2, y2);
    }

    /*
     * CALCULATE VALUES QUADRANTS: Calculate x-y values where direction is not
     * parallel to eith x or y axis.
     */
    private void calcValuesQuad(int x1, int y1, int x2, int y2) {
        double arrowAng = Math.toDegrees(Math.atan((double) haw / (double) al));
        double dist = Math.sqrt(al * al + aw);
        double lineAng = Math.toDegrees(Math.atan(((double) Math.abs(x1 - x2)) / ((double) Math.abs(y1 - y2))));

        // Adjust line angle for quadrant
        if (x1 > x2) {
            // South East
            if (y1 > y2)
                lineAng = 180.0 - lineAng;
        } else {
            // South West
            if (y1 > y2)
                lineAng = 180.0 + lineAng;
            // North West
            else
                lineAng = 360.0 - lineAng;
        }
        // Calculate coords
        xValues[0] = x2;
        yValues[0] = y2;
        calcCoords(1, x2, y2, dist, lineAng - arrowAng);
        calcCoords(2, x2, y2, dist, lineAng + arrowAng);
    }

    /*
     * CALCULATE COORDINATES: Determine new x-y coords given a start x-y and a
     * distance and direction
     */
    private void calcCoords(int index, int x, int y, double dist, double dirn) {
        // System.out.println("dirn = " + dirn);
        while (dirn < 0.0)
            dirn = 360.0 + dirn;
        while (dirn > 360.0)
            dirn = dirn - 360.0;
        // System.out.println("dirn = " + dirn);

        // North-East
        if (dirn <= 90.0) {
            xValues[index] = x + (int) (Math.sin(Math.toRadians(dirn)) * dist);
            yValues[index] = y - (int) (Math.cos(Math.toRadians(dirn)) * dist);
            return;
        }
        // South-East
        if (dirn <= 180.0) {
            xValues[index] = x + (int) (Math.cos(Math.toRadians(dirn - 90)) * dist);
            yValues[index] = y + (int) (Math.sin(Math.toRadians(dirn - 90)) * dist);
            return;
        }
        // South-West
        if (dirn <= 90.0) {
            xValues[index] = x - (int) (Math.sin(Math.toRadians(dirn - 180)) * dist);
            yValues[index] = y + (int) (Math.cos(Math.toRadians(dirn - 180)) * dist);
        }
        // Nort-West
        else {
            xValues[index] = x - (int) (Math.cos(Math.toRadians(dirn - 270)) * dist);
            yValues[index] = y - (int) (Math.sin(Math.toRadians(dirn - 270)) * dist);
        }
    }

    // ARROW COORDS: Load x-y value arrays */
    private void arrowCoords(int x1, int y1, int x2, int y2, int x3, int y3) {
        xValues[0] = x1;
        yValues[0] = y1;
        xValues[1] = x2;
        yValues[1] = y2;
        xValues[2] = x3;
        yValues[2] = y3;
    }

}
