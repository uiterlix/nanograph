/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.drawing.graphicsadapter;

/**
 * GraphicsContext.java
 * 
 * Rationale: 
 * - Create an abstraction of a painting object that fits on painting
 * frameworks like SVG, AWT (G2D), SWT etc.
 * - Create a stack of contexts for painting, such that a child context
 * inherits form a parent, can modify it, but after popping, the parent is unmodified.
 * - This is similar to SVG G-elements
 * - Each renderer can push a context, set its own layout, and pop afterward.
 * - Configuring and Drawing should not be mixed within one context
 *
 * @author Jeroen van Grondelle
 */
public interface GraphicsAdapter 
{
	
	public final static int FONT_STYLE_NORMAL = 1;
	public final static int FONT_STYLE_ITALIC = 2;
	public final static int FONT_STYLE_BOLD = 4;
	
	public final static int LINESTYLE_SOLID = 0;
	public final static int LINESTYLE_DOT = 1;
	public final static int LINESTYLE_DASH = 2;

	public final static int CONNECTOR_STYLE_NONE = 0;
	public final static int CONNECTOR_STYLE_ARROW = 1;
	public final static int CONNECTOR_STYLE_ARROW_FILLED = 2;
	public final static int CONNECTOR_STYLE_CIRCLE = 4;
	public final static int CONNECTOR_STYLE_CIRCLE_FILLED = 8;
	public final static int CONNECTOR_STYLE_DIAMOND = 16;
	public final static int CONNECTOR_STYLE_DIAMOND_FILLED = 32;
    
    public final static int LABEL_STYLE_NONE = 0;
    public final static int LABEL_STYLE_TEXT = 1; 
	
	// configuring
	public void setLineStyle(int lineStyle);
	public void setColor(String cssColor);
	public void setFillColor(String cssColor);
	public void setFont(String face, int size, int style);
	public void setAlpha(int alpha);
	
	// TODO: Linestyles in css format
	public void setLineStyle(String cssAttribute, String value);
	public void setFillStyle(String cssAttribute, String value);
	
	// drawing
	public void drawString(String string, double x1, double x2);
	
	// drawing lines
	public void drawLine(double x1, double y1, double x2, double y2);
	public void drawLine(double x1, double y1, double x2, double y2, int connectorBeginStyle, int connectorEndstyle, int lineStyle);
	
	public void drawOval(double x1, double y1, double x2, double y2);
	public void fillOval(double x1, double y1, double x2, double y2);
	
	public void drawArc(double x, double y, int arcWidth, int arcHeight, int startAngle, int arcAngle);
	
	// calculations
	public double stringWidth(String string);
	public double stringHeight(String string);
	
	public double imageWidth(String uri);
	public double imageHeight(String uri);
	
	public double imageWidth(String id, byte[] data);
	public double imageHeight(String id, byte[] data);
	
	public void drawRectangle(double x1, double y1, double width, double height);
	public void drawRoundRectangle(double x1, double y1, double x2, double y2, double j, double k);
	public void fillRectangle(double x1, double y1, double width, double height);
	public void fillRoundRectangle(double x1, double y1, double x2, double y2, double j, double k);
	public void fillPolygon(int[] values, int[] values2);
	public void drawPolygon(int[] values, int[] values2);
	public void drawImage(String uri, double x, double y);
	public void drawImage(String id, byte[] data, double x, double y);

}
