/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.drawing.layout;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;

import org.nanograph.components.NanoGraphPanel;
import org.nanograph.drawing.NanoGraph;
import org.nanograph.model.GraphModel;

public abstract class AbstractHierarchicalBaseLayout implements Runnable, LayoutAlgorithm, RootNodeLayout {

    public final static int DOWN = 0;
    public final static int UP = 1;
    public final static int BOTH = 2;


    protected NanoGraphPanel gp = null;
    protected GraphModel model = null;
    protected NanoGraph graph = null;
    private Thread relaxer = null;
    protected Hashtable treeCach = new Hashtable();
    protected Collection rootNodes = new ArrayList();
    private int direction = BOTH;

    public AbstractHierarchicalBaseLayout(NanoGraphPanel newGp) {
        super();
        gp = newGp;
    }

    public void findRootNodes() {
        //        final int nodeCount = model.getNodeCount();
//        for (int a = 0; a < nodeCount; a++) {
//            final Object na = model.getNode(a);
//            HierarchicalObject object = new HierarchicalObject(na, model);
//            nodeCach.put(na, object);
//        }
//        for (int a = 0; a < nodeCount; a++) {
//            final Object na = model.getNode(a);
//            final HierarchicalObject current = (HierarchicalObject) nodeCach.get(na);
//            ArrayList items = findImcomingNodes(na);
//            for (int i = 0; i < items.size(); i++) {
//                final HierarchicalObject parent = (HierarchicalObject) nodeCach.get(items.get(i));
//                if (parent != null) {
//                    current.addParent(parent);
//                }
//            }
//            items = findOutGoingNodes(na);
//            for (int i = 0; i < items.size(); i++) {
//                final HierarchicalObject parent = (HierarchicalObject) nodeCach.get(items.get(i));
//                if (parent != null) {
//                    current.addChild(parent);
//                }
//            }
//
//        }
    }

    public void setRootNodes(Collection newRootNodes) {
        rootNodes = newRootNodes;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    protected TreeObject getTreeObject(Object node) {
        TreeObject result = (TreeObject) treeCach.get(node);
        if (result == null) {
            //In this case an error has occured rebuid the tree.
            buildTree();
        }
        result = (TreeObject) treeCach.get(node);
        return result;
    }

    private Object[] findPathTo(Object start, Object to) {
        final int nodeCount = model.getNodeCount();
        for (int a = 0; a < nodeCount; a++) {
            final Object na = model.getNode(a);
            for (int b = 0; b < nodeCount; b++) {
                final Object nb = model.getNode(b);
                //We alk over every comination
                if (findPathTo(na, nb) != null) {
                    //a has path to b.

                }
            }

        }

        return null;
    }


    private ArrayList findIncomingNodes(Object toNode) {
        final ArrayList parents = new ArrayList();
        for (int t = 0; t < model.getNodeCount(); t++) {
            final Object node = model.getNode(t);
            //The node
            for (int et = 0; et < model.getEdgeTypeCount(node); et++) {
                int count = model.getEdgeCount(node, model.getEdgeType(node, et));
                for (int e = 0; e < count; e++) {
                    final Object nodeFrom = model.getDestinationNode(node, model.getEdgeType(node, et), e);
                    if (nodeFrom != null && nodeFrom.equals(toNode)) {
                        parents.add(node);
                    }
                }
            }
        }
        return parents;
    }

    private ArrayList findOutGoingNodes(Object node) {
        ArrayList children = new ArrayList();
        for (int et = 0; et < model.getEdgeTypeCount(node); et++) {
            int count = model.getEdgeCount(node, model.getEdgeType(node, et));
            for (int e = 0; e < count; e++) {
                final Object n2 = model.getDestinationNode(node, model.getEdgeType(node, et), e);
                if (n2 != null) {
                    children.add(n2);
                }
            }
        }
        return children;
    }

    public void layout(NanoGraph newGraph) {
        graph = newGraph;
        model = graph.getModel();
        buildTree();
        if (rootNodes == null || rootNodes.size() == 0) {
            findRootNodes();
        }
        this.start();
    }

    public void buildTree() {

        treeCach.clear();
        if(rootNodes != null){
            Iterator walker = rootNodes.iterator();
            final Hashtable handled = new Hashtable();
            while (walker.hasNext()) {
                Object current = walker.next();
                TreeObject object = new TreeObject(current, model);
                treeCach.put(current, object);
                handled.put(current, object);
                buildTree(object, handled);
            }
        }
//        walker = rootNodes.iterator();

//        while (walker.hasNext()) {
//            Object current = walker.next();
//            TreeObject child = (TreeObject) treeCach.get(current);
//            showTree(child, 0);
//        }
    }

//    private void showTree(TreeObject parent, int level) {
//        System.out.println(level + "" + parent.getData());
//        final TreeObject[] children = parent.getChildren();
//        for (int i = 0; i < children.length; i++) {
//            showTree(children[i], level + 1);
//        }
//    }
//

    private void buildTree(TreeObject current, Hashtable handled) {
        final ArrayList items = new ArrayList();
        if (direction == UP || direction == BOTH) {
            items.addAll(findIncomingNodes(current.getData()));

        }
        if (direction == DOWN || direction == BOTH) {
            items.addAll(findOutGoingNodes(current.getData()));

        }
        for (int i = 0; i < items.size(); i++) {
            Object childData = items.get(i);
            TreeObject child = (TreeObject) treeCach.get(childData);
            if (child == null) {
                child = new TreeObject(childData, model);
                treeCach.put(child.getData(), child);
                current.addChild(child);
            }
        }
        for (int i = 0; i < items.size(); i++) {
            Object childData = items.get(i);
            TreeObject child = (TreeObject) treeCach.get(childData);
            if (child != null) {
                if (!handled.containsKey(child.getData())) {
                    handled.put(child.getData(), child);
                    buildTree(child, handled);
                }
            }
        }
    }


    public abstract boolean shouldStop();

    public abstract void shouldStart();

    public abstract void doStep();


    public void run() {
        final Thread me = Thread.currentThread();
        while (relaxer == me) {
        doStep();
        gp.repaint();
            try {
                Thread.sleep(20); //Delay to wait for the prior repaint command to finish.
                if (shouldStop()) {
                    stop();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
                break;
            }
        }
    }

    public void start() {
        relaxer = new Thread(this);
        relaxer.start();
    }

    public void stop() {
        relaxer = null;
    }

    public int getDirection() {
        return direction;
    }

}
