/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */package org.nanograph.drawing.layout;

import java.awt.geom.Point2D;

import org.nanograph.drawing.NanoGraph;
import org.nanograph.model.GraphModel;

/**
 * CircularLayoutAlgorithm.java
 * 
 * @author Jeroen van Grondelle
 */
public class CircularLayoutAlgorithm implements LayoutAlgorithm {
	private int radius = 100;

	private Object centralNode = null;

	public CircularLayoutAlgorithm() {
	}

	public CircularLayoutAlgorithm(int radius) {
		this.radius = radius;
	}

	public CircularLayoutAlgorithm(int radius, Object centralNode) {
		this.radius = radius;
		this.centralNode = centralNode;
	}

	public Object getCentralNode() {
		return centralNode;
	}

	public void setCentralNode(Object centralNode) {
		this.centralNode = centralNode;
	}

	public void layout(NanoGraph graph) {
		GraphModel g = graph.getModel();
		double x, y, angle;
		Object node;
		int counter = 0;
		for (int i = 0; i < g.getNodeCount(); i++) {
			int nodesOnCircle = (centralNode == null ? g.getNodeCount() : (g.getNodeCount() - 1));
			double d = (counter / ((double) nodesOnCircle));
			angle = d * 2.0d * Math.PI;
			double rotateFactor = 2;
			if (nodesOnCircle % 2 == 0) {
				rotateFactor = 6;
			} else {
				rotateFactor = 3;
			}
			angle += Math.PI / rotateFactor;
			angle = angle % (2 * Math.PI);
			node = g.getNode(i);
			if (centralNode != null && centralNode.equals(node)) {
				x = y = radius;
			} else {
				x = radius + radius * Math.sin(angle);
				y = radius + radius * Math.cos(angle);
				// only increment the counter when this node wasn't the central node
				// or else we get overlapping nodes at 0 degrees and 2 PI degrees
				counter++;
			}
			g.setLocation(node, new Point2D.Double(x+100, y+50));
		}
	}
}
