/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.drawing.layout;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;

import org.nanograph.drawing.NanoGraph;
import org.nanograph.model.GraphModel;

/**
 * This class implements a Force Directed Layout scheme that is similar to the algorithm
 * presented in Sun's demo applet at
 * 		http://java.sun.com/applets/jdk/1.0/demo/GraphLayout/example3.html
 *
 *  @author Jeroen van Grondelle
 */
public class ForceDirectedLayoutAlgorithm implements LayoutAlgorithm
{
	public final int EDGE_LENGTH = 200;

	List nodes = new ArrayList();

	double[] deltax = null, deltay = null;

	JComponent component = null;

	GraphModel model = null;

	public ForceDirectedLayoutAlgorithm(JComponent c) {
		component = c;
	}

	public void layout(NanoGraph graph) {
		this.model = graph.getModel();

		new LayoutThread().start();
	}

	private void initLayout(GraphModel g) {
		deltax = new double[g.getNodeCount()];
		deltay = new double[g.getNodeCount()];

		for (int source = 0; source < g.getNodeCount(); source++) {
			nodes.add(g.getNode(source));
			deltax[source] = 0;
			deltay[source] = 0;
		}
	}

	private void reset(GraphModel g) {
		if (deltax.length != g.getNodeCount()) {
			initLayout(g);
		}
	}

	/*
	 */
	private void relaxEdges(GraphModel g) {
		for (int source = 0; source < g.getNodeCount(); source++) {
			Object node = g.getNode(source);

			for (int edgetype = 0; edgetype < g.getEdgeTypeCount(node); edgetype++) {
				Object edgeType = g.getEdgeType(node, edgetype);

				for (int edge = 0; edge < g.getEdgeCount(node, edgeType); edge++) {
					Object destination = g.getDestinationNode(node, edgeType,
							edge);

					double vx = model.getLocation(destination).getX()
							- model.getLocation(node).getX();
					double vy = model.getLocation(destination).getY()
							- model.getLocation(node).getY();
					double len = Math.sqrt(vx * vx + vy * vy);
					double f = (EDGE_LENGTH - len) / (len * 3);
					double dx = f * vx;
					double dy = f * vy;

					deltax[nodes.indexOf(destination)] += dx;
					deltay[nodes.indexOf(destination)] += dy;
					deltax[nodes.indexOf(node)] += -dx;
					deltay[nodes.indexOf(node)] += -dy;

				}
			}
		}
	}

	private void relaxNodes(GraphModel g) {

		for (int source = 0; source < g.getNodeCount(); source++) {
			Object node1 = g.getNode(source);
			double dx = 0;
			double dy = 0;

			for (int dest = 0; dest < g.getNodeCount(); dest++) {
				if (source == dest) {
					continue;
				}
				Object node2 = g.getNode(dest);

				double vx = model.getLocation(node1).getX()
						- model.getLocation(node2).getX();
				double vy = model.getLocation(node1).getY()
						- model.getLocation(node2).getY();
				double len = vx * vx + vy * vy;
				if (len == 0) {
					dx += Math.random();
					dy += Math.random();
				} else if (len < 100 * 100) {
					dx += vx / len;
					dy += vy / len;
				}

			}
			double dlen = dx * dx + dy * dy;
			if (dlen > 0) {
				dlen = Math.sqrt(dlen) / 2;
				deltax[nodes.indexOf(node1)] += dx / dlen;
				deltay[nodes.indexOf(node1)] += dy / dlen;
			}
		}
	}

	public void applyDelta(GraphModel g) {

		for (int source = 0; source < g.getNodeCount(); source++) {
			Object node = g.getNode(source);

			Point2D loc = model.getLocation(node);
			double x = loc.getX();
			double y = loc.getY();

			x += Math.max(-5, Math.min(5, deltax[nodes.indexOf(node)]));
			y += Math.max(-5, Math.min(5, deltay[nodes.indexOf(node)]));
			//System.out.println("v= " + n.dx + "," + n.dy);
			if (x < 0) {
				x = 0;
			} else if (x > component.getSize().width) {
				x = component.getSize().width;
			}
			if (y < 0) {
				y = 0;
			} else if (y > component.getSize().height) {
				y = component.getSize().height;
			}

			model.setLocation(node, new Point2D.Double(x, y));

			deltax[source] /= 2;
			deltay[source] /= 2;

		}
	}

	class LayoutThread extends Thread {

		public void run() {
			initLayout(model);

			for(int t=0; t< 30; t++) {
				reset(model);
				relaxEdges(model);
				relaxNodes(model);
				applyDelta(model);

				component.repaint();
				try {
					Thread.sleep(100);

				} catch (InterruptedException ie) {
					ie.printStackTrace();
				}
			}
		}
	}
}
