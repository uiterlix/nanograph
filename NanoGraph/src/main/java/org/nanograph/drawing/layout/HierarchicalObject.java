/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.drawing.layout;

import java.util.ArrayList;

import org.nanograph.model.GraphModel;



public class HierarchicalObject extends TreeObject {

    private ArrayList parents = new ArrayList();


    public HierarchicalObject(Object newNode, GraphModel newModel) {
        super(newNode, newModel);
    }


    /**
     *  The Functions for parents
     */

    public HierarchicalObject[] getParents() {
        return (HierarchicalObject[]) parents.toArray(new HierarchicalObject[0]);
    }

    public void setParents(HierarchicalObject[] newParents) {
        parents.clear();
        if (newParents != null) {
            for (int i = 0; i < newParents.length; i++) {
                parents.add(newParents);
            }

        }

    }

    public void addParents(HierarchicalObject[] newParents) {
        if (newParents != null) {
            for (int i = 0; i < newParents.length; i++) {
                parents.add(newParents);
            }
        }
    }

    public void addParent(HierarchicalObject newParents) {
        parents.add(newParents);
    }

    public int NumberOfParents() {
        return parents.size();
    }


}
