/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.drawing.layout;

import java.awt.geom.Point2D;

import org.nanograph.drawing.NanoGraph;
import org.nanograph.model.GraphModel;

/**
 * RandomLayoutAlgorithm.java
 *
 * @author Jeroen van Grondelle
 */
public class RandomLayoutAlgorithm implements LayoutAlgorithm {

    Object root = null;

    public void layout(NanoGraph graph) {
        GraphModel g = graph.getModel();
        for (int t = 0; t < g.getNodeCount(); t++) {
            g.setLocation(g.getNode(t), new Point2D.Double((int) (Math
                    .random() * 400), (int) (Math.random() * 400)));
        }
    }
}
