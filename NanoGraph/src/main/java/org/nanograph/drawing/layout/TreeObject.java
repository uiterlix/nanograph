/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.drawing.layout;

import java.awt.geom.Point2D;
import java.io.Serializable;
import java.util.ArrayList;

import org.nanograph.model.GraphModel;

public class TreeObject implements Serializable {

    private ArrayList children = new ArrayList();
    private Object node = null;
    private GraphModel model = null;

    public TreeObject(Object newNode, GraphModel newModel) {
        super();
        node = newNode;
        model = newModel;
    }

    public Object getData() {
        return node;
    }

    public void setLocation(Point2D p2d) {
        model.setLocation(node, p2d);
    }

    public Point2D getLocation() {
        return model.getLocation(node);

    }


    /**
     * The Chlidren Functions.
     */

    /**
     *
     * @param newChildren Collection
     */
    public void setChildren(TreeObject[] newChildren) {
        children.clear();
        if (newChildren != null) {
            for (int i = 0; i < newChildren.length; i++) {
                children.add(newChildren);
            }

        }
    }

    public void addChildren(TreeObject[] newChildren) {
        if (newChildren != null) {
            for (int i = 0; i < newChildren.length; i++) {
                children.add(newChildren);
            }

        }

    }

    public void addChild(TreeObject newChildren) {
        children.add(newChildren);
    }

    public int numberOfChildren() {
        return children.size();
    }

    public TreeObject[] getChildren() {
        return (TreeObject[]) children.toArray(new TreeObject[0]);
    }

    public int howDeep() {
        int result = 0;
        for (int i = 0; i < children.size(); i++) {
            final Object child = children.get(i);
            if (child instanceof HierarchicalObject) {
                final TreeObject real = (TreeObject) child;
                result = Math.max(result, real.howDeep());
            }
        }
        return result + 1;

    }

    public boolean isChild(Object possibleChild) {
        for (int i = 0; i < children.size(); i++) {
            final Object child = children.get(i);
            if (child instanceof TreeObject) {
                final TreeObject real = (TreeObject) child;
                if (real.isChild(possibleChild)) {
                    return true;
                }
            } else {
                if (child.equals(possibleChild)) {
                    return true;
                }
            }
        }
        return false;
    }


}
