/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.drawing.layout.circular;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Iterator;

import org.nanograph.components.NanoGraphPanel;
import org.nanograph.drawing.layout.AbstractHierarchicalBaseLayout;
import org.nanograph.drawing.layout.TreeObject;

public class CircularLayout extends AbstractHierarchicalBaseLayout {

	private double xSpace = 40.0;

	private boolean nodesMoved = false;

	public CircularLayout(NanoGraphPanel newGp) {
		super(newGp);
	}

	public void doStep() {
		if (rootNodes != null && rootNodes.size() > 0) {
			// First calculate the first node
			nodesMoved = false;
			final Iterator walker = rootNodes.iterator();
			while (walker.hasNext()) {
				final Object rootNode = walker.next();
				final TreeObject root = super.getTreeObject(rootNode);
				final double radius = getRadiusNeededForNode(root);
				System.out.println("" + radius);
				final Point2D locRoot = root.getLocation();
				handleChildren(root, locRoot, radius);
			}
		}
		nodesMoved = false;
	}

	private static double getDiagonal(Rectangle2D size) {
		return Math.sqrt(size.getWidth() * size.getWidth() + size.getHeight() * size.getHeight());
	}

	private void move(TreeObject current, Point2D desiredLocationParent) {
		current.setLocation(desiredLocationParent);

	}

	private void handleChildren(TreeObject parent, Point2D desiredLocationParent, double radius) {
		TreeObject[] children = parent.getChildren();
		double omtrek = radius * 2 * Math.PI;
		double degrees = 0;
		for (int t = 0; t < children.length; t++) {
			final TreeObject current = children[t];
			double diagonaal = (getRadiusNeededForNode(current) * 2);
			double angle = (omtrek / (diagonaal * Math.PI));
			degrees = degrees + Math.toDegrees(angle);
			final Point2D desired = new Point2D.Double(desiredLocationParent.getX() + (radius * Math.sin(Math.toRadians(degrees))), desiredLocationParent.getY() + (radius * Math.cos(Math.toRadians(degrees))));
			move(children[t], desired);
			handleChildren(children[t], desired, diagonaal);

		}

	}

	private double getRadiusNeededForNode(TreeObject node) {
		if (node.numberOfChildren() == 0) {
			return getDiagonal(graph.getNodeBounds(node.getData())) / 2;
		} else {
			double circle = 0.0;
			final TreeObject[] children = node.getChildren();
			for (int i = 0; i < children.length; i++) {
				circle = circle + xSpace + getRadiusNeededForNode(children[i]);
			}
			return (circle / Math.PI) / 2;
		}
	}

	public boolean shouldStop() {
		return !nodesMoved;
	}

	public void shouldStart() {
		nodesMoved = false;
		this.start();
	}

}
