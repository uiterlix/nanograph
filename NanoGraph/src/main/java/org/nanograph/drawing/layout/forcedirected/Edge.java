/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.drawing.layout.forcedirected;

public class Edge {

    public static int DEFAULT_LENGTH = 80;

    public Node from;

    public Node to;

    protected int length;

    protected boolean visible;

    protected String id = null;

    public Edge(Node f, Node t, int len) {
        from = f;
        to = t;
        length = len;
        visible = false;
    }

    public Edge(Node f, Node t) {
        this(f, t, DEFAULT_LENGTH);
    }

//    public static void setEdgeDefaultLength(int length) {
//        DEFAULT_LENGTH = length;
//    }

    public Node getFrom() {
        return from;
    }

    public Node getTo() {
        return to;
    }

    public String getID() {
        return id;
    }

    public void setID(String id) {
        this.id = id;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int len) {
        length = len;
    }

    public void setVisible(boolean v) {
        visible = v;
    }

    public boolean isVisible() {
        return visible;
    }
}
