/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.drawing.layout.forcedirected;

import java.awt.geom.Point2D;
import java.util.Hashtable;

import org.nanograph.components.NanoGraphPanel;
import org.nanograph.drawing.NanoGraph;
import org.nanograph.drawing.layout.LayoutAlgorithm;
import org.nanograph.model.GraphModel;

public class ForceDirectedLayout implements Runnable, LayoutAlgorithm {

    private final static double def_damper = 0.7;

    private Hashtable nodeCach = new Hashtable();
    private Hashtable edgeCach = new Hashtable();
    private double maxMotion = 0; // Keep an eye on the fastest moving node to see if the graph is stabilizing
    private double lastMaxMotion = 0;
    private double motionRatio = 0; // It's sort of a ratio, equal to lastMaxMotion/maxMotion-1
    private double damper = def_damper; // A low damper value causes the
    private boolean damping = true; // When damping is true, the damper
    private double rigidity = 1; // Rigidity has the same effect as
    private double newRigidity = 1;

    private Thread relaxer = null;
    private GraphModel model = null;


    private NanoGraphPanel gp = null;

    public ForceDirectedLayout(NanoGraphPanel gp) {
        this.gp = gp;
    }

    public void layout(NanoGraph graph) {
        model = graph.getModel();
        init();
        // start thread
        this.start();
    }

    private static String createEdgeName(Node n1, Node n2) {
    	if (n1 == null || n2 == null) {
    		return "";
    	}
        return n1.getID() + n2.getID();
    }

    private void init() {
        nodeCach.clear();
        edgeCach.clear();
//        final Map nodesMap = new HashMap();

        // tank graph model over in edges en nodes
        for (int t = 0; t < model.getNodeCount(); t++) {
            final Object n = model.getNode(t);
            final Node node = new Node(n.toString(), n, model);
            nodeCach.put(n, node);
        }
        for (int t = 0; t < model.getNodeCount(); t++) {
            final Object n = model.getNode(t);
            for (int et = 0; et < model.getEdgeTypeCount(n); et++) {
                for (int e = 0; e < model.getEdgeCount(n, model.getEdgeType(n, et)); e++) {
                    final Object n2 = model.getDestinationNode(n, model.getEdgeType(n, et), e);
                    final Node node1 = (Node) nodeCach.get(n);
                    final Node node2 = (Node) nodeCach.get(n2);
                    final Edge edge = new Edge(node1, node2);
                    edgeCach.put(createEdgeName(node1, node2), edge);
                }
            }
        }

    }


    void setRigidity(double r) {
        newRigidity = r; // update rigidity at the end of the relax()
        // thread
    }


    // relaxEdges is more like tense edges up. All edges pull nodes
    // closes together;
    private synchronized void relaxEdges() {

    	Point2D fromLocation;
    	Point2D toLocation;
        for (int t = 0; t < model.getNodeCount(); t++) {
            final Object n = model.getNode(t);
            for (int et = 0; et < model.getEdgeTypeCount(n); et++) {
                for (int ed = 0; ed < model.getEdgeCount(n, model.getEdgeType(n, et)); ed++) {
                    final Object n2 = model.getDestinationNode(n, model.getEdgeType(n, et), ed);
                    final Node node1 = (Node) nodeCach.get(n);
                    final Node node2 = (Node) nodeCach.get(n2);

                    Edge e = (Edge) edgeCach.get(createEdgeName(node1, node2));
                    if (e == null) {
                        e = new Edge(node1, node2);
                        edgeCach.put(createEdgeName(node1, node2), e);
                    }

//                     final Edge e = (Edge) iter.next();

                    fromLocation = e.from.getLocation();
                    toLocation = e.to.getLocation();
                    double vx = toLocation.getX() - fromLocation.getX();
                    double vy = toLocation.getY() - fromLocation.getY();
                    double len = Math.sqrt(vx * vx + vy * vy);

                    double dx = vx * rigidity; // rigidity makes edges tighter
                    double dy = vy * rigidity;

                    dx /= (e.getLength() * 100);
                    dy /= (e.getLength() * 100);

                    // Edges pull directly in proportion to the distance
                    // between the nodes. This is good,
                    // because we want the edges to be stretchy. The edges
                    // are ideal rubberbands. They
                    // They don't become springs when they are too short.
                    // That only causes the graph to
                    // oscillate.


                    if (e.to.justMadeLocal || e.to.markedForRemoval ||
                        (!e.from.justMadeLocal && !e.from.markedForRemoval)) {
                        e.to.dx -= dx * len;
                        e.to.dy -= dy * len;
                    } else {
                        double massfade = (e.from.markedForRemoval ?
                                           e.from.massfade : 1 - e.from.massfade);
                        massfade *= massfade;
                        e.to.dx -= dx * len * massfade;
                        e.to.dy -= dy * len * massfade;
                    }
                    if (e.from.justMadeLocal || e.from.markedForRemoval ||
                        (!e.to.justMadeLocal && !e.to.markedForRemoval)) {
                        e.from.dx += dx * len;
                        e.from.dy += dy * len;
                    } else {
                        double massfade = (e.to.markedForRemoval ? e.to.massfade :
                                           1 - e.to.massfade);
                        massfade *= massfade;
                        e.from.dx += dx * len * massfade;
                        e.from.dy += dy * len * massfade;
                    }
                }
            }
        }
    }


    /*
     * private synchronized void avoidLabels() { for (int i = 0 ; i <
     * graphEltSet.nodeNum() ; i++) { Node n1 = graphEltSet.nodeAt(i);
     * double dx = 0; double dy = 0;
     *
     * for (int j = 0 ; j < graphEltSet.nodeNum() ; j++) { if (i == j) {
     * continue; // It's kind of dumb to do things this way. j should go
     * from i+1 to nodeNum. } Node n2 = graphEltSet.nodeAt(j); double vx =
     * n1.x - n2.x; double vy = n1.y - n2.y; double len = vx * vx + vy * vy; //
     * so it's length squared if (len == 0) { dx += Math.random(); // If two
     * nodes are right on top of each other, randomly separate dy +=
     * Math.random(); } else if (len <600*600) { //600, because we don't
     * want deleted nodes to fly too far away dx += vx / len; // If it was
     * sqrt(len) then a single node surrounded by many others will dy += vy /
     * len; // always look like a circle. This might look good at first, but
     * I think // it makes large graphs look ugly + it contributes to
     * oscillation. A // linear function does not fall off fast enough, so
     * you get rough edges // in the 'force field'
     *  } } n1.dx += dx*100*rigidity; // rigidity makes nodes avoid each
     * other more. n1.dy += dy*100*rigidity; // I was surprised to see that
     * this exactly balances multiplying edge tensions // by the rigidity,
     * and thus has the effect of slowing the graph down, while // keeping
     * it looking identical.
     *  } }
     */

    private synchronized void avoidLabels() {
    	
    	Point2D n1Location;
    	Point2D n2Location;
    	
        for (int t = 0; t < model.getNodeCount(); t++) {
            final Object node1 = model.getNode(t);

            Node n1 = (Node) nodeCach.get(node1);
            if (n1 == null) {
                n1 = new Node(node1.toString(), node1, model);
                nodeCach.put(node1, n1);
            }

            for (int r = 0; r < model.getNodeCount(); r++) {
                final Object node2 = model.getNode(r);
                Node n2 = (Node) nodeCach.get(node2);
                if (n2 == null) {
                    n2 = new Node(node2.toString(), node2, model);
                    nodeCach.put(node2, n2);
                }

                double dx = 0;
                double dy = 0;
                
                n1Location = n1.getLocation();
                n2Location = n2.getLocation();
                
                double vx = n1Location.getX() - n2Location.getX();
                double vy = n1Location.getY() - n2Location.getY();
                double len = vx * vx + vy * vy; // so it's length
                // squared
                if (len == 0) {
                    dx = Math.random(); // If two nodes are right on top
                    // of each other, randomly
                    // separate
                    dy = Math.random();
                } else if (len < 600 * 600) { // 600, because we don't
                    // want deleted nodes to fly
                    // too far away
                    dx = vx / len; // If it was sqrt(len) then a single
                    // node surrounded by many others
                    // will
                    dy = vy / len; // always look like a circle. This
                    // might look good at first, but I
                    // think
                    // it makes large graphs look ugly +
                    // it contributes to oscillation. A
                    // linear function does not fall off
                    // fast enough, so you get rough
                    // edges
                    // in the 'force field'
                }

                int repSum = n1.repulsion * n2.repulsion / 100;
                if (n1.justMadeLocal || n1.markedForRemoval ||
                    (!n2.justMadeLocal && !n2.markedForRemoval)) {
                    n1.dx += dx * repSum * rigidity;
                    n1.dy += dy * repSum * rigidity;
                } else {
                    double massfade = (n2.markedForRemoval ? n2.massfade :
                                       1 - n2.massfade);
                    massfade *= massfade;
                    n1.dx += dx * repSum * rigidity * massfade;
                    n1.dy += dy * repSum * rigidity * massfade;
                }
                if (n2.justMadeLocal || n2.markedForRemoval ||
                    (!n1.justMadeLocal && !n1.markedForRemoval)) {
                    n2.dx -= dx * repSum * rigidity;
                    n2.dy -= dy * repSum * rigidity;
                } else {
                    double massfade = (n1.markedForRemoval ? n1.massfade :
                                       1 - n1.massfade);
                    massfade *= massfade;
                    n2.dx -= dx * repSum * rigidity * massfade;
                    n2.dy -= dy * repSum * rigidity * massfade;
                }
            }
        }
    }

    public void startDamper() {
        damping = true;
    }

    public void stopDamper() {
        damping = false;
        damper = 1.0; //A value of 1.0 means no damping
    }

    public void resetDamper() { //reset the damper, but don't keep damping.
        damping = true;
        damper = def_damper;
        this.start();
    }

    public void stopMotion() { // stabilize the graph, but do so gently by setting the damper to a low value
        damping = true;
        if (damper > 0.3) {
            damper = 0.3;
        } else {
            damper = 0;
        }
    }

    public void damp() {
        if (damping) {
            if (motionRatio <= 0.001) { //This is important.  Only damp when the graph starts to move faster
                //When there is noise, you damp roughly half the time. (Which is a lot)
                //
                //If things are slowing down, then you can let them do so on their own,
                //without damping.

                //If max motion<0.2, damp away
                //If by the time the damper has ticked down to 0.9, maxMotion is still>1, damp away
                //We never want the damper to be negative though
                if ((maxMotion < 0.2 || (maxMotion > 1 && damper < 0.9)) && damper > 0.01) {
                    damper -= 0.01;
                }
                //If we've slowed down significanly, damp more aggresively (then the line two below)
                else if (maxMotion < 0.4 && damper > 0.003) {
                    damper -= 0.003;
                }
                //If max motion is pretty high, and we just started damping, then only damp slightly
                else if (damper > 0.0001) {
                    damper -= 0.0001;
                }
            }
        }
        if (maxMotion < 0.001 && damping) {
            damper = 0;
        }
    }


    private synchronized void moveNodes() {
        lastMaxMotion = maxMotion;
        final double[] maxMotionA = new double[1];
        maxMotionA[0] = 0;
        Point2D nLocation;

        for (int t = 0; t < model.getNodeCount(); t++) {
            final Object node1 = model.getNode(t);

            Node n = (Node) nodeCach.get(node1);
            if (n == null) {
                n = new Node(node1.toString(), node1, model);
                nodeCach.put(node1, n);
            }

            double dx = n.dx;
//            n.getLocation().getX()
            double dy = n.dy;
            dx *= damper; //The damper slows things down.  It cuts down jiggling at the last moment, and optimizes
            dy *= damper; //layout.  As an experiment, get rid of the damper in these lines, and make a
            //long straight line of nodes.  It wiggles too much and doesn't straighten out.

            n.dx = dx / 2; //Slow down, but don't stop.  Nodes in motion store momentum.  This helps when the force
            n.dy = dy / 2; //on a node is very low, but you still want to get optimal layout.

            double distMoved = Math.sqrt(dx * dx + dy * dy); //how far did the node actually move?

            nLocation = n.getLocation();
            double xLoc = nLocation.getX();
            xLoc += Math.max( -30, Math.min(30, dx)); //don't move faster then 30 units at a time.
            if(xLoc < 0.0){
               xLoc = 0.0 ;
            }
            double yLoc = nLocation.getY();
            if(yLoc < 0.0){
               yLoc = 0.0 ;
            }

            yLoc += Math.max( -30, Math.min(30, dy)); //I forget when this is important.  Stopping severed nodes from flying away?

            n.setLocation(xLoc, yLoc);

            maxMotionA[0] = Math.max(distMoved, maxMotionA[0]);
        }

        maxMotion = maxMotionA[0];
        if (maxMotion > 0) {
            motionRatio = lastMaxMotion / maxMotion - 1; //subtract 1 to make a positive value mean that
        } else {
            motionRatio = 0; //things are moving faster
        }

        damp();

    }

    private synchronized void relax() {
        for (int i = 0; i < 10; i++) {
            relaxEdges();
            avoidLabels();
            moveNodes();
        }
        if (rigidity != newRigidity) {
            rigidity = newRigidity; // update
        }
        gp.repaint();
    }


    public void run() {
        Thread me = Thread.currentThread();
//      me.setPriority(1);       //Makes standard executable look better, but the applet look worse.
        while (relaxer == me) {
            relax();
            try {
                Thread.sleep(20); //Delay to wait for the prior repaint command to finish.
                if (damper < 0.1 && damping && maxMotion < 0.001) {
                    stop();
//                    myWait();
//                    resetDamper();

//                    relaxer.yield();
                }
                //System.out.println("damping " + damping + " damp " + damper + "  maxM " + maxMotion + "  motR " + motionRatio );
            } catch (InterruptedException e) {
                e.printStackTrace();
                break;
            }
        }
    }

    public void start() {
        relaxer = new Thread(this);
        relaxer.start();
    }

    public void stop() {
        relaxer = null;
    }

}
