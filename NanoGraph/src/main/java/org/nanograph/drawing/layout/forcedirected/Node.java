/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.drawing.layout.forcedirected;

import java.awt.geom.Point2D;

import org.nanograph.model.GraphModel;

public class Node {

    private String id;

    protected double dx;
    protected double dy;

    public double massfade = 1;

    protected int repulsion = 40;

    public boolean justMadeLocal = false;
    public boolean markedForRemoval = false;
    private GraphModel model = null;
    private Object data = null;

    public Node(String id, Object newData, GraphModel newModel) {
        super();
        data = newData;
        model = newModel;
        this.id = id;
//        x = Math.random() * 2 - 1; // If multiple nodes are added without repositioning,
//        y = Math.random() * 2 - 1; // randomizing starting location causes them to spread out nicely.
        repulsion = 160;
//        Point2D p2d = model.getLocation(data);
//        dx = p2d.getX();
//        dy = p2d.getY();
//        this.setLocation(p2d);
    }

    public void setID(String id) {
        this.id = id;
    }

    public String getID() {
        return id;
    }

    public void setLocation(double x, double y) {
    	model.setLocation(data, x, y);
//        model.setLocation(data, new Point2D.Double(x, y));
//        dx = p2d.getX();
//        dy = p2d.getY();
//        this.x = p.getX();
//        this.y = p.getY();
    }

//    public double getX(){
//        return  getLocation().getX();
//    }
//
//    public double getY(){
//        return  getLocation().getY();
//    }

//    public double getdX(){
//        return  getLocation().getX();
//    }
//
//    public double getdY(){
//        return  getLocation().getY();
//    }



    public Point2D getLocation() {
       return model.getLocation(data);
//        return new Point2D.Double(x, y);
    }
}
