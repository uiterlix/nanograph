/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.drawing.layout.tree;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Hashtable;
import java.util.Iterator;

import org.nanograph.components.NanoGraphPanel;
import org.nanograph.drawing.layout.AbstractHierarchicalBaseLayout;
import org.nanograph.drawing.layout.TreeObject;

public class TreeLayout extends AbstractHierarchicalBaseLayout {

	private double yOffSet = 30.0;

	private double xSpace = 40.0;

	private boolean nodesMoved = false;

	public TreeLayout(NanoGraphPanel newGp) {
		super(newGp);
	}

	public void doStep() {
		if (rootNodes != null && rootNodes.size() > 0) {
			// First calculate the first node
			nodesMoved = false;
			final Iterator walker = rootNodes.iterator();
			while (walker.hasNext()) {
				final Object rootNode = walker.next();
				final TreeObject root = super.getTreeObject(rootNode);
				final double width = getWithNeededForNode(root);

				final Point2D locRoot = root.getLocation();
				if (width / 2 > locRoot.getX()) {
					final Point2D desired = new Point2D.Double(width / 2, locRoot.getY());
					move(root, desired);
					handleChildren(root, desired, width);
				} else {
					handleChildren(root, locRoot, width);
				}

			}
		}
	}

	private void move(TreeObject current, Point2D desiredLocationParent) {
		/*
		final Point2D locNow = current.getLocation();
		double a = locNow.getX();
		double o = locNow.getY();
		double ta2 = desiredLocationParent.getX();
		double to2 = desiredLocationParent.getY();
		double diffX = ta2 - a;
		double diffY = to2 - o;
		// Move max 30 pix to the new locaion.
		double s = Math.sqrt(diffX * diffX + diffY * diffY);
		if (s > 30) {
			// //Only move max 30
			double sin = diffX / s;
			double cos = diffY / s;
			double o2 = sin * 30;
			double a2 = cos * 30;
			final Point2D newPoint = new Point2D.Double(a + o2, o + a2);
			nodesMoved = true;
			current.setLocation(newPoint);
		} else {
			// Move to endpoint.
			current.setLocation(desiredLocationParent);
		}
		*/
		current.setLocation(desiredLocationParent);
	}

	private void handleChildren(TreeObject parent, Point2D desiredLocationParent, double withParent) {
		final Rectangle2D parenSize = graph.getNodeBounds(parent.getData());
		// parent already set. Now draw children
		final TreeObject[] children = parent.getChildren();
		double parentCenter = desiredLocationParent.getX(); // This is the
		// Center line.
		double startXPos = parentCenter - (withParent / 2); // This is the left
		// point
		final double yPos = desiredLocationParent.getY() + parenSize.getHeight() + yOffSet;
		for (int i = 0; i < children.length; i++) {
			final TreeObject current = children[i];
			final double width = getWithNeededForNode(current);
			startXPos = startXPos + (width / 2);
			final Point2D desired = new Point2D.Double(startXPos, yPos);
			move(current, desired);
			startXPos = startXPos + (width / 2) + xSpace;
			handleChildren(current, desired, width);
		}

	}

	protected Hashtable levels = new Hashtable();

	private double getWithNeededForNode(TreeObject node) {
		levels.clear();
		buildLevels(node);
		int levelCount = getLevelsFound();
		Rectangle2D nodeBounds = graph.getNodeBounds(node.getData());
		double max = 0.0d;
		if (nodeBounds != null) {
			max = nodeBounds.getWidth();
		}
		for (int i = 0; i < levelCount; i++) {
			double crWi = 0;
			final Hashtable levelHolder = getLevel(i);
			final TreeObject[] current = (TreeObject[]) levelHolder.values().toArray(new TreeObject[0]);
			for (int ic = 0; ic < current.length; ic++) {
				crWi = crWi + graph.getNodeBounds(current[ic].getData()).getWidth();
				if (ic > 0) {
					crWi = crWi + xSpace;
				}
			}
			max = Math.max(crWi, max);
		}
		return max;
	}

	private int getLevelsFound() {
		return levels.size();
	}

	private Hashtable getLevel(int level) {
		Hashtable levelHolder = (Hashtable) levels.get(String.valueOf(level));
		if (levelHolder == null) {
			levelHolder = new Hashtable();
			levels.put(String.valueOf(level), levelHolder);
		}
		return levelHolder;
	}

	private void buildLevels(TreeObject node) {
		final Hashtable levelHolder = getLevel(0);
		levelHolder.put(node, node);
		buildLevels(node, 1);
	}

	private void buildLevels(TreeObject node, int level) {
		final Hashtable levelHolder = getLevel(level);
		final TreeObject[] children = node.getChildren();
		for (int i = 0; i < children.length; i++) {
			levelHolder.put(children[i], children[i]);
		}
		for (int i = 0; i < children.length; i++) {
			buildLevels(children[i], level + 1);
		}
		levels.put(String.valueOf(level), levelHolder);
	}

	public boolean shouldStop() {
		return !nodesMoved;
	}

	public void shouldStart() {
		nodesMoved = false;
		this.start();
	}

}
