/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.drawing.noderenderer;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.nanograph.drawing.graphicsadapter.GraphicsAdapter;

public abstract class AbstractIconNodeRenderer implements NodeRenderer {
    public final static int NO_ICON_MODE = 0;

    public final static int TEXT_UNDER_ICON_MODE = 1;

    public final static int TEXT_RIGHT_OF_ICON_MODE = 2;

    public final static int DEFAULT_ICON_MODE = TEXT_RIGHT_OF_ICON_MODE;

    protected String shadowColor = DEFAULT_SHADOW_COLOR;

    protected String borderColor = DEFAULT_BODY_COLOR;

    protected String bodyColor = DEFAULT_BODY_COLOR;

    protected String selectedBodyColor = DEFAULT_SELECTED_BODY_COLOR;

    protected String textColor = DEFAULT_TEXT_COLOR;

    protected String selectedTextColor = DEFAULT_SELECTED_BODY_COLOR;

    protected int margin = 10;

    protected int padding = 3;

    protected int iconRenderMode = DEFAULT_ICON_MODE;

    protected abstract String getIconURI(Object node);

    protected abstract String getLabel(Object node);

    public int getIconRenderMode() {
        return iconRenderMode;
    }

    public void setIconRenderMode(int iconRenderMode) {
        this.iconRenderMode = iconRenderMode;
    }

    public String getBodyColor() {
        return bodyColor;
    }

    public void setBodyColor(String bodyColor) {
        this.bodyColor = bodyColor;
    }

    public String getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(String borderColor) {
        this.borderColor = borderColor;
    }

    public int getMargin() {
        return margin;
    }

    public int getPadding() {
        return padding;
    }

    public String getSelectedBodyColor() {
        return selectedBodyColor;
    }

    public void setSelectedBodyColor(String selectedBodyColor) {
        this.selectedBodyColor = selectedBodyColor;
    }

    public String getSelectedTextColor() {
        return selectedTextColor;
    }

    public void setSelectedTextColor(String selectedTextColor) {
        this.selectedTextColor = selectedTextColor;
    }

    public String getShadowColor() {
        return shadowColor;
    }

    public void setShadowColor(String shadowColor) {
        this.shadowColor = shadowColor;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public void render(GraphicsAdapter gc, Object node, Point2D location) {
        render(gc, node, location, false);
    }

    public void renderSelected(GraphicsAdapter gc, Object node, Point2D location) {
        render(gc, node, location, true);
    }

    public void render(GraphicsAdapter gc, Object node, Point2D location, boolean selected) {
        Rectangle2D bounds = getNodeBounds(gc, node, location);
        String iconURI = getIconURI(node);
        int width = getWidth(gc, node);
        int height = getHeight(gc, node);
        int stringWidth = (int) gc.stringWidth(node.toString());
        int stringHeight = (int) gc.stringHeight(node.toString());
        
        gc.setFont(DEFAULT_FONT_FACE, DEFAULT_FONT_SIZE, DEFAULT_FONT_STYLE);
//      gc.setFont(DEFAULT_FONT_FACE, DEFAULT_FONT_SIZE, GraphicsAdapter.FONT_STYLE_ITALIC | GraphicsAdapter.FONT_STYLE_BOLD);
        if (getIconRenderMode() == TEXT_RIGHT_OF_ICON_MODE || getIconRenderMode() == NO_ICON_MODE) {
            
        	// shadow
        	gc.setFillColor(this.shadowColor);
        	gc.fillRoundRectangle(bounds.getMinX() + 2, bounds.getMinY() + 2, width + margin, height + margin, 15, 15);
//        	gc.fillOval(bounds.getMinX() + 2, bounds.getMinY() + 2, width + margin, height + margin);
        	
        	// front
        	gc.setFillColor((selected ? this.selectedBodyColor : this.bodyColor));
        	gc.fillRoundRectangle(bounds.getMinX(), bounds.getMinY(), width + margin, height + margin, 15, 15);
//        	gc.fillOval(bounds.getMinX(), bounds.getMinY(), width + margin, height + margin);
        	
        	// border
        	gc.setColor(borderColor);
        	gc.drawRoundRectangle(bounds.getMinX(), bounds.getMinY(), width + margin, height + margin, 15, 15);
//        	gc.drawOval(bounds.getMinX(), bounds.getMinY(), width + margin, height + margin);
        	
            int cursor = (int) location.getX() - (width / 2) + padding;
            if (iconURI != null && getIconRenderMode() == TEXT_RIGHT_OF_ICON_MODE) {
            	
            	// test with fixed image
//            	String imagedata = "iVBORw0KGgoAAAANSUhEUgAAADQAAAAyCAYAAAATIfj2AAAY6ElEQVR42u1ad1hV95b9nVtp0lXU2GPBFlvUWLEkRo0mMbHEWBK7WLFhR+lXrFFfFKOxJNbEHlswdjQWBASk994uXOrlAmvWwZn3XobMzJv35c3M9775Y3/nnMvl3r32Wnvt/UMFAPHPFK9vaqr/46jIFWlXfERC8AWREflAJJxZKfKjH4jKV9fFk+VtRO7zq5bRt472Sn5wakz20wtr0m4eCEj50fNo0pGFN+ICp92J2vvR4xe7R4Q+/lOb5NDDPaNenfj4/qvTi39KDj6/IDs82DHpwHxRnfZKpP3gLwpu7RMlSc9EWfQ9UZIaLqrLi0R5XoIoy00VhuhgUZGbJiry0uvF3w0oM+qBSD63TmS+uNoh+9a+bU/dO4dE+o2IS/btXpGyvjES3cyRsswMOcu1KHQ3R+FaXjdoUeChRfZuFVK3CiRtYWw0R9yGVojXjUyP8Jvwc17wNbe0szrHwruBoiTlOQHd/QcDuuwjkp5cEZkxTx2iji3aG77lLUPEUitkrnNkwoyNlijebI6SLZao3GKBKi8z1PqYocZHg1pvJa9KVPs14usOMHlaotbfApWe5ijdrEUefy/Fw7Y2ZkPrpLTLu9Ybkp5bl8c9/AcCqswTWTd3ihdXDnz2cMencSErnZCxSgn9aoGyjWqUbyIQX3sYtjqiZKsDinwaoCzADqX+NijTMXytYPRvAOgcUePNq58FQ8VnGSTr5cvP4TV3kxIvltsj1H/ki5K4xy5lWbGvAeX+YYBqBGoZZTmaxJMrPW6ve7s0dJkNctdpYdyiQJWnhAovLcHYosLPGpW+Fij1NUMZq1/qZ4kKnTWMBGT0sWLiDWDcboGKADOU6zSo2GoGY0ADXq35XmsY/Gxh0FmSLYGE+QpErB2UkRZ02K2mukpdkZf4BwGqNtX9LOmSl3vIQlvELLNE9hozlHqoGHJlFUywAUp3NobJnzLyVqPCUwnTVkuUUW5VW21Q4WOJKsqwxssWlVubEFRTFPuRTZ0DSgMcUbrDAeU7+bzNBkVbLVDso0YZC5a30BwP5zsh8pJ/YFl+UpPygsz/HqDamqq/RG11HdWGtAgR+/jC5Lvuzsa0BQL6dWYo2qBBkYcCBi9BSSlRxkobdKy6vz2MW5vBtLs1DDtboWJ/J2T7N0f5vm4w7e0Bg2cbVHs1gsnLEZXejmS0IeVIMJRmxY4GKN9mRqBWKNvehkVy5HcokbVC4PkcLSIC3a5V6HOcSuOfElDq3wPIJGpNFYIW/MHDLQPzIhdZoMhdidzVKhRspFuxb3I9tSjWNUBJgDVKdBbI826KdO+2yNvZCenb26HkYG9kBXRE4e5eMHzVF+leHWEgUyU0gDKaQtVWe4KSpWqDSoZRBuPfFCVfD4bx7BDkB7ZFtoclchap8WyKFUIOrTlfFHG3jbEw+zVL/y7qSw41fxUQpYVZzk98hqamfklpuQkUrhfIchdIdVchY7MtMr2dULavMyoPdEXhtlYo2tYB1d8NRtG+dsjW2SPfg0axyQqFm2yRp2uBBP8WiPV9Eyn+bVGypyP0uoYo8rZEuY+WvceeogsW0EyyD7mg/Mp0pJ74CFEBzkj1cETcEjPc/FSB51/Nu19RkGVr1OeKSgL766gHqCIrqi4qc2JEVVG62ZNvXG8/nG+FtKVqFNLRCtcIFKxXIGe9BsXshaqvO6JkR0sUBTSH3r8J8gnQsKc92WiBAn8H5K61QOYKS6Ssa4yQzS3x3Vxr3FvmgPD1TijY2R5ZnnYooL0bPDUEpXkNSmeFfBZHf8wFuadHI+VPPZCja4r4hRLC5lri+kRrxN0+N0/Ot9pk+k3UA1RVlCFMxVnCpE8TL0+u930yzxyJixRIXqlFhrsWBrKjXysxASvaMhnYYgfjrlaoDXRGJRu90MeasrNCDpMs5NAscLdCyiKCmGODYwsaQzfXHscmC0RsaYtUXXvkBxC4jw0M3hYw0hmr/Mxg8qMZbFAgdQdZvPwpUr51QemxPkh2k5DGYR3yucD1teNjCtKTmxsy4kVxetyfo34P1b5+0Gcld7mzpEVpwSIBwzKBRIJJWstZQoZKNkocomoagg1KfBxQ4d+I1WUv0MFKaNmFvmrot2hg2ECjWGOL1CWNcNu1KXQz7LBkqj12jVbjyIdKRG5qhrK9nVC+oznnjw0d0orBYeytQsVmBeIDWqMoaDbiTn6MqG2dkefXBKnLVEiYK3BjohMeHPT9Nv/VI5EdevvPUV9yRXnCVFkhHh1ccSZ0FvvGlbFEIGWNBZIJqIQMlW2ipXo2oDwcUOxJkD52qGUv1PrZsbHZM15KBi19kxb6lZbIcGuEcxPNsHKkORZTLrvfF7g43QKRG5qyDxuzhziIyaqRM8vIYWv0puw4gDMODUTqzSVIDnJD/PHRyNrRoW6lejVH4OlUa5z/oqsx/XmQS1HCC1EY+7Qu6gEylepFZsSDIUELWyNloQJ6AioiQzlrzZC9zhL5K/nM/in3tueKYwv9ZoL0tEUVQZm8GrC6XGW8OVR9tSj1Nkfxeu5xXIvuz7KEbqQWa0YrcGk6LXhtE6R5vkG5NeEQpnVzcyjnrCrjqlRC59T7OSH//GREXXBF9mNPZNEgItY3QdQiM7xyNUfkDDVufqxFyEndgZrKMmEszquLeoDyfz0tXuyZfiaMrpZMq0xcaoE0NrWeVl20SkLOatr2Gg3y1nO6U/9lfk3JUEOUywPTyxrVHgRF9kpYaT17Qb9FjQIOyOx1tlxGm+LpWkdErrInM81o8y04t5xYHPYh+6jMy4q9aYki9p9+a1skHx6DePZ+9A9fInhLBxagGeLpdDHsycTZGjz8VODcsiHplWUljWpoCLKy6jNUlNYgdN1bSWkzuQkvNsfzFS0RurIZipaoUL5YINNd+5qpzQ1Rs7cjana0pkycuGDawuRL6W20R80WR+5zNmx4Ld2P/eClgGkzVxwve+g9HJBOxkr8mlOyXFIJpJIgqinXcm+5J21Qzg2i2LsVTDfmIu3MFwjZ7YKwTU2BI28h102L2AWU/xwVQqcKfD+1DZ5fPfNJatgjkRr2uD6g6KNLR4QtbYpY6jRmqTmil9vRXayQtVSJvJUqWreCfcSjwFp7NmkH5Bzog+KTLsje5Yxi35bUP62cVlzhw32Ng9bAgVu2rSEB2KPK15rbtmWdYRT6ktm9A1H97XDot7fklmEOA12uxNeR4+BNFH/DWXZyHCL3DEHSARfE+bVD6VfOyFiiQdp8JeJmKfBiuoSrk8xx3Wvy1apyg+p3D3ghga7THk2VELVQjZeLNUghkHzZFJbRdVar2T8C+UsZK+yQ7tsVuT9+goJfvkDhxYlI1PVCvm+bOter9m3Ao0GDuooXBjRFrs6JoGQWtagK4GdSqhnbXGA6/TkKD/Tg0FWi2F9BZhyQ4+eM3KPDkH5wALIOD0HB0eGI9m6HWPeGSFhohqQvWOyZEp5PV+DuRBqMazdjcU7KW6itrQ8o6d6Z+XepzVcLlIjmEpq8jD1AU8hczqrwoJbKQ1o23a5gPQfdhpbIOTEGYd++i8ijHyKfek//E4ch7bZyG2XoTcnxKKDnFp7Hna04oDFZZO9x99MTdIJnZ5QfHQX9152Qx52w2EdRNwZyaBb6/d2RuasTZ5EzEv3bIYWFine3p9xo2+zvcM6iZ58JyLmemkJFPbj8WXZ8eIN6gGKvfb3uMd8Y7SoDskLCIhVyyVCGmwKRNIdozpXKr7qialdnxK60Qca+3ojY74Ksa4uhv++HzB9mIpdy0Qe8QZPgOsOtWx6aOZxT5Qfac1GVdze6JyWYRhmlc7jm6BpDzyOIgb1WyvNUgVdDZFO2yZu4H+6llLlaJaxz4Lplj0iOkugvJDz9ROBXgrn7EQGNt0X4jZOr9dlpLesBev7tmsBnUwRiXVWIJaCkxVxAZYZWqRC9wgYJq2yRvtqSJ0x75LhTzzSBou9Hovi6K8KPzUbpEy9kHRuNxE0OKPNQw0SXK/Xk0kprLjnozEW1Md2M8uLZJ0vXjM7niIxNFmSce+I6CbkcCVnc5rPdJWTzPsfDGpkedkhcYY54mlQkzeol2Qn9TMKT8QL3xtEYRmtwK9DjXFZ8xLv1AAXvmnXlORmKnadA9FJKTgbEnslYpUGMmyVSF3OdJ8D85ZxHnEkla7l5b3bCKy9nPPTvh7QfpyHUtwuyvJqiUj7BMjHT1sbc91ogg3LK5Wm1Qp41XEBzuevl+FCOXJH0q6W6z0znUSFthYSClUqUb7HmmNAgeYmC6xcLPE+JMPb3i8kSQiarEDyWDH0gcGwEzWGH27XUiKdj6gF6sG3atWdstNjZ7CO5AflB2bTrFDcNEjiTchdy2ybAHIIpXvV6LZJX+/iV3LwD+yJK1wkxm5rT3mkI7kx8DV1tIyXk0ZC9YQM9N+sKDx7gaOPZZC9rI+fbWgInA/qlKn4uC0bzKaC80xcqkcYCZnIxjpsjIWKahJdT1HgxQYPgj9S4RzC3uHUcGSpwbbvbzynhjz+uD0g3+WbIBAKiViPmq5HAXsokiHgylUAbL+QBL4nHiIQ1EpNWIW8J16ANrLS7DXK4Zadx8qeuMGMyBLyCBVhmzWLYI2WVHbcN7nhrKS0uujk8V6VvYDG4FxpW8WC4lMfvJVa0ZSUSCSRloSV3NhWS5jPmsJ+n0QionNAJWjwZp8bdsVrc/kCFmwR0yEXCT9vcgpLDHn1SX3IBU268IEPxnEMv56kQv0CBdIKIWUjKqeEcvh65hLTLPbWazU5j0C9n0rT4p5RGLG0+lewWcqnNdVWTae5ermzwFey9xRLyFr1mNGWphITlsrz4Pm7zeTM1yPhCiQR5/vF7w6arud6o8GqaAhHczsOZU9gECY/GSLg9jMvp+1x9uOT+9K7AgUECQV97/JAZEz6yHqCH+1YfezCJDLD5ognkKVkKX2CFdB6BM2ZJrJyqToby2SRpkRJJrGbifK5J3Pti5om6gZw4l6/PVzAkxFP3UZwZCa7sA+6FiTLDi/g67+N4n0jzSV7A6U8w8V9yT5P3vM9VeMk+ifxMjafjJASPUuDBGAVuE8x1MnLpPR4f2DfXhks4NVIBn+HWCL19eWZhVppDPUCPj/p9FURLjKbTRU4XCJmn5XneAnEzeD6ZKSfAsxEjgcknucoJy3+dke9fR+I8eb0nMwQvyyVjiZbvlRBDCcvvTVkk6oDFz5efJb5fjZgvyfYMDcMCz8Yr8Cub/dEo2jJ75C4ZuD1CgftjNLgxUsKl4RykMqChNIKhChzis+/Y5oh98XBMQXa6uh6g0GvH3c7yF2JJ8SsyFfGlBqEztYjkdI6nlGLIQNzc19d4MiADTFqoqLvKSSbN1SCVp8oEJhlFe5W1Hy//LlmKnynqQp4jr2awybmLhbNwLyilZx8rEUwWfqGcZEYevq9A8GgV7nFDvz3SDDdGqHF+sMDZAQI/DBH4abCEy0PU2DlIgm5SN2Qlxw6oqjLW3xTOByyedHysHV7Q36M+ZBN+zBXjcx59mUi4PAM4paMIrG7A0Xnks0n0HAVezWaCfC2S742eokLkZAXCOfjkiGBx4tgT8Vz5X01RIIw9ETaR1vuJhMf8ngej6VbD5JDwC6t+/30tbg1njwzV4KqLGc4PUhOICqffUeDUOwJnBgpcGMD7AWbw7m+FE96uoTx+2/3uLhcWdNb58NQeRTfeVeEZ6X42+vVEfsKqhsxkMqx22AwC4/XVbGr9SwnhX/D16Uo8m8L7SWqEUTZPKJfnH0osCF9n0mHjOUPGqxDC56cfKfHkQxV+pVvde1+JIPbCdRcFZaTB5YEqnOunwMmeCnzXU4kjvVQ42EOJfV3Z/D0kfPu2Esf7CJzso8Q3A2yxYVhL3L343ZHqmhphNP4OQ+kxL8WJ9bMvHOhvwQqp8IDye8iE7rNJHzNp+Tz/gvMghAPuOQE8mcQ1ZJIKTyZw0I3n/Xi5kVWUjISH7IPHY5QMVZ2M7r8n4Q6T/5n6vzGE12EqXKVsLg5U48f+ahzvKSGwk8C+dgJ7GNsZvm8KbGkr4NlBIKCLAvt6yAAV2N9dCc8eVvCa9p6pID+nr5x7jfxH0XrnIR6S0mOi+vm9+6bx8EAtrrPp7pKlO6zwfVb/EeUTzDn1gMZxhwzcHceNlwBuj1HjFjV/h9q/N0rJ4GvvKigdMjBMgauUyU/9OQB5vTZUjcuDVAwznBtgjjPvWLDq5jjQTYW9nVXY00mDbR3N4PmmGu6tlVjaQsKyVgLr2yng10mBHR0FdnXXwu0ta1w6svdoFdkpKyuti/p/9SkvFdXGSnFu+/pD3r2UOMtG/Jks/cJK3+F0vsNl8DYZC6Kkgkax4uPM8PMoM1qoGldcWHG5+izCTYL5mbK9MYyvyw08SIkr7IMrBHJhoAan3pYlJeFwdwUOdlfh624a7OqkREB7JXzbqrGplRburdRY0kqF+a0UDAkr2ymx2VkFXQcJm8ne6mFtER32dKGJx4byivK6qAcoLzlOFKQlibykaKcDs4ZGbu9BV2Ey15hUEJO8QRC/jFXiFgFe4/MVVv8yG/mSixI/DTfD5WFKXKQLXeCwO082zrN5z/WXQ4WzfVU40UuBI90kHKC0vmbscRbYSln5tJUoLQU2tJSwqpmE+U4SZjWW8Hljgcm8n/GGBNdWSqzkezYyFrThdx/U7a+phcZYXS2MPIIbf+/vcgVpiaIwI1nkJUSKpzfPd/GdOCB3Vx8tLvXmIOsr4Ryb99JwVvpdDS7SSmULPcs4STmdJoiTZPQEgZzm86m3ed+dQc1/31uDoz01ONiNDU4QezpJ2NNNi21dtNjcQY017chGa7LxhgLTmqkwtpkGYxopMLahhPGNFZjUVInJvM5sZYYZTgLzPh2XYKiodDJSTZU0g3+LeoCKc9L/HOVF+eLlrYtzvPo3qj3QW4FvKZGj/SR8P0CJU4MJxkVTF6cI4Pt3ZEB0p8FMnrPh1ACJvcF7yvYYZXWcgPZ3IStdFdjurIB/R/YD5bPxTSWWU1KuLZhsU4GpjQQ+dRQY5SAw0vZ1jHKQMKahCuOc1BjpoMJIGxrV5R+WVzPfsrKy38R/CsiQmykMmcniwcn97u59HOHTU4ud3WS3EdjdXcLBvhocYhwk2EPsiaOU1OGBCnzTnz8jowdps4GU1yEyc6i3GXZ3FthBQN5drbDJ2QLubPr5lNd0yuozGQhBjLUTGE0Q7zPp9xjDbCQMslVgUEMtetuq0cNWC6+5Uw2G/Ny3KTdRVVX1m/gvAeUnvRIZYY/EzZOBi2Z2dTQtayawtqMaGxgb26vg1VkJ/y4qfNXbEnt7WWAb5aWjzPwJOoA9sqOLhN1vqbGjmxr+nRXwaC9hUWszzG2hwpeU1wRHCeMIYLS9hPfsJIxgDCOA4Q0EXAimv7WSQDRob6VCe3srrJg3szAvO2vA3/yv4L8HKPHxLZESGSK+9V49dl73FqaPKYnP31BiWhMl5rdQYzGbdUVbDda012A15bTSWYJ7RwkbKavNzgRP51rFWbK0tcCcJgKfNJLwUUOy8a/SGkomhtiRBXsFBtoreVWhfwMJ/WxUZESDlloJbRztsG/XjrNlpaWtamtrhIkGUFv3p+va38TfBCjhcZCIfxFc9/Nzu7x7rZry4aN+/GIXRxXek/XNpMYzwfH2AhPZsFPI4gzGHM6P+bTd2c2luucp7JHxBCH/zgjGULIxmDGAYPraSmRCQi8+9+RzLwct2ptLaGHG19u1wsmjRw5XVlZaV9PRTKaqPwZQdUWZuLTXX4QG39FucZu7/v23uxjaWQj0YiLvyMmxwsMYw21fV/7jhgp84qTEhwT7AWMUYzhBD+J7BzL68339eO0jA5GDRepOuXWg3JpbSejY2BozJ4wrvRd0Y5ucW3l5uaioqPhjAV38ykfcv3peXD51TNwJutZtzeJZp97p0LxcBtbFSqAnoz8lNIgxgpV+jwDrJEUgQ2jD/Qm0Xx0Qgb4EI8fbjN78eSdzBj9nYLvGVbOnTyg6vH9HvL4wr2fdvwORmdLS0n8UoAvix2MHxf1fbohL506JH44H9vdZ67Z71OB+CT2a2jMxNrBKoIelEn2sVejXgGGjJIuKOjB9mHwfzpeeBNuFjLS3ltC5kVXtyL7doVu3/HnUs+DpJSXFjSsry21lILJ7VZv+kYCuXRDnCOhe0HVx9sQRce74QREW8kzs/+aQ+Y/fff/BxkWLd7pOnfqLS9dO+m6NbdHFTosOGoGOaoHOZgIdyIIsq6604gHOzbF41rSoWzdvLMjOyX3TWGWylfMwFOtFeVmJkM84/+OAfjgWKB7euy0OHDworp49K4Jv0RWTE5Xp6Wktg+/fHnni8NdLA3f6Hfdft+y6bp3bTzs3r7n4zQ6fs2cO77sQfOvaviqj0bbKWCWKi4rqkpePAcVFhRyU/4uAgu/dEfsDA8WV06fF7atXRUR4qKhgE2ekJopf796q++yav3yp9NdfbGLCJQaD0BcW1oH5QwH98/33sv8H9H83/gVkEqd4z9hmMwAAAABJRU5ErkJggg==";
//            	byte[] data = Base64.decode(imagedata);
//            	gc.drawImage("id", data, bounds.getMinX() + padding, bounds.getCenterY() - (gc.imageHeight(iconURI) / 2));
            	
            	gc.drawImage(iconURI, bounds.getMinX() + padding, bounds.getCenterY() - (gc.imageHeight(iconURI) / 2));
            	cursor += gc.imageWidth(iconURI) + padding;
            }
            
            gc.setColor(selected ? selectedTextColor : textColor);
            double stringX = 0;
            if (iconURI != null) {
                int iconWidth = (int) gc.imageWidth(iconURI);
                stringX = bounds.getMinX() + padding + iconWidth + padding;
            } else {
                stringX = bounds.getCenterX() - (stringWidth / 2);
            }
            double stringY = bounds.getCenterY() - (stringHeight / 2);
            gc.drawString(node.toString(), ((float) stringX), stringY);
            
        } else if (getIconRenderMode() == TEXT_UNDER_ICON_MODE) {
            if (iconURI != null) {
                gc.drawImage(iconURI, (int) bounds.getCenterX() - (gc.imageWidth(iconURI) / 2), (int) bounds.getCenterY() - (gc.imageHeight(iconURI) / 2) - 10);
            }
            gc.setColor(selected ? selectedTextColor : textColor);
            gc.drawString(node.toString(), (float) bounds.getCenterX() - (stringWidth / 2), (float) bounds.getMaxY() - 10);
        }
    }

    protected int getWidth(GraphicsAdapter gc, Object node) {
        int width = 0;
        gc.setFont(DEFAULT_FONT_FACE, DEFAULT_FONT_SIZE, DEFAULT_FONT_STYLE);
//        gc.setFont(DEFAULT_FONT_FACE, DEFAULT_FONT_SIZE, GraphicsAdapter.FONT_STYLE_ITALIC | GraphicsAdapter.FONT_STYLE_BOLD);        
        String iconURI = getIconURI(node);
        if (getIconRenderMode() == TEXT_RIGHT_OF_ICON_MODE || getIconRenderMode() == NO_ICON_MODE) {
            width = (int) gc.stringWidth(getLabel(node));
            if (iconURI != null) {
                width += gc.imageWidth(iconURI) + padding;
            }
        } else if (getIconRenderMode() == TEXT_UNDER_ICON_MODE) {
            if (iconURI != null) {
                width = (int) gc.imageWidth(iconURI);
                width = Math.max(width, (int) Math.max(gc.stringWidth(node.toString()), gc.imageWidth(iconURI)));
            } else {
                width = (int) gc.stringWidth(node.toString());
            }
        }
        return (width > 10 ? width : 10);
    }

    protected int getHeight(GraphicsAdapter gc, Object node) {
        int height = 0;
        String iconURI = getIconURI(node);
        if (getIconRenderMode() == TEXT_RIGHT_OF_ICON_MODE || getIconRenderMode() == NO_ICON_MODE) {
            if (iconURI != null) {
                height = (int) gc.imageHeight(iconURI);
            }
        } else if (getIconRenderMode() == TEXT_UNDER_ICON_MODE) {
            if (iconURI != null) {
                height = (int) gc.imageHeight(iconURI);
            }
            height += 20;
        }
        return (height > 30 ? height : 30);
    }

    public Rectangle2D getNodeBounds(GraphicsAdapter gc, Object node, Point2D location) {
        int width = getWidth(gc, node);
        int height = getHeight(gc, node);
        return new Rectangle2D.Double(location.getX() - (width / 2) - (margin / 2), location.getY() - (margin / 2) - (height / 2), width + margin, height + margin);
    }

    public void setMargin(int margin) {
        this.margin = margin;
    }

    public void setPadding(int padding) {
        this.padding = padding;
    }
}