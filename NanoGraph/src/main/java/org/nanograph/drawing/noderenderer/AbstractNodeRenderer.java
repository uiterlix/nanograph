/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.drawing.noderenderer;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.nanograph.drawing.graphicsadapter.GraphicsAdapter;

public abstract class AbstractNodeRenderer implements NodeRenderer {

	private String bodyColor = DEFAULT_BODY_COLOR;
	private String selectedBodyColor = DEFAULT_SELECTED_BODY_COLOR;
	private String shadowColor = DEFAULT_SHADOW_COLOR;
	private String textColor = DEFAULT_TEXT_COLOR;
	private String selectedTextColor = DEFAULT_SELECTED_TEXT_COLOR;
	private String fontFace = DEFAULT_FONT_FACE;
	private int fontSize = DEFAULT_FONT_SIZE;
	private int fontStyle = DEFAULT_FONT_STYLE;
	
    private int margin = 2;

    private int padding = 5;

    protected abstract String getLabel(Object node);

    public Rectangle2D getNodeBounds(GraphicsAdapter g, Object node, Point2D location) {
        // determine width
        int width = getWidth(g, node);
        int height = getHeight(node);

        Rectangle2D rect = new Rectangle2D.Double(location.getX() - (width / 2), location.getY() - (height / 2), width + margin, height + margin);

        return rect;
    }

    public void render(GraphicsAdapter g, Object node, Point2D location) {
        render(g, node, location, false);
    }

    public void renderSelected(GraphicsAdapter g, Object node, Point2D location) {
        render(g, node, location, true);
    }

    public void render(GraphicsAdapter g, Object node, Point2D location, boolean selected) {
    	
        int width = getWidth(g, node);
        int height = getHeight(node);

        // shadow
        g.setFillColor(shadowColor);
        g.fillRoundRectangle((int)location.getX() - (width / 2) + margin, (int) location.getY() - (height / 2) + margin, width, height, 15, 15);
        
        if (selected) {
        	g.setFillColor(selectedBodyColor);
        } else {
        	g.setFillColor(bodyColor);
        }
        // label
        g.fillRoundRectangle((int) location.getX() - (width / 2), (int) location.getY() - (height / 2), width, height, 15, 15);
        int textHeight = (int) g.stringHeight(getLabel(node));
        int cursorx = (int) location.getX() - (width / 2) + padding;
        int cursory = (int) location.getY() - (height / 2) + (textHeight / 2) + (padding) / 2;
        // text
        g.setColor(textColor);     

        if (selected) {
        	g.setColor(selectedTextColor);
        }
    	g.setFont(fontFace, fontSize, fontStyle);
        g.drawString(getLabel(node), cursorx, cursory);
    }

    /**
     * @return Returns the width.
     */
    private int getWidth(GraphicsAdapter g, Object node) {
    	
    	if (g == null) {
    		return 100;
    	}
    	
        int width = 10;

        int stringWidth = (int) g.stringWidth(getLabel(node));
        width += stringWidth;
        return width;
    }

    private int getHeight(Object node) {
        int height = 30;
        return height;
    }


}
