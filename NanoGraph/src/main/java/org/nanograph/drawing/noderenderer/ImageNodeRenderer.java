/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.drawing.noderenderer;


/**
 * 
 * @author Jeroen van Grondelle
 */
public class ImageNodeRenderer extends AbstractIconNodeRenderer {

    String imgURI;

    int width = 80;

    int height = 30;

    public ImageNodeRenderer() {
        init(null);
    }

    public ImageNodeRenderer(String image) {
        init(image);
    }

    private void init(String image) {
//        iconRenderMode = TEXT_RIGHT_OF_ICON_MODE;
        iconRenderMode = TEXT_RIGHT_OF_ICON_MODE;
        
        shadowColor = "#CCCCCC";
        borderColor = "#000000";
        bodyColor = "#99ddff";
        selectedBodyColor = "#99dd00";
        
        imgURI = image;
    }

    protected String getLabel(Object node) {
        return node.toString();
    }

	protected String getIconURI(Object node) {
		return imgURI != null && !"".equals(imgURI) ? imgURI : null;
	}
}
