/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.drawing.noderenderer;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.nanograph.drawing.graphicsadapter.GraphicsAdapter;

/**
 * A NodeRender implementation is able to render an node, given the node object
 * obtained from the GraphModel and the central point.
 * 
 * @author Jeroen van Grondelle
 */
public interface NodeRenderer {
    public static final String DEFAULT_SELECTED_BODY_COLOR = "#99DD00";

    public static final String DEFAULT_BODY_COLOR = "#99DDFF";

    public static final String DEFAULT_BORDER_COLOR = "#000000";
    
    public static final String DEFAULT_TEXT_COLOR = "#000000";
    
    public static final String DEFAULT_SELECTED_TEXT_COLOR = "#FF0000";;

    public static final String DEFAULT_SHADOW_COLOR = "#BBBBBB";
    
    public static final String DEFAULT_FONT_FACE = "Arial";
    
    public static final int DEFAULT_FONT_SIZE = 10;
    
    public static final int DEFAULT_FONT_STYLE = GraphicsAdapter.FONT_STYLE_NORMAL;
    
    public void render(GraphicsAdapter g, Object node, Point2D location);

    public void renderSelected(GraphicsAdapter g, Object node, Point2D location);

    public Rectangle2D getNodeBounds(GraphicsAdapter g, Object node, Point2D location);
}