/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.interaction.events;

import java.io.PrintWriter;
import java.io.Writer;

/**
 * DebugGraphListener.java
 */
public class DebugGraphListener implements GraphSelectionListener, GraphActionListener {

    PrintWriter writer = null;

    public DebugGraphListener(Writer w) {
        writer = w instanceof PrintWriter ? (PrintWriter) w : new PrintWriter(w);
    }

    public void selectionReset(GraphSelectionEvent e) {
        log("Selection was reset");
    }

    public void selectionChanged(GraphSelectionEvent e) {
        log("Selection was changed: " + e.getSelection());
    }

    public void graphActionPerformed(GraphActionEvent e) {
        log("An action was performed on object: " + e.getObject());
    }

    private void log(String msg) {
        writer.println("[DebugListener] " + msg);
        writer.flush();
    }

    public void graphActionReset() {
        log("An action was reset");
    }

	public void selectionMove(GraphSelectionEvent e) {
		log("Selection was moved: " + e.getSelection());
		
	}
}
