/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.interaction.events;

import org.nanograph.components.NanoGraphPanel;

/**
 * GraphActionEvent.java
 */
public class GraphActionEvent extends AbstractGraphEvent {
    
    public static final int EDGE_ACTION = 0, NODE_ACTION = 1;
    
    int type = -1;
    Object object = null;
    
    public GraphActionEvent(NanoGraphPanel source, Object object, int type)
    {
        super(source);
        this.type = type;
        this.object = object;
    }
    
    public boolean isNodeAction()
    {
        return type == NODE_ACTION;
    }
    
    public boolean isEdgeAction()
    {
        return type == EDGE_ACTION;
    }
    
    public Object getObject()
    {
        return object;
    }
    
}
