/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.interaction.selection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Selection.java
 *
 * @author Jeroen van Grondelle
 */
public class Selection {
    
    List selection = new ArrayList();
    
    public void reset()
    {
        selection.clear();
    }
    
    public void add(Object o)
    {
        selection.add(o);
    }
    
    public void toggle(Object o)
    {
        if(selection.contains(o))
        {
            selection.remove(o);
        }
        else
        {
            selection.add(o);
        }
    }
    
    public void addAll(Collection c)
    {
        selection.addAll(c);
    }
    
    public void toggleAll(Collection c)
    {
        Iterator i = c.iterator();
        while(i.hasNext())
        {
            Object o = i.next();
            if(selection.contains(o))
            {
                selection.remove(o);
            }
            else
            {
                selection.add(o);
            }
        }
    }
    
    public boolean isEmpty() 
    {
        return selection.isEmpty();
    }
    
    public boolean contains(Object o) 
    {
        return selection.contains(o);
    }
    
    public int size()
    {
        return selection.size();
    }
    
    public List getSelectedObjects()
    {
        return selection;
    }
    
    public String toString()
    {
        return "[Selection: "+selection+"]";
    }
}
