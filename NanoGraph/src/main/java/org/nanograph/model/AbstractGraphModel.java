/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.model;

import java.awt.geom.Point2D;
import java.util.HashMap;
import java.util.Map;

/**
 * AbstractGraphModel.java
 *
 * @author Jeroen van Grondelle
 */
public abstract class AbstractGraphModel {
	Map locations = new HashMap();
	
	public Point2D getLocation(Object node)
	{
		Point2D.Double p2d = null;
		
		Object o = locations.get(node);
		if(o == null)
		{
			p2d = new Point2D.Double(Math.random()*1000.0,Math.random()*1000.0);
			locations.put(node, p2d);
		}
		else
		{
			p2d = (Point2D.Double)o;
		}
		
		return p2d;
	}
	
	public void setLocation(Object node, Point2D point)
	{
		locations.put(node, point);
	}
	
	
	
}
