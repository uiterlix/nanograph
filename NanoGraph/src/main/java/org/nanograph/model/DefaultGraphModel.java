/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.model;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * DefaultGraphModel.java
 * 
 * @author Jeroen van Grondelle
 */
public class DefaultGraphModel extends AbstractGraphModel implements GraphModel {
    List nodes = new LinkedList();

    List[] edges = new List[10000];

    List[] destinations = new List[10000];

    public int getNodeCount() {
        return nodes.size();
    }

    public Object getNode(int index) {
        return nodes.get(index);
    }

    public int getEdgeTypeCount(Object node) {
        return 1;
    }

    public Object getEdgeType(Object node, int index) {
        return null;
    }

    public int getEdgeCount(Object node, Object edgeType) {
        return edges[nodes.indexOf(node)] == null ? 0 : edges[nodes.indexOf(node)].size();
    }

    public Object getEdge(Object node, Object edgeType, int edge) {
        return edges[nodes.indexOf(node)].get(edge);
    }

    public Object getDestinationNode(Object node, Object edgeType, int edge) {
        return destinations[nodes.indexOf(node)].get(edge);
    }

    public void addNode(Object o) {
        if (nodes.size() > 9999) {
            throw new IllegalStateException("Graph is full");
        }
        nodes.add(o);
    }

    public void addEdge(Object edge, Object source, Object sink) {
        int index = nodes.indexOf(source);

        if (index == -1 || nodes.indexOf(sink) == -1)
            throw new IllegalStateException("Unknown node");

        if (edges[index] == null) {
            edges[index] = new ArrayList();
            destinations[index] = new ArrayList();
        }

        edges[index].add(edge);
        destinations[index].add(sink);
    }

	public void setLocation(Object node, double x, double y) {
		setLocation(node, new Point2D.Double(x, y));
	}
}
