/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.model;

import java.awt.geom.Point2D;

/**
 * The GraphModel interface is the main graph abstraction
 * interface: any object model can be implemented by writing 
 * an adapter to it that implements this interface.
 * 
 * This interface is modeled with Swing's TreeModel interface in mind.
 *
 * @author Jeroen van Grondelle
 */
public interface GraphModel {
	
	// access nodes
	int getNodeCount();
	Object getNode(int index);
	
	// acces edgeTypes
	int getEdgeTypeCount(Object node);
	Object getEdgeType(Object node, int index);
	
	//	given a node and an edgeType, access edges
	int getEdgeCount(Object node, Object edgeType);
	Object getEdge(Object node, Object edgeType, int index);
	Object getDestinationNode(Object node, Object edgeType, int index);
	
	// layout methods
	public Point2D getLocation(Object node);
	public void setLocation(Object node, Point2D location);
	public void setLocation(Object node, double x, double y);
	
	//public void putNodeOnTop(Object node);

}