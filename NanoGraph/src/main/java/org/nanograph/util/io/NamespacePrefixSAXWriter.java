/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.util.io;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Stack;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.NamespaceSupport;

public class NamespacePrefixSAXWriter extends DefaultHandler implements ContentHandler {

	NamespaceSupport ns = new NamespaceSupport();
	Stack newPrefixes = new Stack();
	
	PrintWriter w = null;
	private int depth = 0;
	public static String TAB = "   ";
	public static String NEWLINE = "\n";
	
	/**
	 * NamespacePrefixSAXWriter that uses prefixes to encode namespaces.
	 * 
	 * This is necessary when inlining SVG inside HTML documents.
	 * 
	 * @param w
	 */
	public NamespacePrefixSAXWriter(Writer w)
	{
		this.w = w instanceof PrintWriter ? (PrintWriter)w: new PrintWriter(w);
	}
	
	public void startPrefixMapping(String prefix, String uri)
	{
		ns.pushContext();
		ns.declarePrefix(prefix, uri);
		newPrefixes.push(prefix);
	}

	public void endPrefixMapping(String prefix)
	{
		ns.popContext();
	}
	
	public void processingInstruction(java.lang.String target, java.lang.String data) 
	{
		w.write("<?"+target+" "+data+">");
	}
	
	public void startElement(String namespaceURI, String localName,
			String qName, Attributes atts) throws SAXException {
	    
	    String tag = namespaceURI != null && ns.getPrefix(namespaceURI)!= null ?
	            ns.getPrefix(namespaceURI) + ":" + localName : localName;
	    
		w.write(NamespacePrefixSAXWriter.NEWLINE);
		for (int i = 0; i < depth; i++) {
			w.write(NamespacePrefixSAXWriter.TAB);
		}
		depth++;
		
		w.write("<"+tag);
		
		for(int t = 0; t < atts.getLength(); t++)
		{
			if(atts.getURI(t) == null || !atts.getURI(t).equals("http://www.w3.org/2000/xmlns/"))
				w.write(" "+atts.getLocalName(t)+"=\""+atts.getValue(t)+"\"");
		}
		
		while(!newPrefixes.isEmpty())
		{
			String prefix = (String)newPrefixes.pop();
			w.write(" xmlns:"+prefix+"=\""+ns.getURI(prefix)+"\"");
		}
		w.write(">");
	}
	
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		w.write(ch, start, length);

	}

	public void endElement(String namespaceURI, String localName, String qName)
			throws SAXException {
	    
		 String tag = namespaceURI != null && ns.getPrefix(namespaceURI)!= null ?
	            ns.getPrefix(namespaceURI) + ":" + localName : localName;
	    
		depth--;
		w.write(NamespacePrefixSAXWriter.NEWLINE);
		for (int i = 0; i < depth; i++) {
			w.write(NamespacePrefixSAXWriter.TAB);
		}	
		
		w.write("</"+tag+">");
		
	}
	
	public void endDocument()
	{
		w.flush();
	}


}
