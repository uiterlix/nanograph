/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.util.test;

import java.io.OutputStreamWriter;

import org.nanograph.components.NanoGraphPanel;
import org.nanograph.drawing.background.SwimLaneBackground;
import org.nanograph.drawing.docking.IntersectionDockingStrategy;
import org.nanograph.drawing.edgerenderer.ArrowEdgeRenderer;
import org.nanograph.drawing.edgerenderer.EdgeRenderer;
import org.nanograph.drawing.layout.CircularLayoutAlgorithm;
import org.nanograph.drawing.noderenderer.ImageNodeRenderer;
import org.nanograph.drawing.noderenderer.NodeRenderer;
import org.nanograph.interaction.events.DebugGraphListener;
import org.nanograph.interaction.events.GraphActionEvent;
import org.nanograph.interaction.events.GraphActionListener;
import org.nanograph.interaction.events.GraphSelectionEvent;
import org.nanograph.interaction.events.GraphSelectionListener;
import org.nanograph.model.DefaultGraphModel;

public abstract class AbstractNanoGraphTest implements GraphActionListener, GraphSelectionListener {

	DefaultGraphModel g;
	NanoGraphPanel gp;
    private DebugGraphListener dgl = null;

    private NodeRenderer nodeRenderer = null;

    private EdgeRenderer edgeRenderer = null;

    public void initNanoGraph() {
        initGraph();
        dgl = new DebugGraphListener(new OutputStreamWriter(System.out));
        gp.getInteractionManager().addSelectionListener(dgl);
        gp.getInteractionManager().addActionListener(dgl);
        gp.getInteractionManager().addActionListener(this);
        gp.setModel(g);
        gp.getNanoGraph().setLayout(new CircularLayoutAlgorithm(800));
        gp.getNanoGraph().setBackground(new SwimLaneBackground());

        //gp.getNanoGraph().setLayout(new RandomLayoutAlgorithm());
//        gp.getNanoGraph().setBackground(new SwimLaneBackground(new String[] { "Swimlane A", "Swimlane B" }, SwimLaneBackground.MODE_VERTICAL));
        nodeRenderer = new ImageNodeRenderer("/icons/mandarijn.png");
//        nodeRenderer = new DefaultNodeRenderer();
        edgeRenderer = new ArrowEdgeRenderer();
        edgeRenderer.setNormalColor("olivedrab");
        
        // register the default renderers
        gp.getNanoGraph().setDefaultNodeRenderer(nodeRenderer);
        gp.getNanoGraph().setDefaultEdgeRenderer(edgeRenderer);
        // you can render different types of nodes differently
        gp.getNanoGraph().registerNodeRenderer(String.class, nodeRenderer);
        gp.getNanoGraph().registerNodeRenderer(MyNode.class, nodeRenderer);
        gp.getNanoGraph().registerDockingStrategy(String.class, new IntersectionDockingStrategy());

    }

    public void initGraph() {
        g = new DefaultGraphModel();
        MyNode language = new MyNode("Language node string");
        MyNode java = new MyNode("Java");
        MyNode python = new MyNode("Python");
        MyNode cpp = new MyNode("C++");
        MyNode perl = new MyNode("Perl");
        MyNode csharp = new MyNode("C#");
        MyNode lisp = new MyNode("Lisp");
        g.addNode(language);
        g.addNode(java);
        g.addNode(python);
        g.addNode(cpp);
        g.addNode(perl);
        g.addNode(lisp);
        g.addNode(csharp);
        g.addEdge(new MyEdge("is a"), java, language);
        g.addEdge(new MyEdge("is a high level"), python, language);
        g.addEdge(new MyEdge("is a"), cpp, language);
        g.addEdge(new MyEdge("is a"), perl, language);
        g.addEdge(new MyEdge("is a"), csharp, language);
        g.addEdge(new MyEdge("is a"), lisp, language);
        g.addEdge(new MyEdge("look a lot like"), csharp, java);
        g.addEdge(new MyEdge("look a lot like") ,language, generateGraph(g, 10));


        MyNode center = new MyNode("Center");
        g.addNode(center);
        MyNode level_0 = new MyNode("Level 0");
        g.addNode(level_0);
        g.addEdge(new MyEdge(" "), center, level_0);

        MyNode level_1 = new MyNode("Level 1");
        g.addNode(level_1);
        g.addEdge(new MyEdge(" "), center, level_1);

        MyNode level_2 = new MyNode("Level 2");
        g.addNode(level_2);
        g.addEdge(new MyEdge(" "), center, level_2);

        MyNode level_3 = new MyNode("Level3");
        g.addNode(level_3);
        g.addEdge(new MyEdge(" "), center, level_3);

//        MyNode level_4 = new MyNode("Level 4");
//        g.addNode(level_4);
//        g.addEdge(new MyEdge(" "), center, level_4);

//        MyNode level_5 = new MyNode("Level 5");
//        g.addNode(level_5);
//        g.addEdge(new MyEdge(" "), center, level_5);

        MyNode root = new MyNode("Root");
        g.addNode(root);
        MyNode level_01_01 = new MyNode("Level 1 1");
        g.addNode(level_01_01);
        g.addEdge(new MyEdge(" "), root, level_01_01);
        MyNode level_01_02 = new MyNode("Level 1 1");
        g.addNode(level_01_02);
        g.addEdge(new MyEdge(" "), root, level_01_02);
        MyNode level_02_02 = new MyNode("Level 2 1");
        g.addNode(level_02_02);
        g.addEdge(new MyEdge(" "), level_01_01, level_02_02);
        MyNode level_02_01 = new MyNode("Level 2 1");
        g.addNode(level_02_01);
        g.addEdge(new MyEdge(" "), level_01_01, level_02_01);
        MyNode level_03_01 = new MyNode("Level 3 1");
        g.addNode(level_03_01);
        g.addEdge(new MyEdge(" "), level_02_01, level_03_01);
        MyNode level_03_02 = new MyNode("Level 3 1");
        g.addNode(level_03_02);
        g.addEdge(new MyEdge(" "), level_02_01, level_03_02);
        MyNode level_03_03 = new MyNode("Level 3 1");
        g.addNode(level_03_03);
        g.addEdge(new MyEdge(" "), level_02_01, level_03_03);


    }

    private MyNode generateGraph(DefaultGraphModel g, int numOfNodes) {
        MyNode prevNode = null;
        MyNode firstNode = new MyNode("ok");
        for (int i=0; i < numOfNodes; i++) {
            MyNode n = new MyNode("node "+i);
            g.addNode(n);
            if (prevNode != null) {
                g.addEdge(new MyEdge("look a lot like"), n, prevNode);
                g.addEdge(new MyEdge("look a lot like"), n, firstNode);
            } else {
                firstNode = n;
            }
            prevNode = n;
        }
        return firstNode;
    }

    public void selectionChanged(GraphSelectionEvent e) {
    }

    public void selectionReset(GraphSelectionEvent e) {
    }

    public void graphActionPerformed(GraphActionEvent e) {
    }

    public void graphActionReset() {
    }


}

class MyNode {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MyNode(String name) {
        this.name = name;
    }

    public String toString() {
        return name;
    }
}

class MyEdge {
    private String name;

    public MyEdge() {

    }

    public MyEdge(String name) {
        this.name = name;
    }

    public String toString() {
        return name;
    }
}
