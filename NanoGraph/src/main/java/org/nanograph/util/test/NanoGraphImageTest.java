/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.util.test;

import java.io.OutputStreamWriter;

import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamSource;

import org.nanograph.components.image.NanoGraphImage;
import org.nanograph.drawing.NanoGraph;
import org.nanograph.drawing.layout.CircularLayoutAlgorithm;
import org.nanograph.model.DefaultGraphModel;
import org.nanograph.util.io.NamespacePrefixSAXWriter;

/**
 * NanoGraphImageTest.java
 *
 * @author Jeroen van Grondelle
 */
public class NanoGraphImageTest {

	public static void main(String[] args) throws Exception {
		
		/*
		 * Create default graph
		 */
		DefaultGraphModel g = new DefaultGraphModel();
		
		g.addNode("Java");
		g.addNode("Python");
		g.addNode("C++");
		g.addNode("Perl");
		g.addNode("C#");
		g.addNode("Programming Language");

		g.addEdge("is a", "Java", "Programming Language");
		g.addEdge("is a", "C#", "Programming Language");
		g.addEdge("is a", "C++", "Programming Language");
		g.addEdge("is a", "Perl", "Programming Language");
		g.addEdge("is a", "Python", "Programming Language");

		/*
		 * create instance of NanoGraphImage component
		 */
		NanoGraphImage image = new NanoGraphImage(600,600);
		image.setModel(g);
		image.getNanoGraph().setLayout(new CircularLayoutAlgorithm(300));
		
		/*
		 * create transformer with svg-in-html-embedding stylesheet
		 */
		SAXTransformerFactory tf = (SAXTransformerFactory)SAXTransformerFactory.newInstance();
		TransformerHandler t = tf.newTransformerHandler(new StreamSource(
				NanoGraph.class.getResourceAsStream("/stylesheets/embedded_svg.xsl")));
		
		// render to System.out
		t.setResult(new SAXResult(new NamespacePrefixSAXWriter(new OutputStreamWriter(System.out))));
		System.out.flush();
		image.storeAsSVG(t);
	}
}
