/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.util.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import org.nanograph.components.image.NanoGraphImage;
import org.nanograph.drawing.edgerenderer.ArrowEdgeRenderer;
import org.nanograph.drawing.edgerenderer.EdgeRenderer;
import org.nanograph.drawing.noderenderer.ImageNodeRenderer;
import org.nanograph.drawing.noderenderer.NodeRenderer;
import org.nanograph.interaction.events.GraphSelectionEvent;

/**
 * NanoGraphTest.java
 *
 * @author Jeroen van Grondelle
 */
public class SVGNanoGraphTest extends AbstractNanoGraphTest {

    public SVGNanoGraphTest() {
        initGraph();
    }
    
    public void createSVG(File f) {
    	try {
			NanoGraphImage image = new NanoGraphImage(1000,1000);
			image.setModel(g);
	        NodeRenderer nodeRenderer = new ImageNodeRenderer("/icons/mandarijn.png");
	        EdgeRenderer edgeRenderer = new ArrowEdgeRenderer();
	        edgeRenderer.setNormalColor("olivedrab");
	        image.getNanograph().setDefaultNodeRenderer(nodeRenderer);
	        image.getNanograph().setDefaultEdgeRenderer(edgeRenderer);
			FileOutputStream fout = new FileOutputStream(f);
			OutputStreamWriter writer = new OutputStreamWriter(fout);
			image.storeAsSVG(writer);
			writer.close();
			fout.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void createJPEG(File f) {
    	try {
			NanoGraphImage image = new NanoGraphImage(1000,1000);
			image.setModel(g);
	        NodeRenderer nodeRenderer = new ImageNodeRenderer("/icons/mandarijn.png");
	        EdgeRenderer edgeRenderer = new ArrowEdgeRenderer();
	        edgeRenderer.setNormalColor("olivedrab");
	        image.getNanograph().setDefaultNodeRenderer(nodeRenderer);
	        image.getNanograph().setDefaultEdgeRenderer(edgeRenderer);
			FileOutputStream fout = new FileOutputStream(f);
			image.storeAsJpeg(fout);
			fout.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}    	
    }
    
    public static void main(String[] args) {
        SVGNanoGraphTest test = new SVGNanoGraphTest();
        test.createJPEG(new File("c:\\test.jpg"));
        test.createSVG(new File("c:\\test.svg"));
    }

	public void selectionMove(GraphSelectionEvent e) {
	}
}
