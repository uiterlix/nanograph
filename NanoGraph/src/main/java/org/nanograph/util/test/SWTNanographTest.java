/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.util.test;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.nanograph.components.NanoGraphPanel;
import org.nanograph.components.swt.SWTNanoGraphPanel;
import org.nanograph.drawing.layout.forcedirected.ForceDirectedLayout;
import org.nanograph.interaction.events.GraphActionListener;
import org.nanograph.interaction.events.GraphSelectionEvent;
import org.nanograph.interaction.events.GraphSelectionListener;

public class SWTNanographTest extends AbstractNanoGraphTest implements GraphActionListener, GraphSelectionListener {

	public SWTNanographTest() {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setSize(800, 600);

	    shell.open();

	    shell.setLayout(new FillLayout());

	    Composite c = new Composite(shell, SWT.NONE);
	    GridLayout layout = new GridLayout();
	    layout.numColumns = 1;
	    c.setLayout(layout);

	    Composite buttonComposite = new Composite(c, SWT.NONE);
	    buttonComposite.setLayout(new FillLayout());
	    Button button = new Button(buttonComposite, SWT.NONE);
	    button.setText("Apply layout");
	    button.addSelectionListener(new SelectionListener() {

			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub

			}

			public void widgetSelected(SelectionEvent arg0) {
				gp.getNanoGraph().setLayout(new ForceDirectedLayout(gp));
			}

	    });

	    button = new Button(buttonComposite, SWT.NONE);
	    button.setText("Zoom in");
	    button.addSelectionListener(new SelectionListener() {

			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub

			}

			public void widgetSelected(SelectionEvent arg0) {
				System.out.println(gp.getZoomFactor());
				if (gp.getZoomFactor() > 0.15 && gp.getZoomFactor() <= 1.0) {
					gp.setZoomFactor(gp.getZoomFactor() + NanoGraphPanel.ZOOM_STEP);
					gp.repaint();
				}
			}

	    });

	    button = new Button(buttonComposite, SWT.NONE);
	    button.setText("Zoom out");
	    button.addSelectionListener(new SelectionListener() {

			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub

			}

			public void widgetSelected(SelectionEvent arg0) {
				if (gp.getZoomFactor() <= 1.0) {
					gp.setZoomFactor(gp.getZoomFactor() - NanoGraphPanel.ZOOM_STEP);
					gp.repaint();
				}
			}

	    });

	    c.setSize(shell.getSize());

	    // Create the ScrolledComposite to scroll horizontally and vertically
	    ScrolledComposite sc = new ScrolledComposite(c, SWT.H_SCROLL | SWT.V_SCROLL);

	    // Create the actual graphpanel
	    gp = new SWTNanoGraphPanel(sc, SWT.DOUBLE_BUFFERED);
	    ((SWTNanoGraphPanel) gp).setReduceColordepthOnZoom(true);

	    initNanoGraph();

	    sc.setContent((SWTNanoGraphPanel)gp);
	    GridData data = new GridData(GridData.FILL_HORIZONTAL | GridData.FILL_VERTICAL);
	    data.grabExcessHorizontalSpace = true;
	    data.grabExcessVerticalSpace = true;
	    sc.setLayoutData(data);

	    shell.layout();
	    while (!shell.isDisposed()) {
	        if (!display.readAndDispatch())
	          display.sleep();
	      }
	      display.dispose();

	}

	public static void main(String args[]) {
		SWTNanographTest swNanographTest = new SWTNanographTest();

	}
        public void selectionMove(GraphSelectionEvent e) {
                        // TODO Auto-generated method stub

	}

}
