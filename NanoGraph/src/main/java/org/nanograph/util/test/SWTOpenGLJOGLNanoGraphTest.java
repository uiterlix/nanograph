/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.util.test;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.Shell;
import org.nanograph.components.opengl.SWTOpenGLJOGLNanoGraphPanel;
import org.nanograph.interaction.events.GraphActionListener;
import org.nanograph.interaction.events.GraphSelectionEvent;
import org.nanograph.interaction.events.GraphSelectionListener;

public class SWTOpenGLJOGLNanoGraphTest extends AbstractNanoGraphTest implements GraphActionListener, GraphSelectionListener {

    public SWTOpenGLJOGLNanoGraphTest() {
        final Display display = new Display();
        Shell shell = new Shell(display);
        shell.setLayout(new FillLayout());
        
        // Create the actual graphpanel
        Composite body = new Composite(shell, SWT.NONE);
        Layout layout = new FillLayout();
        body.setLayout(layout);
        
        gp = new SWTOpenGLJOGLNanoGraphPanel(body);
        initNanoGraph();
        shell.setText("SWT/JOGL Nanograph test");
        shell.setSize(1024, 768);
        shell.open();
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }
        display.dispose();
    }

    public static void main(String args[]) {
        SWTOpenGLJOGLNanoGraphTest swNanographTest = new SWTOpenGLJOGLNanoGraphTest();

    }

    public void selectionMove(GraphSelectionEvent e) {
    }

}