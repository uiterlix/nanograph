/*
 * NanoGraph, a small footprint java graph drawing component
 * 
 *    Copyright 2004 Jeroen van Grondelle
 *    	        2013 Xander Uiterlinden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.nanograph.util.test;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.nanograph.components.swing.SwingNanoGraphPanel;
import org.nanograph.drawing.layout.CircularLayoutAlgorithm;
import org.nanograph.drawing.layout.LayoutAlgorithm;
import org.nanograph.drawing.layout.RootNodeLayout;
import org.nanograph.drawing.layout.circular.CircularLayout;
import org.nanograph.drawing.layout.forcedirected.ForceDirectedLayout;
import org.nanograph.drawing.layout.tree.TreeLayout;
import org.nanograph.interaction.events.GraphActionEvent;
import org.nanograph.interaction.events.GraphActionListener;
import org.nanograph.interaction.events.GraphSelectionEvent;
import org.nanograph.interaction.events.GraphSelectionListener;

/**
 * NanoGraphTest.java
 *
 * @author Jeroen van Grondelle
 */
public class SwingNanoGraphTest extends AbstractNanoGraphTest implements GraphActionListener, GraphSelectionListener {

    private LayoutAlgorithm fd = null;
    private Collection currentSelection = null;

    public SwingNanoGraphTest() {
        gp = new SwingNanoGraphPanel();

        initNanoGraph();

        JFrame f = new JFrame("NanoGraph Test");
         f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         f.getContentPane().setLayout(new BorderLayout());
         f.getContentPane().add(new JScrollPane((SwingNanoGraphPanel) gp), "Center");

         JPanel topPanel = new JPanel();
         topPanel.setLayout(new FlowLayout());

         JButton circular01Button = new JButton("Circular Layout 01");

         circular01Button.addActionListener(new ActionListener() {
             public void actionPerformed(ActionEvent ae) {
                 fd = new CircularLayoutAlgorithm(400);
                 if (fd instanceof RootNodeLayout) {
                     RootNodeLayout temp = (RootNodeLayout) fd;
                     temp.setRootNodes(currentSelection);
                 }
                 gp.getNanoGraph().setLayout(fd);
             }
         });

         topPanel.add(circular01Button, FlowLayout.LEFT);


         JButton circularButton = new JButton("Circular Layout");

         circularButton.addActionListener(new ActionListener() {
             public void actionPerformed(ActionEvent ae) {
                 fd = new CircularLayout(gp);
                 if (fd instanceof RootNodeLayout) {
                     RootNodeLayout temp = (RootNodeLayout) fd;
                     temp.setRootNodes(currentSelection);
                 }
                 gp.getNanoGraph().setLayout(fd);
             }
         });

         topPanel.add(circularButton, FlowLayout.LEFT);
         JButton treeButton = new JButton("Tree Layout");

         treeButton.addActionListener(new ActionListener() {
             public void actionPerformed(ActionEvent ae) {
                 fd = new TreeLayout(gp);
                 if (fd instanceof RootNodeLayout) {
                     RootNodeLayout temp = (RootNodeLayout) fd;
                     temp.setRootNodes(currentSelection);
                 }
                 gp.getNanoGraph().setLayout(fd);
             }
         });


         topPanel.add(treeButton, FlowLayout.LEFT);

         f.getContentPane().add(topPanel, "North");

         JButton forceDirectedLayout = new JButton("Auto Force Layout");

         forceDirectedLayout.addActionListener(new ActionListener() {
             public void actionPerformed(ActionEvent ae) {
                 fd = new ForceDirectedLayout(gp);
                 gp.getNanoGraph().setLayout(fd);
             }
         });

         topPanel.add(forceDirectedLayout, FlowLayout.LEFT);

         f.getContentPane().add(topPanel, "North");

        JSlider s = new JSlider(50, 100);
        s.setPaintLabels(true);
        s.setPaintTicks(true);
        s.setMajorTickSpacing(25);

        f.getContentPane().add(s, "South");
        s.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                gp.setZoomFactor((double) ((JSlider) e.getSource()).getValue() / 100);
            }
        });

        JComponent c = ((SwingNanoGraphPanel) gp).getOverview();
        c.setMinimumSize(new Dimension(100, 100));
        c.setSize(new Dimension(100, 100));
        c.setMaximumSize(new Dimension(100, 100));
        c.setPreferredSize(new Dimension(100, 100));

        f.getContentPane().add(c, "East");

        f.pack();
        f.setSize(800, 600);
        f.setVisible(true);
        gp.getInteractionManager().addSelectionListener(this);
    }


    public void selectionChanged(GraphSelectionEvent e) {
        currentSelection = e.getSelection().getSelectedObjects();
        if (fd instanceof RootNodeLayout) {
            RootNodeLayout temp = (RootNodeLayout) fd;
            temp.setRootNodes(e.getSelection().getSelectedObjects());
        }
    }

    public void selectionReset(GraphSelectionEvent e) {

    }

    public void selectionMove(GraphSelectionEvent e) {
        if (fd instanceof ForceDirectedLayout) {
            ForceDirectedLayout real = (ForceDirectedLayout) fd;
            real.resetDamper();
        }
    }

    public void graphActionPerformed(GraphActionEvent e) {
        if (e.isNodeAction()) {

        }
    }

    public void graphActionReset() {
    }

    public static void main(String[] args) {
        new SwingNanoGraphTest();
    }
}
