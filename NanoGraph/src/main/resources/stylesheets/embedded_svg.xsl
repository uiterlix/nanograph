<?xml version="1.0"?>

<xsl:stylesheet version="1.0" 
	xmlns:svg="http://www.w3.org/2000/svg"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<xsl:output method="html" />
	
	<xsl:template match="/">
		<html>
			<head>
				<object id="AdobeSVG" classid="clsid:78156a80-c6a1-4bbf-8e6a-3cd390eeb4e2"/>
				<xsl:processing-instruction name="import">
					<xsl:text>namespace="svg" implementation="#AdobeSVG"</xsl:text>
				</xsl:processing-instruction>
				
			</head>
			<body>
				<h1>SVG Graphic</h1>
				<xsl:copy-of select="svg:svg"/>
				<h1>SVG Graphic</h1>
				<xsl:copy-of select="svg:svg"/>
			</body>
		</html>
	</xsl:template>

</xsl:stylesheet>