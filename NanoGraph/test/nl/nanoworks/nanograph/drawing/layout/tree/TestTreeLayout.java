package nl.nanoworks.nanograph.drawing.layout.tree;

import junit.framework.*;

public class TestTreeLayout extends TestCase {
    private TreeLayout treeLayout = null;

    public TestTreeLayout(String name) {
        super(name);
    }

    protected void setUp() throws Exception {
        super.setUp();
        /**@todo verify the constructors*/
        treeLayout = new TreeLayout(null);
    }

    protected void tearDown() throws Exception {
        treeLayout = null;
        super.tearDown();
    }

    public void testDoStep() {

        double a = 80;
        double o = 40;
        double s = Math.sqrt(a*a + o*o);

        double sin = o/s;
        double cos = a/s;
        double o2 = sin * 30;
        double a2 = cos * 30;

       System.out.println("" + o2);
       System.out.println("" + a2);

       double s2 = Math.sqrt(a2*a2 + o2*o2);
       System.out.println("" + s2);
    }

}
